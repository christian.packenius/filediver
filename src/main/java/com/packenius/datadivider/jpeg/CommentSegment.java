package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class CommentSegment extends VariableLengthMarker {
  public CommentSegment(DumpReader reader) {
    super(reader, "COM", "Comment Segment", false);
    readContentBytes("Comment segment content.");
    setEndAddress(reader);
  }
}
