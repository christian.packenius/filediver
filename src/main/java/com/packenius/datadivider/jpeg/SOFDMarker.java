package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOFDMarker extends VariableLengthMarker {
  private final String title;

  public SOFDMarker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF13 - Start Of Frame Marker", false);
    readContentBytes("SOF13 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
