package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOF6Marker extends VariableLengthMarker {
  private final String title;

  public SOF6Marker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF6 - Start Of Frame Marker", false);
    readContentBytes("SOF6 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
