package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOFEMarker extends VariableLengthMarker {
  private final String title;

  public SOFEMarker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF14 - Start Of Frame Marker", false);
    readContentBytes("SOF14 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
