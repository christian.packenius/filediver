package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOFAMarker extends VariableLengthMarker {
  private final String title;

  public SOFAMarker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF10 - Start Of Frame Marker", false);
    readContentBytes("SOF10 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
