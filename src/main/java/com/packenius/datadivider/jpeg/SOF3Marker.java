package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOF3Marker extends VariableLengthMarker {
  private final String title;

  public SOF3Marker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF3 - Start Of Frame Marker", false);
    readContentBytes("SOF3 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
