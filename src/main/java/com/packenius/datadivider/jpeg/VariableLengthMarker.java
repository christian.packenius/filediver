package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public abstract class VariableLengthMarker extends BaseMarker {
  protected final int length;

  public VariableLengthMarker(DumpReader reader, String name, String title, boolean isMarkerEnd) {
    super(reader, name, title, isMarkerEnd);
    length = reader.readBigEndianU2("Marker segment length").value;
  }

  protected byte[] readContentBytes(String title) {
    return reader.readBytes(length - 2, title);
  }
}
