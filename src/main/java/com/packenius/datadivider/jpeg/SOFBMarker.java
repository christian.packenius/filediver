package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOFBMarker extends VariableLengthMarker {
  private final String title;

  public SOFBMarker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF11 - Start Of Frame Marker", false);
    readContentBytes("SOF11 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
