package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.CrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.MainDumpBlock;

/**
 * Creates a dump of a JPEG stream.
 *
 * @author Christian Packenius, 2019.
 */
public class JpegDivider extends MainDumpBlock {
  public JpegDivider(DumpReader reader) {
    super(reader);

    // Dump next marker (block).
    while (reader.getCurrentIndex() < reader.length) {
      readNextJpegMarker(reader);
    }

    setEndAddress(reader);
  }

  private void readNextJpegMarker(DumpReader reader) {
    if (reader.getCurrentByte() != (byte) 0xff) {
      String message = "No JPEG marker start at index " + reader.getCurrentIndex() + "!";
      System.out.println(message);
      throw new RuntimeException(message);
    }
    int marker = 255 & reader.getDeltaByte(1);
    switch (marker) {
      case 0xc0:
        new SOF0Marker(reader);
        break;
      case 0xc1:
        new SOF1Marker(reader);
        break;
      case 0xc2:
        new SOF2Marker(reader, marker, "SOF2", "Start Of Frame Marker (Progressive DCT)");
        break;
      case 0xc3:
        new SOF3Marker(reader, marker, "SOF3", "Start Of Frame Marker (Lossless (Sequential))");
        break;
      case 0xc4:
        new DHTMarker(reader);
        break;
      case 0xc5:
        new SOF5Marker(reader, marker, "SOF5", "Start Of Frame Marker (Differential Sequential DCT)");
        break;
      case 0xc6:
        new SOF6Marker(reader, marker, "SOF6", "Start Of Frame Marker (Differential Progressive DCT)");
        break;
      case 0xc7:
        new SOF7Marker(reader, marker, "SOF7", "Start Of Frame Marker (Differential Lossless)");
        break;
      case 0xc9:
        new SOF9Marker(reader, marker, "0xFFC9 - SOF9",
            "Start Of Frame Marker (Non-Differential Extended Sequential DCT)");
        break;
      case 0xca:
        new SOFAMarker(reader, marker, "0xFFCA - SOF10",
            "Start Of Frame Marker (Non-Differential Progressive DCT)");
        break;
      case 0xcb:
        new SOFBMarker(reader, marker, "0xFFCB - SOF11",
            "Start Of Frame Marker (Non-Differential Lossless (Sequential))");
        break;
      case 0xcd:
        new SOFDMarker(reader, marker, "SOF13", "Start Of Frame Marker (Differential Sequential DCT)");
        break;
      case 0xce:
        new SOFEMarker(reader, marker, "SOF14", "Start Of Frame Marker (Differential Progressive DCT)");
        break;
      case 0xcf:
        new SOFFMarker(reader, marker, "SOF15", "Start Of Frame Marker (Differential Lossless)");
        break;
      case 0xd8:
        new BaseMarker(reader, "SOI", "Start Of Image Marker", true);
        break;
      case 0xd9:
        new BaseMarker(reader, "EOI", "End Of Image Marker", true);
        break;
      case 0xda:
        new SOSMarker(reader, marker, "SOS", "Start Of Scan Marker");
        new ScansContent(reader);
        break;
      case 0xdb:
        new DQTMarker(reader);
        break;
      case 0xe0:
        new APPxSegment(reader, marker);
        break;
      case 0xe1:
        new APPxSegment(reader, marker);
        break;
      case 0xe2:
        new APPxSegment(reader, marker);
        break;
      case 0xe3:
        new APPxSegment(reader, marker);
        break;
      case 0xe4:
        new APPxSegment(reader, marker);
        break;
      case 0xe5:
        new APPxSegment(reader, marker);
        break;
      case 0xe6:
        new APPxSegment(reader, marker);
        break;
      case 0xe7:
        new APPxSegment(reader, marker);
        break;
      case 0xe8:
        new APPxSegment(reader, marker);
        break;
      case 0xe9:
        new APPxSegment(reader, marker);
        break;
      case 0xea:
        new APPxSegment(reader, marker);
        break;
      case 0xeb:
        new APPxSegment(reader, marker);
        break;
      case 0xec:
        new APPxSegment(reader, marker);
        break;
      case 0xed:
        new APPxSegment(reader, marker);
        break;
      case 0xee:
        new APPxSegment(reader, marker);
        break;
      case 0xef:
        new APPxSegment(reader, marker);
        break;
      case 0xfe:
        new CommentSegment(reader);
        break;
      default:
        String message = "Unknown JPEG marker " + Integer.toHexString(marker) + " at index "
            + reader.getCurrentIndex() + "!";
        System.out.println(message);
        throw new RuntimeException(message);
    }
  }

  @Override
  public DumpBlock getCrossPointerDumpBlocks(CrossPointer crossPointer) {
    return null;
  }

  @Override
  public String toString() {
    return "JPEG image stream.";
  }
}
