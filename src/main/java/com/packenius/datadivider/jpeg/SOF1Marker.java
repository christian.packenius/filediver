package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOF1Marker extends VariableLengthMarker {
  public SOF1Marker(DumpReader reader) {
    super(reader, "SOF1", "Start Of Frame Marker (Extended Sequential DCT)", false);
    readContentBytes("SOF1 marker segment content.");
    setEndAddress(reader);
  }
}
