package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

public class ScansContent extends DumpBlock {
  public ScansContent(DumpReader reader) {
    super(reader);

    // Count scans bytes.
    int k = 0;
    while (true) {
      if (reader.getDeltaByte(k) != (byte) 0xFF) {
        k++;
      } else {
        k++;
        if (reader.getDeltaByte(k) != 0) {
          break;
        }
      }
    }
    reader.readBytes(k - 1, "Image data (scans)");
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Scans content data";
  }
}
