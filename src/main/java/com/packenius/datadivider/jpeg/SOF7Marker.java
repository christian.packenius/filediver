package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOF7Marker extends VariableLengthMarker {
  private final String title;

  public SOF7Marker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF7 - Start Of Frame Marker", false);
    readContentBytes("SOF7 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
