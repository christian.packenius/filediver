package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class DHTMarker extends VariableLengthMarker {
  public DHTMarker(DumpReader reader) {
    super(reader, "DHT", "Define Huffman Table(s)", false);
    readContentBytes("DHT marker segment content.");
    setEndAddress(reader);
  }
}
