package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOF9Marker extends VariableLengthMarker {
  private final String title;

  public SOF9Marker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF9 - Start Of Frame Marker", false);
    readContentBytes("SOF9 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
