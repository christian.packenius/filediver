package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOF2Marker extends VariableLengthMarker {

  public SOF2Marker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF2 - Start Of Frame Marker", false);
    readContentBytes("SOF2 marker segment content.");
    setEndAddress(reader);
  }
}
