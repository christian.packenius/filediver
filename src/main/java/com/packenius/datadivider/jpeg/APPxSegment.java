package com.packenius.datadivider.jpeg;

import com.packenius.datadivider.SubContentCrossPointer;
import com.packenius.dumpapi.CrossPointer;
import com.packenius.dumpapi.DumpReader;

public class APPxSegment extends VariableLengthMarker {
  public APPxSegment(DumpReader reader, int marker) {
    super(reader, "APP" + (marker - 0xe0), "Application marker segment", false);
    if (isJFIF(reader, marker)) {
      readApp0JfifContent(reader);
    } else if (isExif(reader, marker)) {
      readApp1ExifContent(reader);
    } else {
      readContentBytes("APP" + (marker - 0xe0) + "Application marker segment content.");
    }
    setEndAddress(reader);
  }

  private boolean isJFIF(DumpReader reader, int marker) {
    return length > 8 //
        && reader.getDeltaByte(0) == 'J' //
        && reader.getDeltaByte(1) == 'F' //
        && reader.getDeltaByte(2) == 'I' //
        && reader.getDeltaByte(3) == 'F' //
        && reader.getDeltaByte(4) == 0 //
        ;
  }

  private void readApp0JfifContent(DumpReader reader) {
    reader.readBytes(5, "JFIF APP0 block");
    reader.readU1("Major version byte");
    reader.readU1("Minor version byte");
    reader.readU1("Units for X and Y density (0:pixel, 1:inch, 2:cm");
    reader.readBigEndianS2("Horizontal pixel density");
    reader.readBigEndianS2("Vertical pixel density");
    int tpx = reader.readU1("Thumbnail horizontal pixel count").value;
    int tpy = reader.readU1("Thumbnail vertical pixel count").value;
    int remLength = length - 16;

    // There must be 3*x*y thumbnail pixels (3: RGB).
    if (3 * tpx * tpy != remLength) {
      throw new RuntimeException(
          "Wrong number of bytes for thumbnail pixels! Not " + remLength + ", but " + 3 * tpx * tpy);
    }
    reader.readBytes(remLength, "Thumbnail RGB bytes");
  }

  private boolean isExif(DumpReader reader, int marker) {
    return length > 9 //
        && reader.getDeltaByte(0) == 'E' //
        && reader.getDeltaByte(1) == 'x' //
        && reader.getDeltaByte(2) == 'i' //
        && reader.getDeltaByte(3) == 'f' //
        && reader.getDeltaByte(4) == 0 //
        && reader.getDeltaByte(5) == 0 //
        ;
  }

  private void readApp1ExifContent(DumpReader reader) {
    reader.readBytes(6, "Exif APP1 block");
    int remLength = length - 8;
    byte[] tiffBytes = reader.readBytes(remLength, "TIFF thumbnail content");
    CrossPointer tiffCP = new SubContentCrossPointer(tiffBytes);
    setCrossPointer(tiffCP);
  }
}
