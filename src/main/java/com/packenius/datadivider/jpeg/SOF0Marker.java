package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOF0Marker extends VariableLengthMarker {
  public SOF0Marker(DumpReader reader) {
    super(reader, "SOF0", "Start Of Frame Marker (Baseline DCT)", false);
    readContentBytes("SOF0 marker segment content.");
    setEndAddress(reader);
  }
}
