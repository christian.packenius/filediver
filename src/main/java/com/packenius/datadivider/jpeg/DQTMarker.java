package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class DQTMarker extends VariableLengthMarker {
  public DQTMarker(DumpReader reader) {
    super(reader, "DQT", "Define Quantization Table(s)", false);
    readContentBytes("DQT marker segment content.");
    setEndAddress(reader);
  }
}
