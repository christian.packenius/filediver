package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * A JPEG marker that has no other content than the marker itself.
 *
 * @author Christian Packenius, 2019.
 */
public class BaseMarker extends DumpBlock {
  public final int marker;

  public final String name;

  public final String title;

  public BaseMarker(DumpReader reader, String name, String title, boolean isMarkerEnd) {
    super(reader);
    this.name = name;
    this.title = title;
    BigEndianUnsigned2ByteInteger u2 = reader.readBigEndianU2(name);
    this.marker = u2.value & 255;
    if (isMarkerEnd) {
      setEndAddress(reader);
    }
  }

  @Override
  public String toString() {
    return name + " - " + title;
  }
}
