package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOF5Marker extends VariableLengthMarker {
  private final String title;

  public SOF5Marker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF5 - Start Of Frame Marker", false);
    readContentBytes("SOF5 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
