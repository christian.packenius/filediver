package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOSMarker extends VariableLengthMarker {
  private final String title;

  public SOSMarker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOS - Start Of Scan", false);
    readContentBytes("SOS marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
