package com.packenius.datadivider.jpeg;

import com.packenius.dumpapi.DumpReader;

public class SOFFMarker extends VariableLengthMarker {
  private final String title;

  public SOFFMarker(DumpReader reader, int marker, String name, String title) {
    super(reader, name, "SOF15 - Start Of Frame Marker", false);
    readContentBytes("SOF15 marker segment content.");
    this.title = title;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return title;
  }
}
