package com.packenius.datadivider.raw_content;

import com.packenius.dumpapi.CrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.MainDumpBlock;

/**
 * Creates a dump of any byte array or file content.
 *
 * @author Christian Packenius, 2019.
 */
public class RawContentDescription extends MainDumpBlock {
  protected static final int RAW_EXPONENT = 5; // Factor 32

  public RawContentDescription(DumpReader reader) {
    super(reader);

    // Get max block size for main dump block.
    int length = reader.length;
    int blockSize = 0x10000000;
    while (blockSize > length && blockSize > 256) {
      blockSize >>= RAW_EXPONENT;
    }

    // Create dump blocks.
    while (reader.getCurrentIndex() < reader.length) {
      partIt(reader, blockSize);
    }

    // Prüfen, ob das Ende der Klasse erreicht wurde.
    setEndAddress(reader);
  }

  protected static String partIt(DumpReader reader, int block) {
    int _start = reader.getCurrentIndex();
    for (int i = 0; i < (2 ^ RAW_EXPONENT) && reader.getCurrentIndex() < reader.length; i++) {
      if (block == 256) {
        int start = reader.getCurrentIndex();
        int len = Math.min(256, reader.length - start);
        int end = start + len - 1;
        String range = start + " .. " + end;
        reader.readBytes(len, range);
      } else {
        new RawBytesDumpBlock(reader, block);
      }
    }
    int _end = reader.getCurrentIndex();
    return _start + " .. " + _end;
  }

  @Override
  public String toString() {
    return "Raw byte array";
  }

  @Override
  public String getDescription() {
    return "Raw bytes without semantic translation.";
  }

  @Override
  public DumpBlock getCrossPointerDumpBlocks(CrossPointer crossPointer) {
    // There are no cross pointer within raw bytes.
    return null;
  }
}
