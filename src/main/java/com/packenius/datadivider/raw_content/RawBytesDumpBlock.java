package com.packenius.datadivider.raw_content;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

public class RawBytesDumpBlock extends DumpBlock {
  private final String range;

  public RawBytesDumpBlock(DumpReader reader, int blockSize) {
    super(reader);

    blockSize >>= RawContentDescription.RAW_EXPONENT;
    range = RawContentDescription.partIt(reader, blockSize);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return range;
  }
}
