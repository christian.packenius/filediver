package com.packenius.datadivider.javaclass;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

import java.util.ArrayList;
import java.util.List;

/**
 * Methoden einer Klasse.
 *
 * @author Christian Packenius, 2016.
 */
public class Methods extends DumpBlock {
  /**
   * Array sämtlicher Methoden dieser Klasse.
   */
  public final MethodDescription[] methods;

  /**
   * Konstruktor.
   */
  public Methods(DumpReader reader) {
    super(reader);
    int methodsCount = reader.readBigEndianU2("Methods count: ##DEC##").value;
    methods = new MethodDescription[methodsCount];
    for (int i = 0; i < methodsCount; i++) {
      methods[i] = new MethodDescription(reader);
    }
    setEndAddress(reader);
  }

  /**
   * Ermittelt die Methode zur Initialisierung der Klasse (nicht einer Instanz!).
   *
   * @return Methode zur Initialisierung einer Klasse oder <i>null</i>, falls
   * nicht gefunden.
   */
  public MethodDescription getClassInitializationMethod() {
    for (MethodDescription md : methods) {
      if (md.methodName.equals("<clinit>")) {
        return md;
      }
    }
    return null;
  }

  /**
   * Ermittelt an Hand des Namens und des Deskriptors die Methode.
   *
   * @return Methodenbeschreibung oder <i>null</i>, falls nicht gefunden.
   */
  public MethodDescription getMethod(ConstantPool cp, String name, String descriptor) {
    for (MethodDescription method : methods) {
      if (method.getName().equals(name)) {
        if (method.descriptor.equals(descriptor)) {
          return method;
        }
      }
    }
    return null;
  }

  /**
   * Erstellt ein Array sämtlicher Konstruktoren.
   *
   * @param cp Konstantenpool.
   * @return Array aller Konstruktor-Methoden.
   */
  public MethodDescription[] getConstructors(ConstantPool cp) {
    List<MethodDescription> result = new ArrayList<>();
    for (MethodDescription method : methods) {
      if (method.getName().equals("<init>")) {
        result.add(method);
      }
    }
    return result.toArray(new MethodDescription[result.size()]);
  }

  @Override
  public String toString() {
    return "Methods [" + methods.length + " entries]";
  }

  @Override
  public String getDescription() {
    return "List of all methods defined within this class.";
  }
}
