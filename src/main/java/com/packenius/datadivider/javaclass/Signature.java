package com.packenius.datadivider.javaclass;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * Signatur einer Java-*.class-Datei. Sollte bei jeder class-Datei identisch
 * sein.
 *
 * @author Christian Packenius, 2016.
 */
public class Signature extends DumpBlock {
  /**
   * Eigentliche Signatur, steht in den ersten 4 Bytes einer jeden class-Datei.
   */
  public static final long CAFEBABE = 0x00cafebabeL;

  /**
   * Konstruktor.
   *
   * @param reader
   */
  public Signature(DumpReader reader) {
    super(reader);
    long id = reader.readBigEndianU4("Signature: ##HEX4##");
    if (id != CAFEBABE) {
      String msg = "Die Klassendatei enthält eine ungültige Signatur! <";
      msg += Long.toHexString(id);
      msg += ">";
      throw new IllegalArgumentException(msg);
    }
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Signature: cafebabe";
  }

  @Override
  public String getDescription() {
    return "Signature - first 4 bytes of every java class content";
  }
}
