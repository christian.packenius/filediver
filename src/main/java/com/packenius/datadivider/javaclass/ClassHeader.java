package com.packenius.datadivider.javaclass;

import com.packenius.datadivider.javaclass.accflags.ClassAccessFlags;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Allgemeiner Klassenkopf (Name, Oberklasse, Zugriffsflags).
 *
 * @author Christian Packenius, 2016.
 */
public class ClassHeader extends DumpBlock {
  /**
   * Zugriffsflags dieser Klasse.
   */
  public final ClassAccessFlags accessFlags;

  /**
   * Name der Klasse.
   */
  public final int thisClassIndex;

  /**
   * Name der Oberklasse.
   */
  public final int superClassIndex;

  /**
   * Name dieser Klasse.
   */
  public final String thisClassName;

  /**
   * Name der Oberklasse.
   */
  public final String superClassName;

  /**
   * Konstruktor.
   */
  public ClassHeader(DumpReader reader) {
    super(reader);

    ConstantPool cp = reader.getUserObject(ConstantPool.class);

    accessFlags = new ClassAccessFlags(reader);

    BigEndianUnsigned2ByteInteger classIndexDumpBlock = reader.readBigEndianU2("This class index: ###DEC##");
    classIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(classIndexDumpBlock.value));
    thisClassIndex = classIndexDumpBlock.value;

    BigEndianUnsigned2ByteInteger superIndexDumpBlock = reader.readBigEndianU2("Super class index: ###DEC##");
    superIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(superIndexDumpBlock.value));
    superClassIndex = superIndexDumpBlock.value;

    thisClassName = cp.getUtf8Entry(cp.getClassEntry(thisClassIndex).classIndex).string;
    if (superClassIndex != 0) {
      superClassName = cp.getUtf8Entry(cp.getClassEntry(superClassIndex).classIndex).string;
    } else {
      // Das hier darf nur bei java.lang.Object vorkommen!
      superClassName = null;
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Class Header";
//        if (superClassName != null) {
//            return "Class Header: " + accessFlags.toString() + " " + thisClassName + " extends " + superClassName;
//        } else {
//            return "Class Header: " + accessFlags.toString() + " " + thisClassName;
//        }
  }

  @Override
  public String getDescription() {
    return "Description off this classes name, its super type and its access flags.";
  }
}
