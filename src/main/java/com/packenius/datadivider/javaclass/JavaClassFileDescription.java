package com.packenius.datadivider.javaclass;

import com.packenius.dumpapi.CrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.MainDumpBlock;

/**
 * Creates a dump for a java class file.
 *
 * @author Christian Packenius, 2016.
 */
public class JavaClassFileDescription extends MainDumpBlock {
  /**
   * Lädt eine Klasse an Hand ihres Inhaltes.
   *
   * @param content Inhalt der Klasse(ndatei) als Bytearray.
   */
  public JavaClassFileDescription(DumpReader reader) {
    super(reader);

    reader.setUserObject(new Signature(reader));
    reader.setUserObject(new Version(reader));
    reader.setUserObject(new ConstantPool(reader));
    reader.setUserObject(new ClassHeader(reader));
    reader.setUserObject(new Interfaces(reader));
    reader.setUserObject(new Fields(reader));
    reader.setUserObject(new Methods(reader));
    reader.setUserObject(new Attributes(reader));

    // Prüfen, ob das Ende der Klasse erreicht wurde.
    setEndAddress(reader);
    checkEndOfData(reader);
  }

  /**
   * Prüft, ob beim ClassReader das Ende des Datenstroms erreicht wurde.
   *
   * @param reader Klassen-Reader.
   */
  private void checkEndOfData(DumpReader reader) {
    if (!reader.isEOF()) {
      throw new IllegalArgumentException("Hinter den class-Daten stehen weitere Bytes!");
    }
  }

  /**
   * Ermittelt den Namen der beschriebenen Klasse.
   *
   * @return Klassenname, voll qualifiziert.
   */
  public String getName() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    ClassHeader classHeader = reader.getUserObject(ClassHeader.class);
    return cp.getClassEntry(classHeader.thisClassIndex).getClassName();
  }

  @Override
  public String toString() {
    return "Java class file";
  }

  public MethodDescription getMethod(String name, String descriptor) {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    return reader.getUserObject(Methods.class).getMethod(cp, name, descriptor);
  }

  @Override
  public String getDescription() {
    return "A java class file is executable within a java virtual machine.";
  }

  @Override
  public DumpBlock getCrossPointerDumpBlocks(CrossPointer crossPointer) {
    if (crossPointer instanceof ConstantPoolEntryCrossPointer) {
      ConstantPoolEntryCrossPointer cpecp = (ConstantPoolEntryCrossPointer) crossPointer;
      int cpeIndex = cpecp.constantPoolEntryIndex;
      return reader.getUserObject(ConstantPool.class).getEntry(cpeIndex);
    }
    if (crossPointer != null) {
      throw new RuntimeException("Unknown cross pointer type <" + crossPointer.getClass() + ">!");
    }
    return null;
  }

  public String dump() {
    StringBuilder sb = new StringBuilder(reader.length * 4);
    dump(sb, null, false);
    return sb.toString();
  }
}
