package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class LocalVarTarget extends TargetInfo {
  public final LocalVar[] localVars;

  /**
   * Konstruktor.
   */
  public LocalVarTarget(DumpReader reader, String kind) {
    super(reader, kind);

    int count = reader.readBigEndianU2("Local vars count: ##DEC##").value;

    localVars = new LocalVar[count];
    for (int i = 0; i < count; i++) {
      localVars[i] = new LocalVar(reader);
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Local Var Target";
  }
}
