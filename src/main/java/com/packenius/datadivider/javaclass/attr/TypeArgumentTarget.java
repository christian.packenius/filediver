package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class TypeArgumentTarget extends TargetInfo {
  public final int offset;

  public final short typeArgumentIndex;

  /**
   * Konstruktor.
   */
  public TypeArgumentTarget(DumpReader reader, String kind) {
    super(reader, kind);

    offset = reader.readBigEndianU2("Offset: ##DEC##").value;
    typeArgumentIndex = reader.readU1("Type argument index: ###DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Type Argument Target";
  }

  @Override
  public String getDescription() {
    return "The type_argument_target item indicates that an annotation appears either on "
        + "the i'th type in a cast expression, or on the i'th type argument in the explicit type "
        + "argument list for any of the following: a new expression, an explicit constructor "
        + "invocation statement, a method invocation expression, or a method reference expression.";
  }
}
