package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.CpUtf8;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Attribut mit der Darstellung einer Signatur.
 *
 * @author Christian Packenius, 2016.
 */
public class SignatureAttribute extends AttributeDescription {
  private final int signatureIndex;

  public final CpUtf8 signature;

  /**
   * Konstruktor.
   */
  public SignatureAttribute(int length, DumpReader reader) {
    super(reader, length);

    ConstantPool cp = reader.getUserObject(ConstantPool.class);

    if (length != 2) {
      throw new RuntimeException("Signature attribute must have length of 2!");
    }

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Signature index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    signatureIndex = indexDumpBlock.value;

    signature = cp.getUtf8Entry(signatureIndex);

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "Signature";
  }

  @Override
  public String toString() {
    return "Signature Attribute";
  }

  @Override
  public String getDescription() {
    return "The Signature attribute is a fixed-length attribute in the attributes table "
        + "of a ClassFile, field_info, or method_info structure.";
  }
}
