package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class StackMap_FullFrame extends StackMapFrame {
  public final int offsetDelta;

  public final int numberOfLocals;

  public final StackMap_VerificationTypeInfo[] locals;

  public final int numberOfStackItems;

  public final StackMap_VerificationTypeInfo[] stackItems;

  /**
   * Konstruktor.
   */
  public StackMap_FullFrame(DumpReader reader) {
    super(reader);

    short tag = reader.readU1("Tag: ##DEC##").value;
    if (tag != 255) {
      throw new RuntimeException("Tag != 255 (FULL_FRAME)!");
    }

    offsetDelta = reader.readBigEndianU2("Offset delta: ##DEC##").value;

    numberOfLocals = reader.readBigEndianU2("Number of locals: ##DEC##").value;
    locals = new StackMap_VerificationTypeInfo[numberOfLocals];
    for (int i = 0; i < numberOfLocals; i++) {
      locals[i] = new StackMap_VerificationTypeInfo(reader);
    }

    numberOfStackItems = reader.readBigEndianU2("Number of stack items: ##DEC##").value;
    stackItems = new StackMap_VerificationTypeInfo[numberOfStackItems];
    for (int i = 0; i < numberOfStackItems; i++) {
      stackItems[i] = new StackMap_VerificationTypeInfo(reader);
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Full Frame";
  }
}
