package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class SourceDebugExtensionAttribute extends AttributeDescription {
  /**
   * Beliebige Bytes als Info zum Debugging.
   */
  public final byte[] debugExtension;

  /**
   * Konstruktor.
   */
  public SourceDebugExtensionAttribute(int length, DumpReader reader) {
    super(reader, length);

    if (length > 0) {
      debugExtension = reader.readBytes(length, "Debugging information");
    } else {
      debugExtension = new byte[0];
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "SourceDebugExtension";
  }

  @Override
  public String toString() {
    return "Source Debug Extension Attribute";
  }

  @Override
  public String getDescription() {
    return "The SourceDebugExtension attribute is an optional attribute in the attributes "
        + "table of a ClassFile structure.";
  }
}
