package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class InnerClass extends DumpBlock {
  public final int innerClassInfoIndex;

  public final int outerClassInfoIndex;

  public final int innerNameIndex;

  public final int innerClassAccessFlags;

  /**
   * Konstruktor.
   */
  public InnerClass(DumpReader reader, int i) {
    super(reader);

    BigEndianUnsigned2ByteInteger icDumpBlock = reader.readBigEndianU2("Inner class info index: ###DEC##");
    icDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(icDumpBlock.value));
    innerClassInfoIndex = icDumpBlock.value;

    BigEndianUnsigned2ByteInteger ocDumpBlock = reader.readBigEndianU2("Outer class info index: ###DEC##");
    ocDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(ocDumpBlock.value));
    outerClassInfoIndex = ocDumpBlock.value;

    BigEndianUnsigned2ByteInteger iniDumpBlock = reader.readBigEndianU2("Inner name index: ###DEC##");
    iniDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(iniDumpBlock.value));
    innerNameIndex = iniDumpBlock.value;

    BigEndianUnsigned2ByteInteger icafDumpBlock = reader.readBigEndianU2("Inner class access flags: ###BIN2##");
    icafDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(icafDumpBlock.value));
    innerClassAccessFlags = icafDumpBlock.value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Inner Class";
  }
}
