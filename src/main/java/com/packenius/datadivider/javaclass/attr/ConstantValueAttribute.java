package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.ConstantPoolConst;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Attribut mit der Darstellung eines konstanten Wertes.
 *
 * @author Christian Packenius, 2016.
 */
public class ConstantValueAttribute extends AttributeDescription {
  private final int index;

  /**
   * Dargestellter Konstanter Wert aus dem Konstantenpool.
   */
  public final ConstantPoolConst constValue;

  /**
   * Konstruktor.
   */
  public ConstantValueAttribute(int length, DumpReader reader) {
    super(reader, length);

    ConstantPool cp = reader.getUserObject(ConstantPool.class);

    if (length != 2) {
      throw new RuntimeException("ConstantValue attribute must have length of 2!");
    }

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Const value index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    constValue = (ConstantPoolConst) cp.getEntry(index);

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "ConstantValue";
  }

  @Override
  public String toString() {
    return "Constant Value Attribute";
  }

  @Override
  public String getDescription() {
    return "The ConstantValue attribute is a fixed-length attribute in the attributes table of "
        + "a field_info structure. A ConstantValue attribute represents the value of "
        + "a constant expression.";
  }
}
