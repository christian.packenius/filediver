package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class LocalVariableType extends DumpBlock {
  public final int startPC;

  public final int length;

  public final int nameIndex;

  public final int signatureIndex;

  public final int index;

  /**
   * Konstruktor.
   */
  public LocalVariableType(DumpReader reader, int i) {
    super(reader);

    startPC = reader.readBigEndianU2("Start PC: ##DEC##").value;

    length = reader.readBigEndianU2("Length: ##DEC##").value;

    BigEndianUnsigned2ByteInteger nameIndexBlock = reader.readBigEndianU2("Name index: ###DEC##");
    nameIndexBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(nameIndexBlock.value));
    nameIndex = nameIndexBlock.value;

    BigEndianUnsigned2ByteInteger signIndexBlock = reader.readBigEndianU2("Signature index: ###DEC##");
    signIndexBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(signIndexBlock.value));
    signatureIndex = signIndexBlock.value;

    index = reader.readBigEndianU2("Local variable index: ###DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Local Variable Type";
  }
}
