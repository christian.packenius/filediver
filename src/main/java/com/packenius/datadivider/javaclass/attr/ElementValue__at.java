package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class ElementValue__at extends ElementValue {
  /**
   * Eingebettete Annotation.
   */
  public final Annotation annotationValue;

  /**
   * Konstruktor.
   */
  public ElementValue__at(DumpReader reader, char tag) {
    super(reader, tag);

    annotationValue = new Annotation(reader);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Element Value Annotation";
  }
}
