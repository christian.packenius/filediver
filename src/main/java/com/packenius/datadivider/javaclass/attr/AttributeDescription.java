package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Allgemeine und abstrakte Beschreibung eines Attributes.
 *
 * @author Christian Packenius, 2016.
 */
public abstract class AttributeDescription extends DumpBlock {
  /**
   * Länge des Datenblocks des Attributes.
   */
  public final int length;

  /**
   * Index auf den Namen des Attributes im Konstantenpool.
   */
  public final int utf8Index;

  /**
   * Konstruktor.
   *
   * @param length Länge des Datenblocks des Attributes.
   */
  public AttributeDescription(DumpReader reader, int length) {
    super(reader);
    this.length = length;

    BigEndianUnsigned2ByteInteger utf8IndexDumpBlock = reader.readBigEndianU2("Attribute name index: ###DEC##");
    utf8IndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(utf8IndexDumpBlock.value));
    utf8Index = utf8IndexDumpBlock.value;

    reader.readBigEndianU4("Attribute length: ##DEC##"); // Länge überlesen.
  }

  /**
   * Gibt den Namen des Attributes zurück.
   *
   * @return Attributname.
   */
  public abstract String getName();

  @Override
  public String toString() {
    return "Attribute Description";
  }
}
