package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class ElementValue__e extends ElementValue {
  public final int enumTypeNameIndex;

  public final int enumConstNameIndex;

  /**
   * Konstruktor.
   */
  public ElementValue__e(DumpReader reader, char tag) {
    super(reader, tag);

    BigEndianUnsigned2ByteInteger index1DumpBlock = reader.readBigEndianU2("Enum type name index: ###DEC##");
    index1DumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(index1DumpBlock.value));
    enumTypeNameIndex = index1DumpBlock.value;

    BigEndianUnsigned2ByteInteger index2DumpBlock = reader.readBigEndianU2("Enum const name index: ###DEC##");
    index2DumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(index2DumpBlock.value));
    enumConstNameIndex = index2DumpBlock.value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Element Value Enum";
  }
}
