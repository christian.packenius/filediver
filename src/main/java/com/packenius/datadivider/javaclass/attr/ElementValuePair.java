package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class ElementValuePair extends DumpBlock {
  /**
   * Index auf den Konstantenpool mit dem Elementnamen als UTF8-Eintrag.
   */
  public final int elementNameIndex;

  /**
   * Der zu obigem Namen zugehörige Wert.
   */
  public final ElementValue elementValue;

  /**
   * Konstruktor.
   */
  public ElementValuePair(DumpReader reader) {
    super(reader);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Element name index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    elementNameIndex = indexDumpBlock.value;

    elementValue = ElementValueFactory.getElementValue(reader);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Element Value Pair";
  }
}
