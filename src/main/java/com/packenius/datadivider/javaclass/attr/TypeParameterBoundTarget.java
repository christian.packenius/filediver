package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class TypeParameterBoundTarget extends TargetInfo {
  public final short typeParameterIndex;

  public final short boundIndex;

  /**
   * Konstruktor.
   */
  public TypeParameterBoundTarget(DumpReader reader, String kind) {
    super(reader, kind);

    typeParameterIndex = reader.readU1("Type parameter index: ###DEC##").value;
    boundIndex = reader.readU1("Bound index: ###DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Type Parameter Bound Target";
  }

  @Override
  public String getDescription() {
    return "The type_parameter_bound_target item indicates that an annotation appears "
        + "on the i'th bound of the j'th type parameter declaration of a generic class, "
        + "interface, method, or constructor.";
  }
}
