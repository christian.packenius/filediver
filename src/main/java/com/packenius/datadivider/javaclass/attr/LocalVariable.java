package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class LocalVariable extends DumpBlock {
  /**
   * Index innerhalb der entsprechenden Tabelle.
   */
  public final int id;

  public final int startPC;

  public final int length;

  public final int nameIndex;

  public final int descriptorIndex;

  public final int index;

  /**
   * Konstruktor.
   */
  public LocalVariable(DumpReader reader, int id) {
    super(reader);

    this.id = id;

    startPC = reader.readBigEndianU2("Start PC: ##DEC##").value;
    length = reader.readBigEndianU2("Length: ##DEC##").value;

    BigEndianUnsigned2ByteInteger nameDumpBlock = reader.readBigEndianU2("Name index: ###DEC##");
    nameDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(nameDumpBlock.value));
    nameIndex = nameDumpBlock.value;

    BigEndianUnsigned2ByteInteger descDumpBlock = reader.readBigEndianU2("Descriptor index: ###DEC##");
    descDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(descDumpBlock.value));
    descriptorIndex = descDumpBlock.value;

    index = reader.readBigEndianU2("Local variable index: ###DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Local Variable";
  }
}
