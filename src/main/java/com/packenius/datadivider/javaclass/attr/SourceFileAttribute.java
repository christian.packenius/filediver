package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Attribut mit der Darstellung einer Source-Datei.
 *
 * @author Christian Packenius, 2016.
 */
public class SourceFileAttribute extends AttributeDescription {
  public final int index;

  /**
   * Konstruktor.
   */
  public SourceFileAttribute(int length, DumpReader reader) {
    super(reader, length);

    if (length != 2) {
      throw new RuntimeException("SourceFile attribute must have length of 2!");
    }

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Source file index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "SourceFile";
  }

  @Override
  public String toString() {
    return "Source File: " + reader.getUserObject(ConstantPool.class).getEntry(index).asUtf8().string;
  }

  @Override
  public String getDescription() {
    return "Name of the source file. When source is written in an other JVM language than "
        + "java, file type can be other than '.java'.";
  }
}
