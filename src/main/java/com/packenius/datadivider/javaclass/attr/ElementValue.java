package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public abstract class ElementValue extends DumpBlock {
  /**
   * Einzelnes Zeichen: <br>
   * 'B' byte const_value_index CONSTANT_Integer<br>
   * 'C' char const_value_index CONSTANT_Integer<br>
   * 'D' double const_value_index CONSTANT_Double<br>
   * 'F' float const_value_index CONSTANT_Float<br>
   * 'I' int const_value_index CONSTANT_Integer<br>
   * 'J' long const_value_index CONSTANT_Long<br>
   * 'S' short const_value_index CONSTANT_Integer<br>
   * 'Z' boolean const_value_index CONSTANT_Integer<br>
   * 's' String const_value_index CONSTANT_Utf8<br>
   * 'e' Enum type enum_const_value Not applicable<br>
   * 'c' Class class_info_index Not applicable<br>
   * '@' Annotation type annotation_value Not applicable<br>
   * '[' Array type array_value Not applicable<br>
   */
  public final char tag;

  /**
   * Konstruktor.
   */
  public ElementValue(DumpReader reader, char tag) {
    super(reader);

    reader.readU1("Tag: '##CHAR##'"); // Tag überlesen.
    this.tag = tag;
  }
}
