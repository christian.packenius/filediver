package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class RuntimeVisibleAnnotationsAttribute extends AttributeDescription {
  public final Annotation[] annotations;

  /**
   * Konstruktor.
   */
  public RuntimeVisibleAnnotationsAttribute(int length, DumpReader reader) {
    super(reader, length);

    int count = reader.readBigEndianU2("Annotations count: ##DEC##").value;

    annotations = new Annotation[count];
    for (int i = 0; i < count; i++) {
      annotations[i] = new Annotation(reader);
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "RuntimeVisibleAnnotations";
  }

  @Override
  public String toString() {
    return "Runtime Visible Annotations Attribute";
  }

  @Override
  public String getDescription() {
    return "The RuntimeVisibleAnnotations attribute records run-time visible "
        + "annotations on the declaration of the corresponding class, field, or method. The "
        + "Java Virtual Machine must make these annotations available so they can be "
        + "returned by the appropriate reflective APIs.";
  }
}
