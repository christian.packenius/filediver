package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class StackMap_SameLocals1StackItemFrameExtended extends StackMapFrame {
  public final short tag;

  public final int offsetDelta;

  public final StackMap_VerificationTypeInfo stackItem;

  /**
   * Konstruktor.
   */
  public StackMap_SameLocals1StackItemFrameExtended(DumpReader reader) {
    super(reader);

    tag = reader.readU1("Tag: ##DEC##").value;
    if (tag != 247) {
      throw new RuntimeException("Tag != 247 (SAME_LOCALS_1_STACK_ITEM_FRAME)!");
    }
    offsetDelta = reader.readBigEndianU2("Offset delta: ##DEC##").value;
    stackItem = new StackMap_VerificationTypeInfo(reader);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Same Locals 1 Stack Item Frame Extended";
  }
}
