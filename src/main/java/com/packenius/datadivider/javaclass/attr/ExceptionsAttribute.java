package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class ExceptionsAttribute extends AttributeDescription {
  /**
   * Array mit Ausnahmen.
   */
  public final int[] exceptionEntryIDs;

  /**
   * Konstruktor.
   */
  public ExceptionsAttribute(int length, DumpReader reader) {
    super(reader, length);

    int count = reader.readBigEndianU2("Exception entries count: ##DEC##").value;

    exceptionEntryIDs = new int[count];
    for (int i = 0; i < count; i++) {
      exceptionEntryIDs[i] = reader.readBigEndianU2("Exception index: ###DEC##").value;
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "Exceptions";
  }

  @Override
  public String toString() {
    return "Exceptions Attribute";
  }

  @Override
  public String getDescription() {
    return "The Exceptions attribute is a variable-length attribute in the attributes table of a "
        + "method_info structure. The Exceptions attribute indicates which checked "
        + "exceptions a method may throw. "
        + "There may be at most one Exceptions attribute in the attributes table of a "
        + "method_info structure.";
  }
}
