package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class StackMap_ChopFrame extends StackMapFrame {
  public final short tag;

  public final int offsetDelta;

  /**
   * Konstruktor.
   */
  public StackMap_ChopFrame(DumpReader reader) {
    super(reader);

    tag = reader.readU1("Tag: ##DEC##").value;
    if (tag < 248 || tag > 250) {
      throw new RuntimeException("Tag != 248..250 (CHOP)!");
    }

    offsetDelta = reader.readBigEndianU2("Offset delta: ##DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Chop Frame";
  }
}
