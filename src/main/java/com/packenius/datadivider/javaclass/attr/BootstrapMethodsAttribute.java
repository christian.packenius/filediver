package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class BootstrapMethodsAttribute extends AttributeDescription {
  /**
   * Array mit Zeilennummereinträgen.
   */
  public final BootstrapMethod[] bootstrapMethods;

  /**
   * Konstruktor.
   */
  public BootstrapMethodsAttribute(int length, DumpReader reader) {
    super(reader, length);

    int count = reader.readBigEndianU2("Bootstrap methods count: ##DEC##").value;

    bootstrapMethods = new BootstrapMethod[count];
    for (int i = 0; i < count; i++) {
      bootstrapMethods[i] = new BootstrapMethod(reader, i);
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "BootstrapMethods";
  }

  @Override
  public String toString() {
    return "Bootstrap Methods Attribute";
  }

  @Override
  public String getDescription() {
    return "The BootstrapMethods attribute is a variable-length attribute in the attributes "
        + "table of a ClassFile structure. The BootstrapMethods attribute "
        + "records bootstrap method specifiers referenced by invokedynamic instructions. "
        + "There must be exactly one BootstrapMethods attribute in the attributes table of "
        + "a ClassFile structure if the constant_pool table of the ClassFile structure has "
        + "at least one CONSTANT_InvokeDynamic_info entry. "
        + "There may be at most one BootstrapMethods attribute in the attributes table of "
        + "a ClassFile structure.";
  }
}
