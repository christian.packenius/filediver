package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class RuntimeInvisibleAnnotationsAttribute extends AttributeDescription {
  public final Annotation[] annotations;

  /**
   * Konstruktor.
   */
  public RuntimeInvisibleAnnotationsAttribute(int length, DumpReader reader) {
    super(reader, length);

    int count = reader.readBigEndianU2("Annotations count: ##DEC##").value;

    annotations = new Annotation[count];
    for (int i = 0; i < count; i++) {
      annotations[i] = new Annotation(reader);
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "RuntimeInvisibleAnnotations";
  }

  @Override
  public String toString() {
    return "Runtime Invisible Annotations Attribute";
  }

  @Override
  public String getDescription() {
    return "The RuntimeInvisibleAnnotations attribute records run-time "
        + "invisible annotations on the declaration of the corresponding class, method, or field. "
        + "There may be at most one RuntimeInvisibleAnnotations attribute in the "
        + "attributes table of a ClassFile, field_info, or method_info structure.";
  }
}
