package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class StackMap_VerificationTypeInfo extends DumpBlock {
  public final short tag;

  /**
   * Eintrag im Konstantenpool.
   */
  public final int cpIndex;

  /**
   * Offset in den Code.
   */
  public final int offset;

  /**
   * Konstruktor.
   */
  public StackMap_VerificationTypeInfo(DumpReader reader) {
    super(reader);

    tag = reader.readU1("Tag: ##DEC##").value;
    switch (tag) {
      case 0: // ITEM_Top
      case 1: // ITEM_Integer
      case 2: // ITEM_Float
      case 5: // ITEM_Null
      case 6: // ITEM_UnitializedThis
        cpIndex = offset = -1;
        break;
      case 7: // ITEM_Object
        cpIndex = reader.readBigEndianU2("Object item index: ###DEC##").value;
        offset = -1;
        break;
      case 8: // ITEM_Unitialized
        offset = reader.readBigEndianU2("Offset: ##DEC##").value;
        cpIndex = -1;
        break;
      case 3: // ITEM_Double
      case 4: // ITEM_Long
        cpIndex = offset = -1;
        break;
      default:
        throw new RuntimeException("vti: " + tag);
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Verification Type Info";
  }
}
