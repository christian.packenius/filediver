package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class TypePathElement extends DumpBlock {
  public final short typePathKind;

  public final short typeArgumentIndex;

  /**
   * Konstruktor.
   */
  public TypePathElement(DumpReader reader) {
    super(reader);

    typePathKind = reader.readU1("Type path kind: ###DEC##").value;
    typeArgumentIndex = reader.readU1("Type argument index: ###DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String getDescription() {
    return "Wherever a type is used in a declaration or expression, the type_path structure "
        + "identifies which part of the type is annotated.";
  }

  @Override
  public String toString() {
    return "Type Path Element";
  }
}
