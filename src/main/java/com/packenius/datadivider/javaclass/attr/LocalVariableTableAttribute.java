package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class LocalVariableTableAttribute extends AttributeDescription {
  /**
   * Array der lokalen Variablen.
   */
  public final LocalVariable[] localVariables;

  /**
   * Konstruktor.
   */
  public LocalVariableTableAttribute(int length, DumpReader reader) {
    super(reader, length);

    int count = reader.readBigEndianU2("Local variables count: ##DEC##").value;

    localVariables = new LocalVariable[count];
    for (int i = 0; i < count; i++) {
      localVariables[i] = new LocalVariable(reader, i);
    }
    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "LocalVariableTable";
  }

  @Override
  public String toString() {
    return "Local Variable Table Attribute";
  }

  @Override
  public String getDescription() {
    return "The LocalVariableTable attribute is an optional variable-length attribute in the "
        + "attributes table of a Code attribute. It may be used by debuggers to "
        + "determine the value of a given local variable during the execution of a method. "
        + "If multiple LocalVariableTable attributes are present in the attributes table of "
        + "a Code attribute, then they may appear in any order. "
        + "There may be no more than one LocalVariableTable attribute per local variable "
        + "in the attributes table of a Code attribute.";
  }
}
