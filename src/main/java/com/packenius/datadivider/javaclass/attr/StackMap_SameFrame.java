package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class StackMap_SameFrame extends StackMapFrame {
  public final short tag;

  /**
   * Konstruktor.
   */
  public StackMap_SameFrame(DumpReader reader) {
    super(reader);

    tag = reader.readU1("Tag: ##DEC##").value;
    if (tag > 63) {
      throw new RuntimeException("Tag > 63 (SAME)!");
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Same Frame";
  }
}
