package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class StackMap_SameFrameExtended extends StackMapFrame {
  public final int offsetDelta;

  /**
   * Konstruktor.
   */
  public StackMap_SameFrameExtended(DumpReader reader) {
    super(reader);

    short tag = reader.readU1("Tag: ##DEC##").value;
    if (tag != 251) {
      throw new RuntimeException("Tag != 251 (SAME_FRAME_EXTENDED)!");
    }
    offsetDelta = reader.readBigEndianU2("Offset delta: ##DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Same Frame Extended";
  }
}
