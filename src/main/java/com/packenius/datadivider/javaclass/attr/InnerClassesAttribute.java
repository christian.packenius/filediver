package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class InnerClassesAttribute extends AttributeDescription {
  /**
   * Array mit Zeilennummereinträgen.
   */
  public final InnerClass[] innerClasses;

  /**
   * Konstruktor.
   */
  public InnerClassesAttribute(int length, DumpReader reader) {
    super(reader, length);

    int count = reader.readBigEndianU2("Inner classes count: ##DEC##").value;

    innerClasses = new InnerClass[count];
    for (int i = 0; i < count; i++) {
      innerClasses[i] = new InnerClass(reader, i);
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "InnerClasses";
  }

  @Override
  public String toString() {
    return "Inner Classes Attribute";
  }

  @Override
  public String getDescription() {
    return "The InnerClasses attribute is a variable-length attribute in the attributes table "
        + "of a ClassFile structure. " + "If the constant pool of a class or interface C contains at least one "
        + "CONSTANT_Class_info entry which represents a class or interface that is "
        + "not a member of a package, then there must be exactly one InnerClasses attribute "
        + "in the attributes table of the ClassFile structure for C.";
  }
}
