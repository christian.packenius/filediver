package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class StackMap_AppendFrame extends StackMapFrame {
  public final short tag;

  public final int offsetDelta;

  public final StackMap_VerificationTypeInfo[] locals;

  /**
   * Konstruktor.
   */
  public StackMap_AppendFrame(DumpReader reader) {
    super(reader);

    tag = reader.readU1("Tag: ##DEC##").value;
    if (tag < 252 || tag > 254) {
      throw new RuntimeException("Tag != 252..254 (APPEND_FRAME)!");
    }

    offsetDelta = reader.readBigEndianU2("Offset delta: ##DEC##").value;

    int localsCount = tag - 251;

    locals = new StackMap_VerificationTypeInfo[localsCount];
    for (int i = 0; i < localsCount; i++) {
      locals[i] = new StackMap_VerificationTypeInfo(reader);
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Append Frame";
  }
}
