package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.Attributes;
import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.instr.CodeTranslation;
import com.packenius.datadivider.javaclass.instr.JvmInstruction;
import com.packenius.dumpapi.DumpReader;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Attribut mit der Darstellung von Methoden-Code.
 *
 * @author Christian Packenius, 2016.
 */
public class CodeAttribute extends AttributeDescription {
  /**
   * Maximale Stack-Größe.
   */
  public final int maxStack;

  /**
   * Maximale Anzahl an 'Lokalen'.
   */
  public final int maxLocals;

  /**
   * Liste aller Exception-Blöcke.
   */
  public final List<ExceptionBlockEntry> exceptionBlocks = new ArrayList<>();

  /**
   * Liste sämtlicher Attribute.
   */
  private final Attributes attributes;

  /**
   * Tabelle, mit Hilfe derer jeder Instruktion im Code eine Zeilennummer im
   * Sourcecode zugewiesen werden kann.
   */
  public List<LineNumber> lineNumbers = new ArrayList<LineNumber>();

  /**
   * Tabelle, in der enthalten ist, welche lokale Variable an welcher Stelle wie
   * Verwendung findet.
   */
  public List<LocalVariable> localVariables = new ArrayList<LocalVariable>();

  /**
   * Tabelle, in der enthalten ist, welche lokale Variablentypen an welcher Stelle
   * wie Verwendung findet.
   */
  public List<LocalVariableType> localVariableTypes = new ArrayList<LocalVariableType>();

  /**
   * Hilfstabelle zur schnelleren Typprüfung.
   */
  public StackMapTableAttribute stackMapTableAttribute = null;

  /**
   * Eigentlicher JVM-ByteCode.
   */
  public final List<JvmInstruction> instructions;

  public TypeAnnotation[] runtimeInvisibleAnnotations;

  public TypeAnnotation[] runtimeVisibleAnnotations;

  public TypeAnnotation[] runtimeInvisibleTypeAnnotations;

  public TypeAnnotation[] runtimeVisibleTypeAnnotations;

  private final int codeLength;

  /**
   * Konstruktor.
   *
   * @param length Länge des Code-Attribut-Block-Inhaltes.
   */
  public CodeAttribute(int length, DumpReader reader) {
    super(reader, length);

    ConstantPool cp = reader.getUserObject(ConstantPool.class);

    int startIndex = reader.getCurrentIndex();

    maxStack = reader.readBigEndianU2("Max stack size: ##DEC##").value;
    maxLocals = reader.readBigEndianU2("Max locals size: ##DEC##").value;

    // Den eigentlichen Code der Methode einlesen.
    {
      codeLength = (int) reader.readBigEndianU4("Code length: ##DEC##");
      instructions = CodeTranslation.translate(codeLength, reader, cp);
    }

    // Einträge der Exception Table.
    {
      int exceptionTableLength = reader.readBigEndianU2("Exception table entries count: ##DEC##").value;
      for (int i = exceptionTableLength; i > 0; i--) {
        exceptionBlocks.add(new ExceptionBlockEntry(reader, cp));
      }
    }

    // Weitere Attribute des Codes.
    {
      attributes = new Attributes(reader);

      for (int i = 0; i < attributes.attributes.size(); i++) {
        AttributeDescription attribute = attributes.attributes.get(i);

        if (attribute instanceof LineNumberTableAttribute) {
          lineNumbers.addAll(asList(((LineNumberTableAttribute) attribute).lineNumbers));
        } else if (attribute instanceof LocalVariableTableAttribute) {
          localVariables.addAll(asList(((LocalVariableTableAttribute) attribute).localVariables));
        } else if (attribute instanceof LocalVariableTypeTableAttribute) {
          localVariableTypes.addAll(asList(((LocalVariableTypeTableAttribute) attribute).localVariableTypes));
        } else if (attribute instanceof StackMapTableAttribute) {
          stackMapTableAttribute = (StackMapTableAttribute) attribute;
        } else if (attribute instanceof RuntimeVisibleTypeAnnotationsAttribute) {
          runtimeVisibleTypeAnnotations = ((RuntimeVisibleTypeAnnotationsAttribute) attribute).annotations;
        } else if (attribute instanceof RuntimeInvisibleTypeAnnotationsAttribute) {
          runtimeInvisibleTypeAnnotations = ((RuntimeInvisibleTypeAnnotationsAttribute) attribute).annotations;
        }
      }
    }

    int endIndex = reader.getCurrentIndex();
    if (startIndex + length != endIndex) {
      throw new RuntimeException(
          "startIndex[" + startIndex + "]+length[" + length + "]!=endIndex[" + endIndex + "]!");
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "Code";
  }

  @Override
  public String toString() {
    return "Code Attribute";
  }

  @Override
  public String getDescription() {
    return "A code attribute contains the Java Virtual Machine instructions and "
        + "auxiliary information for a method, including an instance "
        + "initialization method or a class or interface initialization method.";
  }
}
