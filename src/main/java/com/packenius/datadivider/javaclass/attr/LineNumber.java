package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class LineNumber extends DumpBlock {
  /**
   * Start-ProgramCounter.
   */
  public final int startPC;

  /**
   * Zeilennummer.
   */
  public final int lineNumber;

  /**
   * Konstruktor.
   */
  public LineNumber(DumpReader reader, int id) {
    super(reader);
    startPC = reader.readBigEndianU2("Start PC: ##DEC##").value;
    lineNumber = reader.readBigEndianU2("Line number: ##DEC##").value;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Line Number " + lineNumber + " starting at " + startPC;
  }
}
