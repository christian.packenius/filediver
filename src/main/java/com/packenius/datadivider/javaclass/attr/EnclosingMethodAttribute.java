package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class EnclosingMethodAttribute extends AttributeDescription {
  public final int classIndex;

  public final int methodIndex;

  /**
   * Konstruktor.
   */
  public EnclosingMethodAttribute(int length, DumpReader reader) {
    super(reader, length);

    BigEndianUnsigned2ByteInteger classIndexDumpBlock = reader.readBigEndianU2("Class index: ###DEC##");
    classIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(classIndexDumpBlock.value));
    classIndex = classIndexDumpBlock.value;

    BigEndianUnsigned2ByteInteger methodIndexDumpBlock = reader.readBigEndianU2("Method index: ###DEC##");
    methodIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(methodIndexDumpBlock.value));
    methodIndex = methodIndexDumpBlock.value;

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "EnclosingMethod";
  }

  @Override
  public String toString() {
    return "Enclosing Method Attribute";
  }
}
