package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class AnnotationDefaultAttribute extends AttributeDescription {
  /**
   * Default value.
   */
  public final ElementValue defaultValue;

  /**
   * Konstruktor.
   */
  public AnnotationDefaultAttribute(int length, DumpReader reader) {
    super(reader, length);

    defaultValue = ElementValueFactory.getElementValue(reader);

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "AnnotationDefault";
  }

  @Override
  public String toString() {
    return "Annotation Default Attribute";
  }

  @Override
  public String getDescription() {
    return "The AnnotationDefault attribute is a variable-length attribute in the attributes "
        + "table of certain method_info structures, namely those representing elements "
        + "of annotation types. The AnnotationDefault attribute records the "
        + "default value for the element represented by the method_info "
        + "structure. The Java Virtual Machine must make this default value available so it "
        + "can be applied by appropriate reflective APIs.";
  }
}
