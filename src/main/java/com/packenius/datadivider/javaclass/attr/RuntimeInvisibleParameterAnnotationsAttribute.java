package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class RuntimeInvisibleParameterAnnotationsAttribute extends AttributeDescription {
  /**
   * Ein Annotation-Array je Parameter.
   */
  public final Annotation[][] annotations;

  /**
   * Konstruktor.
   */
  public RuntimeInvisibleParameterAnnotationsAttribute(int length, DumpReader reader) {
    super(reader, length);

    int parmCount = reader.readU1("Parameter count: ##DEC##").value;

    annotations = new Annotation[parmCount][];
    for (int k = 0; k < parmCount; k++) {
      int count = reader.readBigEndianU2("Annotations count: ##DEC##").value;

      annotations[k] = new Annotation[count];
      for (int i = 0; i < count; i++) {
        annotations[k][i] = new Annotation(reader);
      }
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "RuntimeInvisibleParameterAnnotations";
  }

  @Override
  public String toString() {
    return "Runtime Invisible Parameter Annotations Attribute";
  }

  @Override
  public String getDescription() {
    return "The RuntimeInvisibleParameterAnnotations attribute records run-time invisible "
        + "annotations on the declarations of formal parameters of the corresponding method. "
        + "There may be at most one RuntimeInvisibleParameterAnnotations attribute in "
        + "the attributes table of a method_info structure.";
  }
}
