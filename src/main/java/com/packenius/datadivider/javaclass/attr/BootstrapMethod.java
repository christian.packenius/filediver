package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class BootstrapMethod extends DumpBlock {
  /**
   * Eintrag im Konstantenpool.
   */
  public final int bootstrapMethodRef;

  /**
   * Indexes of bootstrap arguments.
   */
  public final int[] bootstrapArguments;

  /**
   * Konstruktor.
   */
  public BootstrapMethod(DumpReader reader, int i) {
    super(reader);

    BigEndianUnsigned2ByteInteger bmrDumpBlock = reader.readBigEndianU2("Bootstrap method ref index: ###DEC##");
    bmrDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(bmrDumpBlock.value));
    bootstrapMethodRef = bmrDumpBlock.value;

    int count = reader.readBigEndianU2("Bootstrap method arguments count: ##DEC##").value;

    bootstrapArguments = new int[count];
    for (int k = 0; k < count; k++) {
      bootstrapArguments[k] = reader.readBigEndianU2("Argument index: ###DEC##").value;
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Bootstrap Method";
  }
}
