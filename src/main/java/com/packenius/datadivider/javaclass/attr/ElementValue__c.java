package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class ElementValue__c extends ElementValue {
  /**
   * Zeiger auf den Konstantenpool mit einem UTF8-Eintrag, der einen
   * Return-Type-Desktiptor enthält.
   */
  public final int classInfoIndex;

  /**
   * Konstruktor.
   */
  public ElementValue__c(DumpReader reader, char tag) {
    super(reader, tag);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Class info index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    classInfoIndex = indexDumpBlock.value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Element Value Class Info";
  }
}
