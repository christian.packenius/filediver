package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Packenius, 2016.
 */
public class StackMapTableAttribute extends AttributeDescription {
  /**
   * Anzahl der Einträge.
   */
  public final int count;

  /**
   * Einträge in der Stack Map Table.
   */
  public final List<StackMapFrame> entries = new ArrayList<>();

  /**
   * Konstruktor.
   */
  public StackMapTableAttribute(int length, DumpReader reader) {
    super(reader, length);

    if (length > 0) {
      count = reader.readBigEndianU2("Entries count: ##DEC##").value;
      for (int i = 0; i < count; i++) {
        entries.add(StackMapFrame.getNextStackMapFrame(reader));
      }
    } else {
      count = 0;
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "StackMapTable";
  }

  @Override
  public String toString() {
    return "Stack Map Table Attribute";
  }

  @Override
  public String getDescription() {
    return "The StackMapTable attribute is a variable-length attribute in the attributes table "
        + "of a Code attribute. A StackMapTable attribute is used during the process "
        + "of verification by type checking.";
  }
}
