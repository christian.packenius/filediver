package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class TypeParameterTarget extends TargetInfo {
  /**
   * Index des Typparameters.
   */
  public final short typeParameterIndex;

  /**
   * Konstruktor.
   */
  public TypeParameterTarget(DumpReader reader, String kind) {
    super(reader, kind);

    typeParameterIndex = reader.readU1("Type parameter index: ###DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Type Parameter Target";
  }

  @Override
  public String getDescription() {
    return "The type_parameter_target item indicates that an annotation appears on the "
        + "declaration of the i'th type parameter of a generic class, generic interface, generic "
        + "method, or generic constructor.";
  }
}
