package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class LocalVar extends DumpBlock {
  public final int startPC;

  public final int length;

  public final int index;

  /**
   * Konstruktor.
   */
  public LocalVar(DumpReader reader) {
    super(reader);

    startPC = reader.readBigEndianU2("Start PC: ##DEC##").value;
    length = reader.readBigEndianU2("Length: ##DEC##").value;
    index = reader.readBigEndianU2("Local variable index: ###DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Local Var";
  }
}
