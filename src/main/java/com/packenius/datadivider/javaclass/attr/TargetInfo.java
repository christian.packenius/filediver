package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public abstract class TargetInfo extends DumpBlock {
  private final String kind;

  /**
   * Konstruktor.
   */
  public TargetInfo(DumpReader reader, String kind) {
    super(reader);
    this.kind = kind;
  }

  /**
   * Gibt den Klartext als Erläuterung dieses Targets zurück.
   *
   * @return Klartext als Erläuterung.
   */
  public final String getKind() {
    return kind;
  }
}
