package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public abstract class StackMapFrame extends DumpBlock {
  /**
   * Konstruktor.
   */
  public StackMapFrame(DumpReader reader) {
    super(reader);
  }

  /**
   * @return Einzelner StackMapFrame.
   */
  public static StackMapFrame getNextStackMapFrame(DumpReader reader) {
    int tag = reader.getCurrentByte() & 255;
    if (tag <= 63) {
      return new StackMap_SameFrame(reader);
    }
    if (tag <= 127) {
      return new StackMap_SameLocals1StackItemFrame(reader);
    }
    if (tag <= 246) {
      throw new RuntimeException("StackMapFrame " + tag + " reserved for future use!");
    }
    if (tag == 247) {
      return new StackMap_SameLocals1StackItemFrameExtended(reader);
    }
    if (tag <= 250) {
      return new StackMap_ChopFrame(reader);
    }
    if (tag == 251) {
      return new StackMap_SameFrameExtended(reader);
    }
    if (tag <= 254) {
      return new StackMap_AppendFrame(reader);
    }
    if (tag == 255) {
      return new StackMap_FullFrame(reader);
    }
    throw new RuntimeException("Unbekanntes tag-Byte: " + tag);
  }

  @Override
  public String toString() {
    return "Stack Map Frame";
  }
}
