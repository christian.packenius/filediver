package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class ElementValue__array extends ElementValue {
  /**
   * Für Array-Typ.
   */
  public final ElementValue[] arrayElementValues;

  /**
   * Konstruktor.
   */
  public ElementValue__array(DumpReader reader, char tag) {
    super(reader, tag);

    int count = reader.readBigEndianU2("Array length: ##DEC##").value;

    arrayElementValues = new ElementValue[count];
    for (int i = 0; i < count; i++) {
      arrayElementValues[i] = ElementValueFactory.getElementValue(reader);
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Element Value Array";
  }
}
