package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class SyntheticAttribute extends AttributeDescription {
  /**
   * Konstruktor.
   */
  public SyntheticAttribute(int length, DumpReader reader) {
    super(reader, length);

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "Synthetic";
  }

  @Override
  public String toString() {
    return "Synthetic Attribute";
  }

  @Override
  public String getDescription() {
    return "The Synthetic attribute is a fixed-length attribute in the attributes table of "
        + "a ClassFile, field_info, or method_info structure.";
  }
}
