package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class Annotation extends DumpBlock {
  /**
   * Index auf Konstantenpool mit UTF8-Eintrag mit Feld-Desktiptor.
   */
  public final int typeIndex;

  public final ElementValuePair[] elementValuePairs;

  /**
   * Konstruktor.
   *
   * @param reader
   */
  public Annotation(DumpReader reader) {
    super(reader);

    BigEndianUnsigned2ByteInteger typeIndexDumpBlock = reader.readBigEndianU2("Type index: ###DEC##");
    typeIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(typeIndexDumpBlock.value));
    typeIndex = typeIndexDumpBlock.value;

    int count = reader.readBigEndianU2("Element/value entries: ##DEC##").value;

    elementValuePairs = new ElementValuePair[count];
    for (int i = 0; i < count; i++) {
      elementValuePairs[i] = new ElementValuePair(reader);
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Annotation";
  }
}
