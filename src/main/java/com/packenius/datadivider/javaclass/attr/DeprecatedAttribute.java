package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class DeprecatedAttribute extends AttributeDescription {
  /**
   * Konstruktor.
   */
  public DeprecatedAttribute(int length, DumpReader reader) {
    super(reader, length);

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "Deprecated";
  }

  @Override
  public String toString() {
    return "Deprecated Attribute";
  }

  @Override
  public String getDescription() {
    return "The Deprecated attribute is an optional fixed-length attribute in the attributes "
        + "table of a ClassFile, field_info, or method_info structure. A "
        + "class, interface, method, or field may be marked using a Deprecated attribute to "
        + "indicate that the class, interface, method, or field has been superseded. "
        + "A run-time interpreter or tool that reads the class file format, such as a compiler, "
        + "can use this marking to advise the user that a superseded class, interface, method, "
        + "or field is being referred to. The presence of a Deprecated attribute does not alter "
        + "the semantics of a class or interface.";
  }
}
