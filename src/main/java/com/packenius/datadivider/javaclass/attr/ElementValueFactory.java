package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * Erzeugt je nach Bytes des Readers ein Objekt von ElementValue.
 *
 * @author Christian Packenius, 2016.
 */
public class ElementValueFactory {
  /**
   * Konstruktor.
   *
   * @param reader
   * @return ElementValue-Objekt.
   */
  public static ElementValue getElementValue(DumpReader reader) {
    char tag = (char) (255 & reader.getCurrentByte());

    switch (tag) {
      case 'B':
      case 'C':
      case 'D':
      case 'F':
      case 'I':
      case 'J':
      case 'S':
      case 'Z':
      case 's':
        return new ElementValue__BCDFIJSZs(reader, tag);

      case 'e':
        return new ElementValue__e(reader, tag);

      case 'c':
        return new ElementValue__c(reader, tag);

      case '@':
        return new ElementValue__at(reader, tag);

      case '[':
        return new ElementValue__array(reader, tag);

      default:
        throw new RuntimeException("Illegales Tag '" + tag + "' gefunden!");
    }
  }
}
