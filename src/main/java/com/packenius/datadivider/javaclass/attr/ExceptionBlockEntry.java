package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.CpClass;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Eintrag eines Ausnahmeblockes.
 *
 * @author Christian Packenius, 2016.
 */
public class ExceptionBlockEntry extends DumpBlock {
  /**
   * Start-Program-Counter des zu überwachenden Blockes.
   */
  public final int startPC;

  /**
   * End-Program-Counter des zu überwachenden Blockes.
   */
  public final int endPC;

  /**
   * Program-Counter, ab dem der Exception-Handler zu finden ist.
   */
  public final int handlerPC;

  /**
   * Zeiger in den Konstantenpool für den CatchType oder aber 0, falls es sich um
   * einen finally-Block handelt.
   */
  public final int catchTypeIndex;

  /**
   * Überwachte Ausnahme oder <i>null</i> im Falle eines finally-Blockes.
   */
  public final CpClass catchType;

  /**
   * Konstruktor.
   *
   * @param reader Klassen-Reader.
   */
  public ExceptionBlockEntry(DumpReader reader, ConstantPool cp) {
    super(reader);

    startPC = reader.readBigEndianU2("Start PC: ##DEC##").value;
    endPC = reader.readBigEndianU2("End PC: ##DEC##").value;
    handlerPC = reader.readBigEndianU2("Handler PC: ##DEC##").value;

    BigEndianUnsigned2ByteInteger catchTypeIndexDumpBlock = reader.readBigEndianU2("Catch type index: ###DEC##");
    catchTypeIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(catchTypeIndexDumpBlock.value));
    catchTypeIndex = catchTypeIndexDumpBlock.value;

    if (catchTypeIndex != 0) {
      catchType = cp.getClassEntry(catchTypeIndex);
    } else {
      catchType = null;
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Exception Block Entry";
  }
}
