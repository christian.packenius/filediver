package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class OffsetTarget extends TargetInfo {
  public final int offset;

  /**
   * Konstruktor.
   */
  public OffsetTarget(DumpReader reader, String kind) {
    super(reader, kind);

    offset = reader.readBigEndianU2("Offset: ##DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Offset Target";
  }
}
