package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class UserDefinedAttribute extends AttributeDescription {
  private final String name;

  /**
   * Konstruktor.
   */
  public UserDefinedAttribute(int length, DumpReader reader, String name) {
    super(reader, length);

    this.name = name;

    if (length > 0) {
      reader.readBytes(length, "User defined data");
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "#userdefined[" + name + "]";
  }

  @Override
  public String getDescription() {
    return "A non standard (user defined) attribute. A class compiler can "
        + "define any new attributes while not effekting the program flow.";
  }

  @Override
  public String toString() {
    return "User Defined Attribute";
  }
}
