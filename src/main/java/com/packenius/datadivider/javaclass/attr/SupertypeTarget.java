package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.Interfaces;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class SupertypeTarget extends TargetInfo {
  public final int supertypeIndex;

  /**
   * Supertype-Index als String.
   */
  public final String superType;

  /**
   * Konstruktor.
   */
  public SupertypeTarget(DumpReader reader, String kind) {
    super(reader, kind);

    Interfaces interfaces = reader.getUserObject(Interfaces.class);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Super type index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    supertypeIndex = indexDumpBlock.value;

    if (supertypeIndex == 65535) {
      superType = "[65535 -> extends]";
    } else {
      superType = "[interfaces[" + supertypeIndex + "]] " + interfaces.interfaces[supertypeIndex];
    }

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Supertype Target";
  }

  @Override
  public String getDescription() {
    return "The supertype_target item indicates that an annotation appears on a type in "
        + "the extends or implements clause of a class or interface declaration.";
  }
}
