package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class StackMap_SameLocals1StackItemFrame extends StackMapFrame {
  public final short tag;

  public final StackMap_VerificationTypeInfo stackItem;

  /**
   * Konstruktor.
   */
  public StackMap_SameLocals1StackItemFrame(DumpReader reader) {
    super(reader);

    tag = reader.readU1("Tag: ##DEC##").value;
    if (tag < 64 || tag > 127) {
      throw new RuntimeException("Tag != 64..127 (SAME_LOCALS_1_STACK_ITEM_FRAME)!");
    }
    stackItem = new StackMap_VerificationTypeInfo(reader);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Same Locals 1 Stack Item Frame";
  }
}
