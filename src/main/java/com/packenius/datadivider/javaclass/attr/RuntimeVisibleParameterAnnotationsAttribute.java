package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class RuntimeVisibleParameterAnnotationsAttribute extends AttributeDescription {
  /**
   * Ein Annotation-Array je Parameter.
   */
  public final Annotation[][] annotations;

  /**
   * Konstruktor.
   */
  public RuntimeVisibleParameterAnnotationsAttribute(int length, DumpReader reader) {
    super(reader, length);

    int parmCount = reader.readU1("Parm count: ##DEC##").value;
    annotations = new Annotation[parmCount][];

    for (int k = 0; k < parmCount; k++) {
      int count = reader.readBigEndianU2("Annotations count: ##DEC##").value;

      annotations[k] = new Annotation[count];
      for (int i = 0; i < count; i++) {
        annotations[k][i] = new Annotation(reader);
      }
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "RuntimeVisibleParameterAnnotations";
  }

  @Override
  public String toString() {
    return "Runtime Visible Parameter Annotations Attribute";
  }

  @Override
  public String getDescription() {
    return "The RuntimeVisibleParameterAnnotations attribute records run-time visible "
        + "annotations on the declarations of formal parameters of the corresponding method. "
        + "The Java Virtual Machine must make these annotations available so they can be "
        + "returned by the appropriate reflective APIs.";
  }
}
