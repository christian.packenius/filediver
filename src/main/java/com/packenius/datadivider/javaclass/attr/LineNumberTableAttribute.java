package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class LineNumberTableAttribute extends AttributeDescription {
  /**
   * Array mit Zeilennummereinträgen.
   */
  public final LineNumber[] lineNumbers;

  /**
   * Konstruktor.
   */
  public LineNumberTableAttribute(int length, DumpReader reader) {
    super(reader, length);

    int count = reader.readBigEndianU2("Line numbers count: ##DEC##").value;

    lineNumbers = new LineNumber[count];
    for (int i = 0; i < count; i++) {
      lineNumbers[i] = new LineNumber(reader, i);
    }

    setEndAddress(reader);
  }

  @Override
  public String getName() {
    return "LineNumberTable";
  }

  @Override
  public String toString() {
    return "Line Number Table Attribute";
  }

  @Override
  public String getDescription() {
    return "The LineNumberTable attribute is an optional variable-length attribute in the "
        + "attributes table of a Code attribute. It may be used by debuggers to "
        + "determine which part of the code array corresponds to a given line number in the "
        + "original source file. "
        + "If multiple LineNumberTable attributes are present in the attributes table of a "
        + "Code attribute, then they may appear in any order. "
        + "There may be more than one LineNumberTable attribute per line of a source file "
        + "in the attributes table of a Code attribute. That is, LineNumberTable attributes "
        + "may together represent a given line of a source file, and need not be one-to-one "
        + "with source lines.";
  }
}
