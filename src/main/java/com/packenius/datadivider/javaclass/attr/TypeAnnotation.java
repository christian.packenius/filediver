package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class TypeAnnotation extends DumpBlock {
  public final short targetType;

  public final TargetInfo targetInfo;

  public final TypePathElement[] typePath;

  public final int typeIndex;

  public final ElementValuePair[] elementValuePairs;

  /**
   * Konstruktor.
   */
  public TypeAnnotation(DumpReader reader) {
    super(reader);

    targetType = reader.readU1("Type type: 0x###HEX2##").value;

    targetInfo = readTargetInfo(reader);

    int typePathLength = reader.readU1("Type path length: ##DEC##").value;

    typePath = new TypePathElement[typePathLength];
    for (int i = 0; i < typePathLength; i++) {
      typePath[i] = new TypePathElement(reader);
    }

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Type index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    typeIndex = indexDumpBlock.value;

    int count = reader.readBigEndianU2("Element/value pairs count: ##DEC##").value;

    elementValuePairs = new ElementValuePair[count];
    for (int i = 0; i < count; i++) {
      elementValuePairs[i] = new ElementValuePair(reader);
    }

    setEndAddress(reader);
  }

  private TargetInfo readTargetInfo(DumpReader reader) {
    switch (targetType) {
      case 0x00:
        return new TypeParameterTarget(reader, "type parameter declaration of generic class or interface");

      case 0x01:
        return new TypeParameterTarget(reader, "type parameter declaration of generic method or constructor");

      case 0x10:
        return new SupertypeTarget(reader,
            "type in extends or implements clause of class declaration (including the direct superclass or direct superinterface of an anonymous class declaration), or in extends clause of interface declaration");

      case 0x11:
        return new TypeParameterBoundTarget(reader,
            "type in bound of type parameter declaration of generic class or interface");

      case 0x12:
        return new TypeParameterBoundTarget(reader,
            "type in bound of type parameter declaration of generic method or constructor");

      case 0x13:
        return new EmptyTarget(reader, "type in field declaration");

      case 0x14:
        return new EmptyTarget(reader, "return type of method, or type of newly constructed object");

      case 0x15:
        return new EmptyTarget(reader, "receiver type of method or constructor");

      case 0x16:
        return new FormalParameterTarget(reader,
            "type in formal parameter declaration of method, constructor, or lambda expression");

      case 0x17:
        return new ThrowsTarget(reader, "type in throws clause of method or constructor");

      case 0x40:
        return new LocalVarTarget(reader, "type in local variable declaration");

      case 0x41:
        return new LocalVarTarget(reader, "type in resource variable declaration");

      case 0x42:
        return new CatchTarget(reader, "type in exception parameter declaration");

      case 0x43:
        return new OffsetTarget(reader, "type in instanceof expression");

      case 0x44:
        return new OffsetTarget(reader, "type in new expression");

      case 0x45:
        return new OffsetTarget(reader, "type in method reference expression using ::new");

      case 0x46:
        return new OffsetTarget(reader, "type in method reference expression using ::Identifier");

      case 0x47:
        return new TypeArgumentTarget(reader, "type in cast expression");

      case 0x48:
        return new TypeArgumentTarget(reader,
            "type argument for generic constructor in new expression or explicit constructor invocationstatement");

      case 0x49:
        return new TypeArgumentTarget(reader, "type argument for generic method in method invocation expression");

      case 0x4a:
        return new TypeArgumentTarget(reader,
            "type argument for generic constructor in method reference expression using ::new");

      case 0x4b:
        return new TypeArgumentTarget(reader,
            "type argument for generic method in method reference expression using ::Identifier");

      default:
        throw new RuntimeException("Unknown targetType 0x" + Integer.toHexString(targetType) + " found!");
    }
  }

  @Override
  public String toString() {
    return "Type Annotation";
  }

  @Override
  public String getDescription() {
    return "Each entry in the annotations table represents a single run-time visible "
        + "annotation on a type used in a declaration or expression.";
  }
}
