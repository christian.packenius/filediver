package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class LocalVariableTypeTableAttribute extends AttributeDescription {
  public final LocalVariableType[] localVariableTypes;

  /**
   * Konstruktor.
   */
  public LocalVariableTypeTableAttribute(int length, DumpReader reader) {
    super(reader, length);

    int count = reader.readBigEndianU2("Local variable types count: ##DEC##").value;

    localVariableTypes = new LocalVariableType[count];
    for (int i = 0; i < count; i++) {
      localVariableTypes[i] = new LocalVariableType(reader, i);
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "LocalVariableTypeTable";
  }

  @Override
  public String toString() {
    return "Local Variable Type Table Attribute";
  }

  @Override
  public String getDescription() {
    return "The LocalVariableTypeTable attribute is an optional variable-length attribute in "
        + "the attributes table of a Code attribute. It may be used by debuggers to "
        + "determine the value of a given local variable during the execution of a method. "
        + "If multiple LocalVariableTypeTable attributes are present in the attributes "
        + "table of a given Code attribute, then they may appear in any order. "
        + "There may be no more than one LocalVariableTypeTable attribute per local "
        + "variable in the attributes table of a Code attribute.";
  }
}
