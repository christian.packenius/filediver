package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class ElementValue__BCDFIJSZs extends ElementValue {
  /**
   * Zeiger in den Konstantenpool mit dem entsprechenden Konstantenwert (atomarer
   * Datentyp oder String).
   */
  public final int constValueIndex;

  /**
   * Konstruktor.
   */
  public ElementValue__BCDFIJSZs(DumpReader reader, char tag) {
    super(reader, tag);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Const value index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    constValueIndex = indexDumpBlock.value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Element Value Constant";
  }
}
