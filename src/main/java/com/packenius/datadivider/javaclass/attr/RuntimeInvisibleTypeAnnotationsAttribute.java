package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class RuntimeInvisibleTypeAnnotationsAttribute extends AttributeDescription {
  public final TypeAnnotation[] annotations;

  /**
   * Konstruktor.
   */
  public RuntimeInvisibleTypeAnnotationsAttribute(int length, DumpReader reader) {
    super(reader, length);

    int count = reader.readBigEndianU2("Annotations count: ##DEC##").value;

    annotations = new TypeAnnotation[count];
    for (int i = 0; i < count; i++) {
      annotations[i] = new TypeAnnotation(reader);
    }

    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "RuntimeInvisibleTypeAnnotations";
  }

  @Override
  public String toString() {
    return "Runtime Invisible Type Annotations Attribute";
  }

  @Override
  public String getDescription() {
    return "The RuntimeInvisibleTypeAnnotations "
        + "attribute records run-time invisible annotations on types used in the corresponding "
        + "declaration of a class, field, or method, or in an expression in the corresponding "
        + "method body. The RuntimeInvisibleTypeAnnotations attribute also records "
        + "annotations on type parameter declarations of generic classes, interfaces, methods, "
        + "and constructors.";
  }
}
