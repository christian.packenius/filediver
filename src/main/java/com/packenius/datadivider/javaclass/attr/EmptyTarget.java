package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class EmptyTarget extends TargetInfo {
  /**
   * Konstruktor.
   */
  public EmptyTarget(DumpReader reader, String kind) {
    super(reader, kind);
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Empty Target";
  }
}
