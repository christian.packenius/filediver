package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class FormalParameterTarget extends TargetInfo {
  public final short formalParameterIndex;

  /**
   * Konstruktor.
   */
  public FormalParameterTarget(DumpReader reader, String kind) {
    super(reader, kind);

    formalParameterIndex = reader.readU1("Formal parameter index: ###DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Format Parameter Target";
  }
}
