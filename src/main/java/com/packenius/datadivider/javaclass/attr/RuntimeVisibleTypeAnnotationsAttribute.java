package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class RuntimeVisibleTypeAnnotationsAttribute extends AttributeDescription {
  private final int count;

  final TypeAnnotation[] annotations;

  /**
   * Konstruktor.
   */
  public RuntimeVisibleTypeAnnotationsAttribute(int length, DumpReader reader) {
    super(reader, length);

    count = reader.readBigEndianU2("Annotations count: ##DEC##").value;

    annotations = new TypeAnnotation[count];
    for (int i = 0; i < count; i++) {
      annotations[i] = new TypeAnnotation(reader);
    }
    setEndAddress(reader);
  }

  /**
   * @see AttributeDescription#getName()
   */
  @Override
  public String getName() {
    return "RuntimeVisibleTypeAnnotations";
  }

  @Override
  public String toString() {
    return "Runtime Visible Type Annotations Attribute";
  }

  @Override
  public String getDescription() {
    return "The RuntimeVisibleTypeAnnotations "
        + "attribute records run-time visible annotations on types used in the declaration of "
        + "the corresponding class, field, or method, or in an expression in the corresponding method body.";
  }
}
