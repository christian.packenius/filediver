package com.packenius.datadivider.javaclass.attr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class CatchTarget extends TargetInfo {
  public final int exceptionTableIndex;

  /**
   * Konstruktor.
   */
  public CatchTarget(DumpReader reader, String kind) {
    super(reader, kind);

    exceptionTableIndex = reader.readBigEndianU2("Exception table index: ###DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Catch Target";
  }
}
