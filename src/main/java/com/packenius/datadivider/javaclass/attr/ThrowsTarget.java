package com.packenius.datadivider.javaclass.attr;

import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class ThrowsTarget extends TargetInfo {
  public final int throwsTypeIndex;

  /**
   * Konstruktor.
   */
  public ThrowsTarget(DumpReader reader, String kind) {
    super(reader, kind);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Type index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    throwsTypeIndex = indexDumpBlock.value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Throws Target";
  }

  @Override
  public String getDescription() {
    return "The throws_target item indicates that an annotation appears on the i'th type in "
        + "the throws clause of a method or constructor declaration.";
  }
}
