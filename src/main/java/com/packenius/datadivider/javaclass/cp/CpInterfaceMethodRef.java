package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.dumpapi.DumpReader;

/**
 * Beschreibt einen Eintrag eines Konstanten-Pools einer class-Struktur, in dem
 * eine Interface-Methode beschrieben wird.
 *
 * @author Christian Packenius, 2016.
 */
public class CpInterfaceMethodRef extends ConstantPoolEntryWithMethodRef {
  /**
   * Konstruktor.
   */
  public CpInterfaceMethodRef(DumpReader reader, int id, ConstantPool constantPool) {
    super(reader, id, constantPool);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nInterface method reference constant, class #" + classIndex
        + ", name and type #" + nameAndTypeIndex;
  }

  @Override
  public String toString() {
    return "Interface method reference: " + super.toString() + " [#" + id + "]";
  }
}
