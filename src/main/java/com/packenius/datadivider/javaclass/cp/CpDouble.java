package com.packenius.datadivider.javaclass.cp;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class CpDouble extends ConstantPoolEntry implements ConstantPoolConst {
  /**
   * Eigentlicher double-Wert.
   */
  public final double doubleValue;

  /**
   * Constructor.
   */
  public CpDouble(DumpReader reader, int id) {
    super(reader, id);
    doubleValue = Double.longBitsToDouble(reader.readBigEndianU8("Const value: ##DOUBLE##"));
    setEndAddress(reader);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nDouble constant " + doubleValue;
  }

  @Override
  public String toString() {
    return "Double: " + Double.toString(doubleValue) + " [#" + id + "]";
  }
}
