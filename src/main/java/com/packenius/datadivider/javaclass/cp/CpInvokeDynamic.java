package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class CpInvokeDynamic extends ConstantPoolEntry {
  /**
   * Index auf die Tabelle der Bootstrap-Methoden.
   */
  public final int bootstrapMethodAttrIndex;

  /**
   * Name und Typ (Index).
   */
  public final int nameAndTypeIndex;

  /**
   * Konstruktor.
   */
  public CpInvokeDynamic(DumpReader reader, int id, ConstantPool constantpool) {
    super(reader, id);

    BigEndianUnsigned2ByteInteger bmrDumpBlock = reader
        .readBigEndianU2("Bootstrap method attribute index: ###DEC##");
    bmrDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(bmrDumpBlock.value));
    bootstrapMethodAttrIndex = bmrDumpBlock.value;

    BigEndianUnsigned2ByteInteger natDumpBlock = reader.readBigEndianU2("Name and type index: ###DEC##");
    natDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(natDumpBlock.value));
    nameAndTypeIndex = natDumpBlock.value;

    setEndAddress(reader);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nInvoke dynamic constant, bootstrap method attribute #"
        + bootstrapMethodAttrIndex + ", name and type #" + nameAndTypeIndex;
  }

  @Override
  public String toString() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    ConstantPoolEntry bootstrapMethodAttr = cp.getEntry(bootstrapMethodAttrIndex);
    if (bootstrapMethodAttr == null) {
      return "Invoke Dynamic: " + cp.getEntry(nameAndTypeIndex).toString() + " [#" + id + "]";
    }
    return "Invoke Dynamic: " + cp.getEntry(nameAndTypeIndex).toString() + ", with Bootstrap method: "
        + bootstrapMethodAttr.toString() + " [#" + id + "]";
  }
}
