package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class CpMethodType extends ConstantPoolEntry {
  /**
   * Index im Konstantenpool, UTF8-Eintrag.
   */
  public final int descriptorIndex;

  /**
   * Konstruktor.
   */
  public CpMethodType(DumpReader reader, int id, ConstantPool constantpool) {
    super(reader, id);
    BigEndianUnsigned2ByteInteger descriptorIndexDumpBlock = reader.readBigEndianU2("Descriptor index: ###DEC##");
    descriptorIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(descriptorIndexDumpBlock.value));
    descriptorIndex = descriptorIndexDumpBlock.value;
    setEndAddress(reader);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nMethod type constant, descriptor #" + descriptorIndex;
  }

  @Override
  public String toString() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    String type = cp.getEntry(descriptorIndex).asUtf8().string;
    String rc = singleTypeTranslation(type.substring(type.indexOf(')') + 1));
    String parms = parmsListTranslation(type);
    return "Method: " + parms + " : " + rc + " [#" + id + "]";
  }
}
