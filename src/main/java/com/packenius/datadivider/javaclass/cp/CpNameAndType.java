package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Name und Typ (eines Feldes oder einer Methode) im Konstantenpool einer
 * Klassenbeschreibung.
 *
 * @author Christian Packenius, 2016.
 */
public class CpNameAndType extends ConstantPoolEntry {
  /**
   * Index im Konstantenpool auf einen Namen.
   */
  public final int nameIndex;

  /**
   * Index im Konstantenpool auf einen Descriptor.
   */
  public final int descriptorIndex;

  /**
   * Constructor.
   */
  public CpNameAndType(DumpReader reader, int id) {
    super(reader, id);

    BigEndianUnsigned2ByteInteger nameIndexDumpBlock = reader.readBigEndianU2("Name index: ###DEC##");
    nameIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(nameIndexDumpBlock.value));
    nameIndex = nameIndexDumpBlock.value;

    BigEndianUnsigned2ByteInteger descriptorIndexDumpBlock = reader.readBigEndianU2("Descriptor index: ###DEC##");
    descriptorIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(descriptorIndexDumpBlock.value));
    descriptorIndex = descriptorIndexDumpBlock.value;

    setEndAddress(reader);
  }

  public String getFieldType() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    String type = cp.getEntry(descriptorIndex).asUtf8().string;
    return singleTypeTranslation(type);
  }

  public String getReturnType() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    String type = cp.getEntry(descriptorIndex).asUtf8().string;

    // Methode?
    if (type.charAt(0) == '(') {
      return singleTypeTranslation(type.substring(type.indexOf(')') + 1));
    }

    throw new RuntimeException("Missing return type!");
  }

  public String getParamTypes() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    String type = cp.getEntry(descriptorIndex).asUtf8().string;

    // Methode?
    if (type.charAt(0) == '(') {
      return parmsListTranslation(type);
    }

    throw new RuntimeException("Missing param list!");
  }

  public String getName() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    return cp.getEntry(nameIndex).asUtf8().string;
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nName and Type constant, name #" + nameIndex + ", descriptor #"
        + descriptorIndex;
  }

  @Override
  public String toString() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    String type = cp.getEntry(descriptorIndex).asUtf8().string;

    // Methode?
    if (type.charAt(0) == '(') {
      String returnType = getReturnType();
      String methodName = getName();
      String paramList = getParamTypes();
      return "Name and Type: " + returnType + " " + methodName + paramList;
    }

    // Feld!
    String fieldType = getFieldType();
    String fieldName = getName();
    return "Name and Type: " + fieldType + " " + fieldName + " [#" + id + "]";
  }
}
