package com.packenius.datadivider.javaclass.cp;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class CpLong extends ConstantPoolEntry implements ConstantPoolConst {
  /**
   * Eigentlicher long-Wert.
   */
  public final long longValue;

  /**
   * Constructor.
   */
  public CpLong(DumpReader reader, int id) {
    super(reader, id);
    longValue = reader.readBigEndianU8("Const value: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nLong constant " + longValue;
  }

  @Override
  public String toString() {
    return "Long: " + Long.toString(longValue) + "L" + " [#" + id + "]";
  }
}
