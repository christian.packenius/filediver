package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Beschreibt einen Eintrag eines Konstanten-Pools einer class-Struktur, in dem
 * ein String beschrieben wird.
 *
 * @author Christian Packenius, 2016.
 */
public class CpString extends ConstantPoolEntry implements ConstantPoolConst {
  /**
   * Index auf einen Eintrag im Konstantenpool.
   */
  public final int utf8Index;

  private String string;

  /**
   * Konstruktor.
   *
   * @param reader Klassen-Reader.
   */
  public CpString(DumpReader reader, int id, ConstantPool constantpool) {
    super(reader, id);

    BigEndianUnsigned2ByteInteger utf8IndexDumpBlock = reader.readBigEndianU2("UTF8 index: ###DEC##");
    utf8IndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(utf8IndexDumpBlock.value));
    utf8Index = utf8IndexDumpBlock.value;

    setEndAddress(reader);
  }

  /**
   * Gibt den Eintrag als String zurück.
   *
   * @return String, es wird immer der gleiche zurück gegeben.
   */
  public String getStringConst() {
    if (string == null) {
      string = reader.getUserObject(ConstantPool.class).getUtf8Entry(utf8Index).string;
    }
    return string;
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nString constant," + string.length() + " characters:\r\n\"" + string
        + "\"";
  }

  @Override
  public String toString() {
    getStringConst();
    return "String: \"" + string.replace("\\", "\\\\").replace("\"", "\\\"") + "\"" + " [#" + id + "]";
  }
}
