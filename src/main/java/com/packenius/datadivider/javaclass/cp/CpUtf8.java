package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.dumpapi.DumpReader;

import java.nio.charset.Charset;

/**
 * UTF8-Zeichenkette im Konstantenpool einer Klassenbeschreibung.
 *
 * @author Christian Packenius, 2016.
 */
public class CpUtf8 extends ConstantPoolEntry {
  /**
   * UTF8 charset.
   */
  public static final Charset UTF8 = Charset.forName("utf8");

  /**
   * String in Byte-Form.
   */
  public final byte[] bytes;

  /**
   * String als solcher.
   */
  public final String string;

  /**
   * Konstruktor.
   *
   * @param reader Klassen-Reader.
   */
  public CpUtf8(DumpReader reader, int id, ConstantPool constantpool) {
    super(reader, id);
    int length = reader.readBigEndianU2("Bytes count: ##DEC##").value;
    bytes = reader.readBytes(length, "UTF-8-String: ##UTF8##");
    string = new String(bytes, UTF8);
    setEndAddress(reader);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nUTF-8 constant," + bytes.length + " bytes, " + string.length()
        + " characters:\r\n" + string;
  }

  @Override
  public String toString() {
    return "UTF-8: " + string + " [#" + id + "]";
  }
}
