package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public abstract class ConstantPoolEntryWithMethodRef extends ConstantPoolEntry {
  /**
   * Index im Konstantenpool auf eine Klasse.
   */
  public final int classIndex;

  /**
   * Index im Konstantenpool auf einen Namen und einen Typ.
   */
  public final int nameAndTypeIndex;

  /**
   * Konstruktor.
   */
  public ConstantPoolEntryWithMethodRef(DumpReader reader, int id, ConstantPool constantpool) {
    super(reader, id);

    BigEndianUnsigned2ByteInteger classDumpBlock = reader.readBigEndianU2("Class index: ###DEC##");
    classDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(classDumpBlock.value));
    classIndex = classDumpBlock.value;

    BigEndianUnsigned2ByteInteger natDumpBlock = reader.readBigEndianU2("Name and type index: ###DEC##");
    natDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(natDumpBlock.value));
    nameAndTypeIndex = natDumpBlock.value;

    setEndAddress(reader);
  }

  /**
   * Ermittelt den Methodenkopf.
   */
  public String getMethodHead() {
    return methodHeadFromConstantPoolEntry(classIndex, nameAndTypeIndex);
  }

  @Override
  public String toString() {
    return getString();
  }

  public String getString() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    String className = cp.getEntry(classIndex).asClass().getClassName();
    CpNameAndType nat = (CpNameAndType) cp.getEntry(nameAndTypeIndex);
    String returnType = nat.getReturnType();
    String methodName = nat.getName();
    String paramList = nat.getParamTypes();
    return returnType + " " + className + "." + methodName + paramList;
  }
}
