package com.packenius.datadivider.javaclass.cp;

import com.packenius.dumpapi.DumpReader;

/**
 * Beschreibung eines Integer-Wertes als Eintrag im Konstantenpool einer
 * Klassenbeschreibung.
 *
 * @author Christian Packenius, 2016.
 */
public class CpInteger extends ConstantPoolEntry implements ConstantPoolConst {
  /**
   * Eigentlicher Integer-Wert.
   */
  public final int integerValue;

  /**
   * Constructor.
   */
  public CpInteger(DumpReader reader, int id) {
    super(reader, id);
    integerValue = (int) reader.readBigEndianU4("Constant: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nInteger  constant " + integerValue;
  }

  @Override
  public String toString() {
    return "Integer: " + Integer.toString(integerValue) + " [#" + id + "]";
  }
}
