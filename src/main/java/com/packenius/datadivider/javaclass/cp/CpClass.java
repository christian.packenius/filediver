package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Beschreibt einen Eintrag eines Konstanten-Pools einer class-Struktur, in dem
 * eine Klasse beschrieben wird.
 *
 * @author Christian Packenius, 2016.
 */
public class CpClass extends ConstantPoolEntry {
  /**
   * Index auf einen Eintrag im Konstantenpool.
   */
  public final int classIndex;

  /**
   * Konstruktor.
   */
  public CpClass(DumpReader reader, int id) {
    super(reader, id);

    BigEndianUnsigned2ByteInteger classDumpBlock = reader.readBigEndianU2("Class index: ###DEC##");
    classDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(classDumpBlock.value));
    classIndex = classDumpBlock.value;

    setEndAddress(reader);
  }

  public String getClassName() {
    CpUtf8 entry = (CpUtf8) reader.getUserObject(ConstantPool.class).getEntry(classIndex);
    return normalizeClassName(entry.string);
  }

  /**
   * Tauscht in einem Klassennamen sämtliche Slashes (wie die Klasse JVM-intern
   * verwendet wird) gegen Punkte aus.
   *
   * @param className Klassenname.
   * @return Normalisierter Klassenname.
   */
  public static String normalizeClassName(String className) {
    return className.replace('/', '.');
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nClass constant #" + classIndex;
  }

  @Override
  public String toString() {
    return "Class: " + getClassName() + " [#" + id + "]";
  }
}
