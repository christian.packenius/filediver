package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * @author Christian Packenius, 2016.
 */
public class CpMethodHandle extends ConstantPoolEntry {
  /**
   * Typ des Methoden-Handles, muss zwischen 1 und 9 liegen.
   */
  public final short referenceKind;

  private static final String[] refKinds = {null, "REF_getField :: getfield C.f:T",
      "REF_getStatic :: getstatic C.f:T", "REF_putField :: putfield C.f:T", "REF_putStatic :: putstatic C.f:T",
      "REF_invokeVirtual :: invokevirtual C.m:(A*)T", "REF_invokeStatic :: invokestatic C.m:(A*)T",
      "REF_invokeSpecial :: invokespecial C.m:(A*)T",
      "REF_newInvokeSpecial :: new C; dup; invokespecial C.<init>:(A*)V",
      "REF_invokeInterface :: invokeinterface C.m:(A*)T"};

  /**
   * Index im Konstantenpool, in Abhängigkeit vom obigen Typ.
   */
  public final int referenceIndex;

  /**
   * Konstruktor.
   */
  public CpMethodHandle(DumpReader reader, int id, ConstantPool constantpool) {
    super(reader, id);

    referenceKind = reader.readU1("Reference kind: ##DEC##").value;

    BigEndianUnsigned2ByteInteger referenceIndexDumpBlock = reader.readBigEndianU2("Reference index: ###DEC##");
    referenceIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(referenceIndexDumpBlock.value));
    referenceIndex = referenceIndexDumpBlock.value;

    setEndAddress(reader);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nMethod handle constant, reference #" + referenceIndex
        + "\r\nReference kind " + referenceKind + ": " + refKinds[referenceKind];
  }

  @Override
  public String toString() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    return "Method handle: " + refKinds[referenceKind] + " :: " + cp.getEntry(referenceIndex) + " [#" + id + "]";
  }
}
