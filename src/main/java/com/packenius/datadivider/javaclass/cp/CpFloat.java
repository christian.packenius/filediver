package com.packenius.datadivider.javaclass.cp;

import com.packenius.dumpapi.DumpReader;

/**
 * Beschreibung eines Float-Wertes als Eintrag im Konstantenpool einer
 * Klassenbeschreibung.
 *
 * @author Christian Packenius, 2016.
 */
public class CpFloat extends ConstantPoolEntry implements ConstantPoolConst {
  /**
   * Eigentlicher Float-Wert.
   */
  public final float floatValue;

  /**
   * Constructor.
   */
  public CpFloat(DumpReader reader, int id) {
    super(reader, id);
    floatValue = Float.intBitsToFloat((int) reader.readBigEndianU4("Constant: ##FLOAT##"));
    setEndAddress(reader);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nFloat constant " + floatValue;
  }

  @Override
  public String toString() {
    return "Float: " + Float.toString(floatValue) + "f" + " [#" + id + "]";
  }
}
