package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.dumpapi.DumpReader;

/**
 * Beschreibt einen Eintrag eines Konstanten-Pools einer class-Struktur, in dem
 * eine Methode beschrieben wird.
 *
 * @author Christian Packenius, 2016.
 */
public class CpMethodRef extends ConstantPoolEntryWithMethodRef {
  /**
   * Konstruktor.
   */
  public CpMethodRef(DumpReader reader, int id, ConstantPool constantPool) {
    super(reader, id, constantPool);
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nMethod reference constant, class #" + classIndex
        + ", name and type #" + nameAndTypeIndex;
  }

  @Override
  public String toString() {
    return "Method reference: " + super.toString() + " [#" + id + "]";
  }
}
