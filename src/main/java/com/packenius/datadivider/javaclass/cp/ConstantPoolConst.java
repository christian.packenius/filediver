package com.packenius.datadivider.javaclass.cp;

/**
 * Schnittstelle für Einträge des Konstantenpools, die Konstanten darstellen.
 *
 * @author Christian Packenius, 2016.
 */
public interface ConstantPoolConst {
}
