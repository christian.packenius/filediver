package com.packenius.datadivider.javaclass.cp;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Beschreibt einen Eintrag eines Konstanten-Pools einer class-Struktur, in dem
 * ein Feld beschrieben wird.
 *
 * @author Christian Packenius, 2016.
 */
public class CpFieldRef extends ConstantPoolEntry {
  /**
   * Index im Konstantenpool auf eine Klasse.
   */
  public final int classIndex;

  /**
   * Index im Konstantenpool auf einen Namen und einen Typ.
   */
  public final int nameAndTypeIndex;

  /**
   * Konstruktor.
   */
  public CpFieldRef(DumpReader reader, int id, ConstantPool constantpool) {
    super(reader, id);

    BigEndianUnsigned2ByteInteger classIndexDumpBlock = reader.readBigEndianU2("Class index: ###DEC##");
    classIndexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(classIndexDumpBlock.value));
    classIndex = classIndexDumpBlock.value;

    BigEndianUnsigned2ByteInteger natDumpBlock = reader.readBigEndianU2("Name and type index: ###DEC##");
    natDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(natDumpBlock.value));
    nameAndTypeIndex = natDumpBlock.value;

    setEndAddress(reader);
  }

  /**
   * Ermittelt den Namen des hier beschriebenen Feldes.
   */
  public String getFieldName() {
    ConstantPool constantpool = reader.getUserObject(ConstantPool.class);
    CpNameAndType nat = constantpool.getNameAndTypeEntry(nameAndTypeIndex);
    return constantpool.getEntry(nat.nameIndex).asUtf8().string;
  }

  /**
   * Ermittelt den Namen der Klasse des hier beschriebenen Feldes.
   */
  public String getClassName() {
    return reader.getUserObject(ConstantPool.class).getEntry(classIndex).asClass().getClassName();
  }

  @Override
  public String getDescription() {
    return "Constant pool entry #" + id + "\r\nField reference constant, class #" + classIndex + ", name and type #"
        + nameAndTypeIndex;
  }

  @Override
  public String toString() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    String className = getClassName();
    CpNameAndType nat = (CpNameAndType) cp.getEntry(nameAndTypeIndex);
    String fieldType = nat.getFieldType();
    String fieldName = nat.getName();
    return "Field reference: " + fieldType + " " + className + "." + fieldName + " [#" + id + "]";
  }
}
