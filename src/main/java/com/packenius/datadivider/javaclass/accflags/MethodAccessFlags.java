package com.packenius.datadivider.javaclass.accflags;

import com.packenius.dumpapi.DumpReader;

import java.util.TreeMap;

/**
 * Zugriffs-Flags einer Methode.
 *
 * @author Christian Packenius, 2016.
 */
public class MethodAccessFlags extends AccessFlags {
  /**
   * Konstruktor.
   *
   * @param accessFlags Zugriffs-Flags als Bitmuster.
   */
  public MethodAccessFlags(DumpReader reader) {
    super(reader);
  }

  /**
   * Returns a List of all currently set flags.
   *
   * @return List of set flags in string representation.
   */
  @Override
  public TreeMap<Integer, String> getFlagMap() {
    TreeMap<Integer, String> flags = new TreeMap<>();

    if (isPublic()) {
      flags.put(PUBLIC, "public");
    }
    if (isPrivate()) {
      flags.put(PRIVATE, "private");
    }
    if (isProtected()) {
      flags.put(PROTECTED, "protected");
    }
    if (isStatic()) {
      flags.put(STATIC, "static");
    }
    if (isFinal()) {
      flags.put(FINAL, "final");
    }
    if (isSynchronized()) {
      flags.put(SYNCHRONIZED, "synchronized");
    }
    if (isBridge()) {
      flags.put(BRIDGE, "bridge");
    }
    if (isVarargs()) {
      flags.put(VARARGS, "varargs");
    }
    if (isNative()) {
      flags.put(NATIVE, "native");
    }
    if (isAbstract()) {
      flags.put(ABSTRACT, "abstract");
    }
    if (isStrict()) {
      flags.put(STRICT, "strict");
    }
    if (isSynthetic()) {
      flags.put(SYNTHETIC, "synthetic");
    }

    return flags;
  }

  @Override
  public String toString() {
    return "Method access flags: " + getFlagsAsString();
  }
}
