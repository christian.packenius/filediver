package com.packenius.datadivider.javaclass.accflags;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.Statics;
import com.packenius.dumpapi.TechnicalDumpBlock;

import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Abstrakte Oberklasse aller Klassen, die sich mit AccessFlags beschäftigen.
 *
 * @author Christian Packenius, 2016.
 */
public abstract class AccessFlags extends DumpBlock implements TechnicalDumpBlock {
  /**
   * PUBLIC.
   */
  public static final int PUBLIC = 0x0001;

  /**
   * PRIVATE.
   */
  public static final int PRIVATE = 0x0002;

  /**
   * PROTECTED.
   */
  public static final int PROTECTED = 0x0004;

  /**
   * STATIC.
   */
  public static final int STATIC = 0x0008;

  /**
   * FINAL.
   */
  public static final int FINAL = 0x0010;

  /**
   * SUPER.
   */
  public static final int SUPER = 0x0020;

  /**
   * SYNCHRONIZED.
   */
  public static final int SYNCHRONIZED = 0x0020;

  /**
   * VOLATILE.
   */
  public static final int VOLATILE = 0x0040;

  /**
   * BRIDGE.
   */
  public static final int BRIDGE = 0x0040;

  /**
   * TRANSIENT.
   */
  public static final int TRANSIENT = 0x0080;

  /**
   * VARARGS.
   */
  public static final int VARARGS = 0x0080;

  /**
   * NATIVE.
   */
  public static final int NATIVE = 0x0100;

  /**
   * INTERFACE.
   */
  public static final int INTERFACE = 0x0200;

  /**
   * ABSTRACT.
   */
  public static final int ABSTRACT = 0x0400;

  /**
   * STRICT.
   */
  public static final int STRICT = 0x0800;

  /**
   * SYNTHETIC.
   */
  public static final int SYNTHETIC = 0x1000;

  /**
   * ANNOTATION.
   */
  public static final int ANNOTATION = 0x2000;

  /**
   * ENUM.
   */
  public static final int ENUM = 0x4000;

  /**
   * Zugriffsflags als Bitmuster.
   */
  protected int accessFlags;

  /**
   * Konstruktor.
   */
  public AccessFlags(DumpReader reader) {
    super(reader);
    this.accessFlags = reader.readBigEndianU2("Access Flags: ##BIN2##").value;
    setEndAddress(reader);
  }

  /**
   * Gibt die Zugriffsflags der Klasse als Bitmuster zurück.
   *
   * @return Zugriffsflags als Bitmuster.
   */
  public int getAccessFlags() {
    return accessFlags;
  }

  /**
   * Returns a List of all currently set flags.
   *
   * @return List of set flags in string representation.
   */
  public abstract TreeMap<Integer, String> getFlagMap();

  /**
   * Prüft, ob das Flag SUPER gesetzt ist.
   *
   * @return true/false
   */
  public boolean isSuper() {
    return (accessFlags & SUPER) != 0;
  }

  /**
   * Prüft, ob das Flag INTERFACE gesetzt ist.
   *
   * @return true/false
   */
  public boolean isInterface() {
    return (accessFlags & INTERFACE) != 0;
  }

  /**
   * Prüft, ob das Flag ANNOTATION gesetzt ist.
   *
   * @return true/false
   */
  public boolean isAnnotation() {
    return (accessFlags & ANNOTATION) != 0;
  }

  /**
   * Prüft, ob das Flag VOLATILE gesetzt ist.
   *
   * @return true/false
   */
  public boolean isVolatile() {
    return (accessFlags & VOLATILE) != 0;
  }

  /**
   * Prüft, ob das Flag TRANSIENT gesetzt ist.
   *
   * @return true/false
   */
  public boolean isTransient() {
    return (accessFlags & TRANSIENT) != 0;
  }

  /**
   * Prüft, ob das Flag ENUM gesetzt ist.
   *
   * @return true/false
   */
  public boolean isEnum() {
    return (accessFlags & ENUM) != 0;
  }

  /**
   * Prüft, ob das Flag PUBLIC gesetzt ist.
   *
   * @return true/false
   */
  public boolean isPublic() {
    return (accessFlags & PUBLIC) != 0;
  }

  /**
   * Prüft, ob das Flag PRIVATE gesetzt ist.
   *
   * @return true/false
   */
  public boolean isPrivate() {
    return (accessFlags & PRIVATE) != 0;
  }

  /**
   * Prüft, ob das Flag PROTECTED gesetzt ist.
   *
   * @return true/false
   */
  public boolean isProtected() {
    return (accessFlags & PROTECTED) != 0;
  }

  /**
   * Prüft, ob das Flag STATIC gesetzt ist.
   *
   * @return true/false
   */
  public boolean isStatic() {
    return (accessFlags & STATIC) != 0;
  }

  /**
   * Prüft, ob das Flag FINAL gesetzt ist.
   *
   * @return true/false
   */
  public boolean isFinal() {
    return (accessFlags & FINAL) != 0;
  }

  /**
   * Prüft, ob das Flag SYNCHRONIZED gesetzt ist.
   *
   * @return true/false
   */
  public boolean isSynchronized() {
    return (accessFlags & SYNCHRONIZED) != 0;
  }

  /**
   * Prüft, ob das Flag BRIDGE gesetzt ist.
   *
   * @return true/false
   */
  public boolean isBridge() {
    return (accessFlags & BRIDGE) != 0;
  }

  /**
   * Prüft, ob das Flag VARARGS gesetzt ist.
   *
   * @return true/false
   */
  public boolean isVarargs() {
    return (accessFlags & VARARGS) != 0;
  }

  /**
   * Prüft, ob das Flag NATIVE gesetzt ist.
   *
   * @return true/false
   */
  public boolean isNative() {
    return (accessFlags & NATIVE) != 0;
  }

  /**
   * Prüft, ob das Flag ABSTRACT gesetzt ist.
   *
   * @return true/false
   */
  public boolean isAbstract() {
    return (accessFlags & ABSTRACT) != 0;
  }

  /**
   * Prüft, ob das Flag STRICT gesetzt ist.
   *
   * @return true/false
   */
  public boolean isStrict() {
    return (accessFlags & STRICT) != 0;
  }

  /**
   * Prüft, ob das Flag SYNTHETIC gesetzt ist.
   *
   * @return true/false
   */
  public boolean isSynthetic() {
    return (accessFlags & SYNTHETIC) != 0;
  }

  @Override
  public String getDescription() {
    StringBuilder sb = new StringBuilder("Single flags:");
    for (Entry<Integer, String> entry : getFlagMap().entrySet()) {
      sb.append("\r\n" + Statics.toBinaryString(entry.getKey(), 16) + ": " + entry.getValue());
    }
    return sb.toString();
  }

  public String getFlagsAsString() {
    StringBuilder sb = new StringBuilder();
    for (Entry<Integer, String> entry : getFlagMap().entrySet()) {
      String value = entry.getValue();
      sb.append(" " + value);
    }
    return sb.toString().trim();
  }
}
