package com.packenius.datadivider.javaclass.accflags;

import com.packenius.dumpapi.DumpReader;

import java.util.TreeMap;

/**
 * Zugriffs-Flags einer Klasse.
 *
 * @author Christian Packenius, 2016.
 */
public class ClassAccessFlags extends AccessFlags {
  /**
   * Konstruktor.
   */
  public ClassAccessFlags(DumpReader reader) {
    super(reader);
  }

  /**
   * Returns a List of all currently set flags.
   *
   * @return List of set flags in string representation.
   */
  @Override
  public TreeMap<Integer, String> getFlagMap() {
    TreeMap<Integer, String> flags = new TreeMap<>();

    if (isPublic()) {
      flags.put(PUBLIC, "ACC_PUBLIC");
    }
    if (isFinal()) {
      flags.put(FINAL, "ACC_FINAL");
    }
    if (isSuper()) {
      flags.put(SUPER, "ACC_SUPER");
    }
    if (isInterface()) {
      flags.put(INTERFACE, "ACC_INTERFACE");
    }
    if (isAbstract()) {
      flags.put(ABSTRACT, "ACC_ABSTRACT");
    }
    if (isSynthetic()) {
      flags.put(SYNTHETIC, "ACC_SYNTHETIC");
    }
    if (isAnnotation()) {
      flags.put(ANNOTATION, "ACC_ANNOTATION");
    }
    if (isEnum()) {
      flags.put(ENUM, "ACC_ENUM");
    }

    return flags;
  }

  @Override
  public String toString() {
    return "Class Access Flags: " + getFlagsAsString();
  }
}
