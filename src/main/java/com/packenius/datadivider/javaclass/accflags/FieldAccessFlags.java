package com.packenius.datadivider.javaclass.accflags;

import com.packenius.dumpapi.DumpReader;

import java.util.TreeMap;

/**
 * Zugriffs-Flags eines Feldes.
 *
 * @author Christian Packenius, 2016.
 */
public class FieldAccessFlags extends AccessFlags {
  /**
   * Konstruktor.
   *
   * @param accessFlags Zugriffs-Flags als Bitmuster.
   */
  public FieldAccessFlags(DumpReader reader) {
    super(reader);
  }

  /**
   * Returns a List of all currently set flags.
   *
   * @return List of set flags in string representation.
   */
  @Override
  public TreeMap<Integer, String> getFlagMap() {
    TreeMap<Integer, String> flags = new TreeMap<>();

    if (isPublic()) {
      flags.put(PUBLIC, "public");
    }
    if (isPrivate()) {
      flags.put(PRIVATE, "private");
    }
    if (isProtected()) {
      flags.put(PROTECTED, "protected");
    }
    if (isStatic()) {
      flags.put(STATIC, "static");
    }
    if (isFinal()) {
      flags.put(FINAL, "final");
    }
    if (isVolatile()) {
      flags.put(VOLATILE, "volatile");
    }
    if (isTransient()) {
      flags.put(TRANSIENT, "transient");
    }
    if (isSynthetic()) {
      flags.put(SYNTHETIC, "synthetic");
    }
    if (isEnum()) {
      flags.put(ENUM, "enum");
    }

    return flags;
  }

  @Override
  public String toString() {
    return "Field access flags: " + getFlagsAsString();
  }
}
