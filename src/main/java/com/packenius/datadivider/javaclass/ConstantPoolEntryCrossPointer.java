package com.packenius.datadivider.javaclass;

import com.packenius.dumpapi.CrossPointer;

public class ConstantPoolEntryCrossPointer implements CrossPointer {
  public int constantPoolEntryIndex;

  public ConstantPoolEntryCrossPointer(int constantPoolEntryIndex) {
    this.constantPoolEntryIndex = constantPoolEntryIndex;
  }
}
