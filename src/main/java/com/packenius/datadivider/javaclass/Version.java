package com.packenius.datadivider.javaclass;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * Version einer class-Datei. Besteht aus Major- und Minor-Part.
 *
 * @author Christian Packenius, 2016.
 */
public class Version extends DumpBlock {
  /**
   * Haupt-Version.
   */
  public final int majorVersion;

  /**
   * Versionsteil hinter dem Dezimalpunkt.
   */
  public final int minorVersion;

  /**
   * Konstruktor.
   */
  public Version(DumpReader reader) {
    super(reader);
    minorVersion = reader.readBigEndianU2("Minor version: ##DEC##").value;
    majorVersion = reader.readBigEndianU2("Major version: ##DEC##").value;
    setEndAddress(reader);
  }

  /**
   * Ermittelt die Version als String.
   *
   * @return Versionsnummer in String-Form.
   */
  public String getVersion() {
    return majorVersion + "." + minorVersion;
  }

  /**
   * Gibt die Javaversion der class-Datei-Version zurück.
   *
   * @return Javaversion als String.
   */
  public String getJavaVersion() {
    switch (majorVersion) {
      case 45:
        if (0 <= minorVersion && minorVersion <= 3) {
          return "JDK 1.0.2";
        }
        return "JDK 1.1";
      case 46:
        return "JDK 1.2";
      case 47:
        return "JDK 1.3";
      case 48:
        return "JDK 1.4";
      case 49:
        return "Java SE 5.0";
      case 50:
        return "Java SE 6.0";
      case 51:
        return "Java SE 7";
      case 52:
        return "Java SE 8";
      case 53:
        return "Java SE 9";
    }

    if (majorVersion > 53) {
      return "Java, version > SE 9";
    }

    return "Java (unknown version)";
  }

  @Override
  public String toString() {
    return "Version: " + getJavaVersion();
  }

  @Override
  public String getDescription() {
    return majorVersion + "." + minorVersion + " -> " + getJavaVersion();
  }
}
