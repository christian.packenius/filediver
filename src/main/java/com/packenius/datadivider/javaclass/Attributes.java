package com.packenius.datadivider.javaclass;

import com.packenius.datadivider.javaclass.attr.AnnotationDefaultAttribute;
import com.packenius.datadivider.javaclass.attr.AttributeDescription;
import com.packenius.datadivider.javaclass.attr.BootstrapMethodsAttribute;
import com.packenius.datadivider.javaclass.attr.CodeAttribute;
import com.packenius.datadivider.javaclass.attr.ConstantValueAttribute;
import com.packenius.datadivider.javaclass.attr.DeprecatedAttribute;
import com.packenius.datadivider.javaclass.attr.EnclosingMethodAttribute;
import com.packenius.datadivider.javaclass.attr.ExceptionsAttribute;
import com.packenius.datadivider.javaclass.attr.InnerClassesAttribute;
import com.packenius.datadivider.javaclass.attr.LineNumberTableAttribute;
import com.packenius.datadivider.javaclass.attr.LocalVariableTableAttribute;
import com.packenius.datadivider.javaclass.attr.LocalVariableTypeTableAttribute;
import com.packenius.datadivider.javaclass.attr.RuntimeInvisibleAnnotationsAttribute;
import com.packenius.datadivider.javaclass.attr.RuntimeInvisibleParameterAnnotationsAttribute;
import com.packenius.datadivider.javaclass.attr.RuntimeInvisibleTypeAnnotationsAttribute;
import com.packenius.datadivider.javaclass.attr.RuntimeVisibleAnnotationsAttribute;
import com.packenius.datadivider.javaclass.attr.RuntimeVisibleParameterAnnotationsAttribute;
import com.packenius.datadivider.javaclass.attr.RuntimeVisibleTypeAnnotationsAttribute;
import com.packenius.datadivider.javaclass.attr.SignatureAttribute;
import com.packenius.datadivider.javaclass.attr.SourceDebugExtensionAttribute;
import com.packenius.datadivider.javaclass.attr.SourceFileAttribute;
import com.packenius.datadivider.javaclass.attr.StackMapTableAttribute;
import com.packenius.datadivider.javaclass.attr.SyntheticAttribute;
import com.packenius.datadivider.javaclass.attr.UserDefinedAttribute;
import com.packenius.datadivider.javaclass.cp.CpUtf8;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

import java.util.ArrayList;
import java.util.List;

/**
 * Menge von Attributen (z.B. einer Klasse).
 *
 * @author Christian Packenius, 2016.
 */
public class Attributes extends DumpBlock {
  /**
   * Alle verfügbaren Attribute.
   */
  public final List<AttributeDescription> attributes = new ArrayList<>();

  /**
   * Konstruktor.
   */
  public Attributes(DumpReader reader) {
    super(reader);
    int attributesCount = reader.readBigEndianU2("Attributes count: ##DEC##").value;
    for (int i = 0; i < attributesCount; i++) {
      attributes.add(readNextAttribute(reader));
    }
    setEndAddress(reader);
  }

  /**
   * Liest das nächste Attribut aus der Klasse.
   *
   * @return Attributobjekt.
   */
  private AttributeDescription readNextAttribute(DumpReader reader) {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);

    // Jedes Attribut enthält zuerst einen Namen und dann eine 4-Byte-Länge.
    int utf8Index = reader.readBigEndianU2("Attribute name index: ###DEC##").value;
    CpUtf8 utf8 = cp.getUtf8Entry(utf8Index);

    int length = (int) reader.readBigEndianU4("Attribute length: ##DEC##");

    reader.deltaIndex(-6);

    switch (utf8.string) {
      case "ConstantValue":
        return new ConstantValueAttribute(length, reader);
      case "Signature":
        return new SignatureAttribute(length, reader);
      case "SourceFile":
        return new SourceFileAttribute(length, reader);
      case "LineNumberTable":
        return new LineNumberTableAttribute(length, reader);
      case "LocalVariableTable":
        return new LocalVariableTableAttribute(length, reader);
      case "LocalVariableTypeTable":
        return new LocalVariableTypeTableAttribute(length, reader);
      case "StackMapTable":
        return new StackMapTableAttribute(length, reader);
      case "Code":
        return new CodeAttribute(length, reader);
      case "Exceptions":
        return new ExceptionsAttribute(length, reader);
      case "InnerClasses":
        return new InnerClassesAttribute(length, reader);
      case "EnclosingMethod":
        return new EnclosingMethodAttribute(length, reader);
      case "RuntimeVisibleAnnotations":
        return new RuntimeVisibleAnnotationsAttribute(length, reader);
      case "RuntimeInvisibleAnnotations":
        return new RuntimeInvisibleAnnotationsAttribute(length, reader);
      case "RuntimeVisibleParameterAnnotations":
        return new RuntimeVisibleParameterAnnotationsAttribute(length, reader);
      case "RuntimeInvisibleParameterAnnotations":
        return new RuntimeInvisibleParameterAnnotationsAttribute(length, reader);
      case "Deprecated":
        return new DeprecatedAttribute(length, reader);
      case "Synthetic":
        return new SyntheticAttribute(length, reader);
      case "AnnotationDefault":
        return new AnnotationDefaultAttribute(length, reader);
      case "BootstrapMethods":
        return new BootstrapMethodsAttribute(length, reader);
      case "SourceDebugExtension":
        return new SourceDebugExtensionAttribute(length, reader);
      case "RuntimeVisibleTypeAnnotations":
        return new RuntimeVisibleTypeAnnotationsAttribute(length, reader);
      case "RuntimeInvisibleTypeAnnotations":
        return new RuntimeInvisibleTypeAnnotationsAttribute(length, reader);
      case "MethodParameters":
        throw new RuntimeException("TODO!");
      default:
        return new UserDefinedAttribute(length, reader, utf8.string);
    }
  }

  /**
   * @param name Name des Attributes.
   * @return Attribute oder <i>null</i>, falls nicht gefunden.
   */
  public AttributeDescription getAttributeByName(String name) {
    for (AttributeDescription attr : attributes) {
      if (name.equals(attr.getName())) {
        return attr;
      }
    }
    return null;
  }

  @Override
  public String toString() {
    return "Attributes [" + attributes.size() + " entries]";
  }

  @Override
  public String getDescription() {
    return "All attributes of this class definition.";
  }
}
