package com.packenius.datadivider.javaclass;

import com.packenius.datadivider.javaclass.accflags.FieldAccessFlags;
import com.packenius.datadivider.javaclass.cp.ConstantPoolEntry;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Felddefinition innerhalb einer class-Datei.
 *
 * @author Christian Packenius, 2016.
 */
public class FieldDescription extends DumpBlock {
  /**
   * Zugriffsflags des Feldes.
   */
  public final FieldAccessFlags flags;

  /**
   * Index auf den Namen des Feldes im Konstantenpool.
   */
  public final int nameIndex;

  /**
   * Index auf die Beschreibung des Feldes im Konstantenpool.
   */
  public final int descriptorIndex;

  /**
   * Sämtliche Attribute des Feldes (falls vorhanden).
   */
  public final Attributes attributes;

  /**
   * Feldname.
   */
  public final String name;

  /**
   * Felddeskriptor (Typ).
   */
  public final String descriptor;

  /**
   * Constructor.
   */
  public FieldDescription(DumpReader reader) {
    super(reader);

    ConstantPool cp = reader.getUserObject(ConstantPool.class);

    flags = new FieldAccessFlags(reader);

    BigEndianUnsigned2ByteInteger nameIndexBlock = reader.readBigEndianU2("Name index: ###DEC##");
    nameIndexBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(nameIndexBlock.value));
    nameIndex = nameIndexBlock.value;
    name = cp.getUtf8Entry(nameIndex).string;

    BigEndianUnsigned2ByteInteger descriptorIndexBlock = reader.readBigEndianU2("Descriptor index: ###DEC##");
    descriptorIndexBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(descriptorIndexBlock.value));
    descriptorIndex = descriptorIndexBlock.value;
    descriptor = cp.getUtf8Entry(descriptorIndex).string;

    attributes = new Attributes(reader);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    String rt = ConstantPoolEntry.singleTypeTranslation(descriptor);

    // Klassen kürzen wir her ab.
    // int k = rt.lastIndexOf('.');
    // if (k > 0) {
    // rt = rt.substring(k + 1);
    // }

    return flags.getFlagsAsString() + " " + rt + " " + name;
  }

  @Override
  public String getDescription() {
    return "Full description of the field:\r\n" + toString();
  }
}
