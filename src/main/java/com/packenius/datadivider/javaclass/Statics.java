package com.packenius.datadivider.javaclass;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Statische Hilfsmethoden.
 *
 * @author Christian Packenius, 2016.
 */
public class Statics {
  /**
   * Gibt einen Wert rechtsbündig in gewünschter Länge als String zurück.
   *
   * @param value
   * @param size
   * @return Rechtsbündiger String.
   */
  public static String rightAlign(long value, int size) {
    String s = "                                  " + Long.toString(value);
    while (s.length() < size) {
      s = "                               " + s;
    }
    return s.substring(s.length() - size);
  }

  /**
   * Ersetzt alle "zu escapenden" Zeichen durch die lange Form.
   *
   * @param s
   * @return String mit "escapeten" Zeichen.
   */
  public static String escapeString(String s) {
    return s.replace("\\", "\\\\").replace("\"", "\\\"").replace("\'", "\\\'").replace("\f", "\\f")
        .replace("\r", "\\r").replace("\n", "\\n").replace("\b", "\\b").replace("\t", "\\t");
  }

  /**
   * Liest den gesamten InputStream ein und gibt das Ergebnis als byte[]-Objekt
   * zurück.
   *
   * @param in InputStream, der eingelesen werden soll.
   * @return byte[]-Array.
   * @throws IOException
   */
  public static byte[] getInputStreamBytes(InputStream in) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    int by;
    while ((by = in.read()) >= 0) {
      baos.write(by);
    }
    return baos.toByteArray();
  }

  /**
   * Gibt die Parameteranzahl der Methode zurück.
   *
   * @param descriptor Methoden-Descriptor.
   * @return Anzahl der Parameter dieser Methode.
   */
  public static int getParmCount(String descriptor) {
    if (descriptor.charAt(0) != '(') {
      throw new IllegalArgumentException("No method descriptor! " + descriptor);
    }
    int parmCount = 0;
    for (int i = 1; true; i++) {
      char ch = descriptor.charAt(i);

      // Atomarer Typ?
      if (ch == 'B' || ch == 'C' || ch == 'D' || ch == 'F' || ch == 'I' || ch == 'S' || ch == 'J' || ch == 'Z') {
        parmCount++;
      }

      // Klasse?
      else if (ch == 'L') {
        parmCount++;
        i = descriptor.indexOf(';', i);
      } else if (ch == ')') {
        break;
      }
    }
    return parmCount;
  }

  /**
   * @param descriptor
   * @return Array aller Parameter des Deskriptors.
   */
  public static String[] getParmDescriptors(String descriptor) {
    String[] pd = new String[getParmCount(descriptor)];
    if (descriptor.charAt(0) != '(') {
      throw new IllegalArgumentException("No method descriptor! " + descriptor);
    }
    for (int i = 1, k = 0; true; i++) {
      char ch = descriptor.charAt(i);
      String preArray = "";
      while (ch == '[') {
        preArray += "[";
        ch = descriptor.charAt(++i);
      }

      // Atomarer Typ?
      if (ch == 'B' || ch == 'C' || ch == 'D' || ch == 'F' || ch == 'I' || ch == 'S' || ch == 'J' || ch == 'Z') {
        pd[k++] = preArray + descriptor.substring(i, i + 1);
      }

      // Klasse?
      else if (ch == 'L') {
        int m = i;
        i = descriptor.indexOf(';', m);
        pd[k++] = preArray + descriptor.substring(m, i + 1);
      } else if (ch == ')') {
        break;
      } else {
        throw new RuntimeException("???");
      }
    }
    return pd;
  }

  /**
   * Bringt ein String-Array auf eine einheitliche Länge. Falls es diese Länge
   * schon besitzt, bleibt es einfach bestehen. Falls die gewünschte Länge kleiner
   * ist als die des Arrays, werden die hinteren Strings entfernt. Falls das neue
   * Array größer ist, werden die neuen Elemente mit <i>null</i> gefüllt.
   *
   * @param content String-Array.
   * @param newSize
   * @return (Neues oder gleiches) String-Array.
   */
  public static String[] resize(String[] content, int newSize) {
    if (content.length == newSize) {
      return content;
    }
    String[] c2 = new String[newSize];
    System.arraycopy(content, 0, c2, 0, Math.min(newSize, content.length));
    return c2;
  }

  /**
   * Ermittelt die Länge des längsten Strings. <i>null</i>-Strings werden nicht
   * beachtet.
   *
   * @param content String-Array.
   * @return Länge des längsten Strings oder 0, falls kein valider String
   * existiert.
   */
  public static int getMaxLength(String[] content) {
    int width = 0;
    for (String s : content) {
      if (s != null) {
        width = Math.max(s.length(), width);
      }
    }
    return width;
  }

  /**
   * Ermittelt die "raw bytes" einer Klasse. Dies ist quasi der Inhalt der
   * .class-Datei.
   *
   * @param clazz Zu ermittelnde Klasse.
   * @return byte[]-Array dieser Klasse.
   * @throws IOException
   */
  public static byte[] getClassBytes(Class<?> clazz) throws IOException {
    // Raw bytestream der Klasse ermitteln, um ihre Beschreibung erstellen zu
    // können.

    // Wir können hier nicht blind getSimpleName() verwenden, weil dort bei
    // inneren
    // Klassen nur der Name hinter "$" zurück gegeben wird. :-/
    String simpleName = clazz.getName();
    int k = simpleName.lastIndexOf('.');
    if (k > 0) {
      simpleName = simpleName.substring(k + 1);
    }
    return Statics.getInputStreamBytes(clazz.getResourceAsStream(simpleName + ".class"));
  }
}
