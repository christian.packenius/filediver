package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class ALOAD extends JvmInstruction implements UsingLocalVariable {
  /**
   * Index auf eine lokale Variable.
   */
  public final int index;

  /**
   * Konstruktor.
   */
  public ALOAD(int address, DumpReader reader) {
    super(address, reader);
    index = reader.readU1("Variable Index: ##DEC##").value;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "aload v" + index;
  }

  /**
   * @see UsingLocalVariable#getLocalVariableIndex()
   */
  @Override
  public int getLocalVariableIndex() {
    return index;
  }
}
