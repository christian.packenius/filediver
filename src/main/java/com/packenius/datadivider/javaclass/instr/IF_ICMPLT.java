package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IF_ICMPLT extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IF_ICMPLT(int address, DumpReader reader) {
    super("if_icmplt", address, reader);
    setEndAddress(reader);
  }
}
