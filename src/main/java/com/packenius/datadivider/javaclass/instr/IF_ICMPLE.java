package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IF_ICMPLE extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IF_ICMPLE(int address, DumpReader reader) {
    super("if_icmple", address, reader);
    setEndAddress(reader);
  }
}
