package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public abstract class IfInstruction extends JvmInstruction {
  /**
   * Name des IF-Befehls, z.B. 'ifnull'.
   */
  protected final String ifName;

  /**
   * Wird zum Program Counter hinzu gezählt (und damit verzweigt), wenn der IF mit
   * dem Wert auf dem Stack zieht.
   */
  public final short relativeAddress;

  /**
   * Konstruktor.
   */
  public IfInstruction(String ifName, int address, DumpReader reader) {
    super(address, reader);
    this.ifName = ifName;
    relativeAddress = reader.readBigEndianS2("Relative address: ##DEC##");
  }

  @Override
  public String toString() {
    int absoluteAddress = address + relativeAddress;
    if (relativeAddress < 0) {
      return ifName + " " + relativeAddress + "  // " + absoluteAddress;
    }
    return ifName + " +" + relativeAddress + "  // " + absoluteAddress;
  }
}
