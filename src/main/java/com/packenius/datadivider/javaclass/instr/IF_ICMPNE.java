package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IF_ICMPNE extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IF_ICMPNE(int address, DumpReader reader) {
    super("if_icmpne", address, reader);
    setEndAddress(reader);
  }
}
