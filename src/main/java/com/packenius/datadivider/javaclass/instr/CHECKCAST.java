package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.CpClass;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class CHECKCAST extends JvmInstruction {
  /**
   * Index in den Konstantenpool.
   */
  private final int index;

  /**
   * Klassen-, Interface- oder Array-Typ.
   */
  public final CpClass type;

  /**
   * Konstruktor.
   */
  public CHECKCAST(int address, DumpReader reader, ConstantPool constantpool) {
    super(address, reader);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Class index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    type = constantpool.getClassEntry(index);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "checkcast " + type.toString() + "  // #" + index;
  }
}
