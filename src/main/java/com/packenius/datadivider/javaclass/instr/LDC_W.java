package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.ConstantPoolEntry;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class LDC_W extends JvmInstruction {
  /**
   * Index in den Konstantenpool.
   */
  private final int index;

  /**
   * Double- oder Long-Konstante.
   */
  public final ConstantPoolEntry _const;

  /**
   * Konstruktor.
   */
  public LDC_W(int address, DumpReader reader, ConstantPool constantpool) {
    super(address, reader);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Const index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    _const = constantpool.getEntry(index);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "ldc_w " + reader.getUserObject(ConstantPool.class).toString() + " " + _const.toString() + "  // #"
        + index;
  }
}
