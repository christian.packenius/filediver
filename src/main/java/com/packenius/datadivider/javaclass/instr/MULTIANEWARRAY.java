package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.ConstantPoolEntry;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class MULTIANEWARRAY extends JvmInstruction {
  /**
   * Index in den Konstantenpool.
   */
  private final int index;

  /**
   * Methode oder Interface-Methode.
   */
  public final ConstantPoolEntry type;

  /**
   * Anzahl Dimensionen.
   */
  public final int dimensions;

  /**
   * Konstruktor.
   */
  public MULTIANEWARRAY(int address, DumpReader reader, ConstantPool constantpool) {
    super(address, reader);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Type index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    type = constantpool.getEntry(index);

    dimensions = reader.readU1("Dimensions: ##DEC##").value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "multianewarray #" + index + " " + dimensions + "  " + type;
  }
}
