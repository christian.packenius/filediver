package com.packenius.datadivider.javaclass.instr;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class RemoveGoMethods3 {
  public static void main(String[] args) throws Exception {
    for (File file : new File(
        "D:\\Chris\\Java_Eclipse_2019\\DataDivider\\src\\com\\packenius\\library\\datadivider\\javaclass\\instr")
        .listFiles()) {
      if (file.getName().endsWith(".java")) {
        workJavaFile(file);
      }
    }
  }

  private static void workJavaFile(File file) throws Exception {
    // String content = new
    // String(Files.readAllBytes(Paths.get(file.getCanonicalPath())),
    // Charset.forName("Cp1252"));
    List<String> lines = Files.readAllLines(Paths.get(file.getCanonicalPath()), Charset.forName("Cp1252"));
    for (int i = 0; i < lines.size(); i++) {
      String line = lines.get(i);
      if (line.contains("(int address, DumpReader reader) {")) {
        lines.set(i, line.replace("(int address, DumpReader reader)) {", "(int address, DumpReader reader) {"));
        Files.write(Paths.get(file.getCanonicalPath()), lines, Charset.forName("Cp1252"));
        break;
      }
//            if (line.endsWith("(int address) {") && lines.get(i + 1).contentEquals("        super(address);")) {
//                System.out.println("-> Found in " + file.getName());
//                lines.set(i, line.replace("(int address", "(int address, DumpReader reader)"));
//                lines.set(i + 1, "        super(address, reader);");
//
//                Files.write(Paths.get(file.getCanonicalPath()), lines, Charset.forName("Cp1252"));
//
//                break;
//            }
//            if (line.trim().equals("* @see com.packenius.datadivider.javaclass.instr.Instruction#toString()")
//                    && lines.get(i + 3).contentEquals("    public String toString() {")) {
//                System.out.println("-> Found in " + file.getName());
//                lines.set(i + 3, "    public String getTitle() {");
//                lines.remove(i - 1);
//                lines.remove(i - 1);
//                lines.remove(i - 1);
//
//                Files.write(Paths.get(file.getCanonicalPath()), lines, Charset.forName("Cp1252"));
//
//                break;
//            }
    }
  }
}
