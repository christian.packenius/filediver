package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class WIDE__ILOAD extends JvmInstruction implements UsingLocalVariable {
  /**
   * Index auf eine lokale Variable.
   */
  public final int index;

  /**
   * Konstruktor.
   */
  public WIDE__ILOAD(int address, DumpReader reader) {
    super(address, reader);
    index = reader.readBigEndianU2("Local var index: ###DEC##").value;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "wide iload v" + index;
  }

  /**
   * @see UsingLocalVariable#getLocalVariableIndex()
   */
  @Override
  public int getLocalVariableIndex() {
    return index;
  }
}
