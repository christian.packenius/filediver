package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IFNULL extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IFNULL(int address, DumpReader reader) {
    super("ifnull", address, reader);
    setEndAddress(reader);
  }
}
