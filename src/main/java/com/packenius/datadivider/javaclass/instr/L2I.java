package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class L2I extends JvmInstruction {
  /**
   * Konstruktor.
   */
  public L2I(int address, DumpReader reader) {
    super(address, reader);
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "l2i";
  }
}
