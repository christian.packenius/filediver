package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IFEQ extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IFEQ(int address, DumpReader reader) {
    super("ifeq", address, reader);
    setEndAddress(reader);
  }
}
