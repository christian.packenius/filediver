package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class ALOAD_3 extends JvmInstruction implements UsingLocalVariable {
  /**
   * Konstruktor.
   */
  public ALOAD_3(int address, DumpReader reader) {
    super(address, reader);
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "aload_3";
  }

  /**
   * @see UsingLocalVariable#getLocalVariableIndex()
   */
  @Override
  public int getLocalVariableIndex() {
    return 3;
  }
}
