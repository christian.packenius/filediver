package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class GOTO_W extends JvmInstruction {
  /**
   * Wird zum Program Counter hinzu gezählt (und damit verzweigt).
   */
  public final int branchDelta;

  /**
   * Konstruktor.
   */
  public GOTO_W(int address, DumpReader reader) {
    super(address, reader);
    branchDelta = reader.readBigEndianS4("Branch delta: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    int adr = address + branchDelta;
    return "goto_w " + adr + "  // " + branchDelta;
  }
}
