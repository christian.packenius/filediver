package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class INVOKEVIRTUAL extends InvokeInstruction {
  /**
   * Index in den Konstantenpool.
   */
  public final int index;

  /**
   * Konstruktor.
   */
  public INVOKEVIRTUAL(int address, DumpReader reader, ConstantPool cp) {
    super(address, reader);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Method index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "invokevirtual " + reader.getUserObject(ConstantPool.class).getMethodRefEntry(index).getString()
        + "  // #" + index;
  }
}
