package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class NEW extends JvmInstruction {
  /**
   * Index in den Konstantenpool.
   */
  public final int index;

  /**
   * Klasse oder Interface.
   */
  public final String className;

  /**
   * Konstruktor.
   */
  public NEW(int address, DumpReader reader, ConstantPool constantpool) {
    super(address, reader);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Class index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    ConstantPool cp = constantpool;
    className = cp.getUtf8Entry(cp.getClassEntry(index).classIndex).string.replace('/', '.');

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "new " + className + "  // #" + index;
  }
}
