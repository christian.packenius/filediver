package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.TechnicalDumpBlock;

/**
 * Instruktion der JVM, steckt im Code-Attribut einer Methode.
 *
 * @author Christian Packenius, 2016.
 */
public abstract class JvmInstruction extends DumpBlock implements TechnicalDumpBlock {
  /**
   * Startadresse der Instruktion, wird nach dem Aufruf des Konstruktors gesetzt.
   */
  public final int address;

  /**
   * Meldung, was dieser Befehl zuletzt getan hat.
   */
  protected String lastDoneThings = null;

  /**
   * Gibt die Anzahl an Bytes dieser Instruktion zurück.
   *
   * @return Anzahl Bytes, >=1.
   */
  public final int getByteCount() {
    return getEndAddress() - getStartAddress();
  }

  /**
   * @param address
   */
  public JvmInstruction(int address, DumpReader reader) {
    super(reader);
    reader.readU1("OP code: ##HEX1##");
    this.address = address;
  }
}
