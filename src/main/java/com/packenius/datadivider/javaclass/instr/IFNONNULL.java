package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IFNONNULL extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IFNONNULL(int address, DumpReader reader) {
    super("ifnonnull", address, reader);
    setEndAddress(reader);
  }
}
