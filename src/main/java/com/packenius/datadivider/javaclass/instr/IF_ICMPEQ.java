package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IF_ICMPEQ extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IF_ICMPEQ(int address, DumpReader reader) {
    super("if_icmpeq", address, reader);
    setEndAddress(reader);
  }
}
