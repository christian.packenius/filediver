package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class POP2 extends JvmInstruction {
  /**
   * Konstruktor.
   */
  public POP2(int address, DumpReader reader) {
    super(address, reader);
  }

  @Override
  public String toString() {
    return "pop2";
  }
}
