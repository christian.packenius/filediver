package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class DSTORE extends JvmInstruction implements UsingLocalVariable {
  /**
   * Index auf eine lokale Variable.
   */
  public final int index;

  /**
   * Konstruktor.
   */
  public DSTORE(int address, DumpReader reader) {
    super(address, reader);
    index = reader.readU1("Local variable index: ###DEC##").value;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "dstore v" + index;
  }

  /**
   * @see UsingLocalVariable#getLocalVariableIndex()
   */
  @Override
  public int getLocalVariableIndex() {
    return index;
  }
}
