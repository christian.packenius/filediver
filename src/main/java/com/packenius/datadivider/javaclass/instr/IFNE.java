package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IFNE extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IFNE(int address, DumpReader reader) {
    super("ifne", address, reader);
    setEndAddress(reader);
  }
}
