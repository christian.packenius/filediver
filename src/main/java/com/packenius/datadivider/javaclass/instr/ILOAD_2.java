package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class ILOAD_2 extends JvmInstruction implements UsingLocalVariable {
  /**
   * Konstruktor.
   */
  public ILOAD_2(int address, DumpReader reader) {
    super(address, reader);
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "iload_2";
  }

  /**
   * @see UsingLocalVariable#getLocalVariableIndex()
   */
  @Override
  public int getLocalVariableIndex() {
    return 2;
  }
}
