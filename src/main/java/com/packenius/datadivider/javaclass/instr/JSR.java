package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class JSR extends JvmInstruction {
  /**
   * Wird zum Program Counter hinzu gezählt (und damit verzweigt).
   */
  public final short branchDelta;

  /**
   * Konstruktor.
   */
  public JSR(int address, DumpReader reader) {
    super(address, reader);
    branchDelta = reader.readBigEndianS2("Branch delta: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    String delta = branchDelta < 0 ? "" + branchDelta : "+" + branchDelta;
    int adr = address + branchDelta;
    return "jsr " + adr + "  // " + delta;
  }
}
