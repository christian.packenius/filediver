package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.CpInterfaceMethodRef;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class INVOKEINTERFACE extends InvokeInstruction {
  /**
   * Index in den Konstantenpool.
   */
  public final int index;

  /**
   * Methode oder Interface-Methode.
   */
  public final CpInterfaceMethodRef interfaceMethod;

  /**
   * Anzahl Parameter.
   */
  public final int count;

  /**
   * Konstruktor.
   */
  public INVOKEINTERFACE(int address, DumpReader reader, ConstantPool constantpool) {
    super(address, reader);

    BigEndianUnsigned2ByteInteger iiDumpBlock = reader.readBigEndianU2("Interface method index: ###DEC##");
    iiDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(iiDumpBlock.value));
    index = iiDumpBlock.value;

    interfaceMethod = constantpool.getInterfaceMethodEntry(index);

    count = reader.readU1("Count: ##DEC##").value;

    // Dieser Wert hier muss 0 sein!
    if (reader.readU1("Zero value").value != 0) {
      throw new RuntimeException();
    }
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "invokeinterface " + interfaceMethod.getString() + "  // #" + index + ", " + count + " parms";
  }
}
