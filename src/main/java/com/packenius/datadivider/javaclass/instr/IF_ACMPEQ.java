package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IF_ACMPEQ extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IF_ACMPEQ(int address, DumpReader reader) {
    super("if_acmpeq", address, reader);
    setEndAddress(reader);
  }
}
