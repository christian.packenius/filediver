package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IFGE extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IFGE(int address, DumpReader reader) {
    super("ifge", address, reader);
    setEndAddress(reader);
  }
}
