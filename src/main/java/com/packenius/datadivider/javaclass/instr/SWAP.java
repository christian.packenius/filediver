package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class SWAP extends JvmInstruction {
  /**
   * Konstruktor.
   */
  public SWAP(int address, DumpReader reader) {
    super(address, reader);
  }

  @Override
  public String toString() {
    return "swap";
  }
}
