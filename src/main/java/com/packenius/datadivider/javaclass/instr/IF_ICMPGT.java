package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IF_ICMPGT extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IF_ICMPGT(int address, DumpReader reader) {
    super("if_icmpgt", address, reader);
    setEndAddress(reader);
  }
}
