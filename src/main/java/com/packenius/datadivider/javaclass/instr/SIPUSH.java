package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class SIPUSH extends JvmInstruction {
  /**
   * Short-Wert.
   */
  public final short _const;

  /**
   * Konstruktor.
   */
  public SIPUSH(int address, DumpReader reader) {
    super(address, reader);
    _const = reader.readBigEndianS2("Const value: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "sipush " + _const;
  }
}
