package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IFLE extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IFLE(int address, DumpReader reader) {
    super("ifle", address, reader);
    setEndAddress(reader);
  }
}
