package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.CpFieldRef;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class PUTSTATIC extends JvmInstruction {
  /**
   * Index in den Konstantenpool.
   */
  private final int index;

  /**
   * Feld.
   */
  public final CpFieldRef field;

  /**
   * Konstruktor.
   */
  public PUTSTATIC(int address, DumpReader reader, ConstantPool cp) {
    super(address, reader);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Field index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    field = cp.getFieldRefEntry(index);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "putstatic #" + index + "  " + field.toString();
  }
}
