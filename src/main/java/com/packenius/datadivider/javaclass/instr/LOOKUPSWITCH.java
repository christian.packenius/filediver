package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class LOOKUPSWITCH extends JvmInstruction {
  /**
   * 0..3 padding bytes.
   */
  public final int padding;

  public final int defaultByte;

  public final int npairs;

  public final int[] matches;

  public final int[] offsets;

  /**
   * Konstruktor.
   */
  public LOOKUPSWITCH(int address, DumpReader reader) {
    super(address, reader);
    address++;
    padding = 4 - (address & 3) & 3;
    reader.readBytes(padding, "Padding");
    defaultByte = (int) reader.readBigEndianU4("Default: ##DEC##");
    npairs = (int) reader.readBigEndianU4("Pairs count: ##DEC##");
    matches = new int[npairs];
    offsets = new int[npairs];
    for (int k = 0; k < npairs; k++) {
      matches[k] = (int) reader.readBigEndianU4("Value: ##DEC##");
      offsets[k] = (int) reader.readBigEndianU4("Offset: ##DEC##");
    }
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    String s = "lookupswitch default:" + defaultByte;
    for (int i = 0; i < npairs; i++) {
      s += " " + matches[i] + ">>" + offsets[i];
    }
    return s;
  }
}
