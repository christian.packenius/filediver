package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class LSHL extends JvmInstruction {
  /**
   * Konstruktor.
   */
  public LSHL(int address, DumpReader reader) {
    super(address, reader);
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "lshl";
  }
}
