package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IF_ICMPGE extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IF_ICMPGE(int address, DumpReader reader) {
    super("if_icmpge", address, reader);
    setEndAddress(reader);
  }
}
