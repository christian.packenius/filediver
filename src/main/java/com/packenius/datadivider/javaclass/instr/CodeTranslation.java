package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.dumpapi.DumpReader;

import java.util.ArrayList;
import java.util.List;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class CodeTranslation {
  /**
   * Übersetzt den gesamten Code-Block einer Methode.
   *
   * @param codeLength Känge des zu lesenden Codeblocks in Bytes.
   * @param code       Codeblock in byte[]-Form.
   * @param reader     Klassenleser, zur Übersetzung aus dem Konstantenpool
   *                   notwendig.
   * @return Liste aller Instruktionen der Methode.
   */
  public static List<JvmInstruction> translate(int codeLength, DumpReader reader, ConstantPool constantpool) {
    ArrayList<JvmInstruction> codes = new ArrayList<>();

    int startOfCodeAddress = reader.getCurrentIndex();
    int endOfCodeAddress = startOfCodeAddress + codeLength;

    while (true) {
      int address = reader.getCurrentIndex() - startOfCodeAddress;
      if (address >= codeLength) {
        break;
      }

      JvmInstruction instruction = getNextCode(address, reader, constantpool);
      // reader.setDumpText(Statics.rightAlign(address, 5) + " " +
      // instruction.toString());

      // System.out.println(" " + instruction);
      codes.add(instruction);
    }
    if (reader.getCurrentIndex() != endOfCodeAddress) {
      throw new RuntimeException("Fehler - letzte Instruktion zu lang?!");
    }
    return codes;
  }

  private static JvmInstruction getNextCode(int address, DumpReader reader, ConstantPool constantpool) {
    int opCode = reader.getCurrentByte() & 255;
    switch (opCode) {
      case 0x0:
        return new NOP(address, reader);
      case 0x1:
        return new ACONST_NULL(address, reader);
      case 0x2:
        return new ICONST_M1(address, reader);
      case 0x3:
        return new ICONST_0(address, reader);
      case 0x4:
        return new ICONST_1(address, reader);
      case 0x5:
        return new ICONST_2(address, reader);
      case 0x6:
        return new ICONST_3(address, reader);
      case 0x7:
        return new ICONST_4(address, reader);
      case 0x8:
        return new ICONST_5(address, reader);
      case 0x9:
        return new LCONST_0(address, reader);
      case 0xa:
        return new LCONST_1(address, reader);
      case 0xb:
        return new FCONST_0(address, reader);
      case 0xc:
        return new FCONST_1(address, reader);
      case 0xd:
        return new FCONST_2(address, reader);
      case 0xe:
        return new DCONST_0(address, reader);
      case 0xf:
        return new DCONST_1(address, reader);
      case 0x10:
        return new BIPUSH(address, reader);
      case 0x11:
        return new SIPUSH(address, reader);
      case 0x12:
        return new LDC(address, reader, constantpool);
      case 0x13:
        return new LDC_W(address, reader, constantpool);
      case 0x14:
        return new LDC2_W(address, reader, constantpool);
      case 0x15:
        return new ILOAD(address, reader);
      case 0x16:
        return new LLOAD(address, reader);
      case 0x17:
        return new FLOAD(address, reader);
      case 0x18:
        return new DLOAD(address, reader);
      case 0x19:
        return new ALOAD(address, reader);
      case 0x1a:
        return new ILOAD_0(address, reader);
      case 0x1b:
        return new ILOAD_1(address, reader);
      case 0x1c:
        return new ILOAD_2(address, reader);
      case 0x1d:
        return new ILOAD_3(address, reader);
      case 0x1e:
        return new LLOAD_0(address, reader);
      case 0x1f:
        return new LLOAD_1(address, reader);
      case 0x20:
        return new LLOAD_2(address, reader);
      case 0x21:
        return new LLOAD_3(address, reader);
      case 0x22:
        return new FLOAD_0(address, reader);
      case 0x23:
        return new FLOAD_1(address, reader);
      case 0x24:
        return new FLOAD_2(address, reader);
      case 0x25:
        return new FLOAD_3(address, reader);
      case 0x26:
        return new DLOAD_0(address, reader);
      case 0x27:
        return new DLOAD_1(address, reader);
      case 0x28:
        return new DLOAD_2(address, reader);
      case 0x29:
        return new DLOAD_3(address, reader);
      case 0x2a:
        return new ALOAD_0(address, reader);
      case 0x2b:
        return new ALOAD_1(address, reader);
      case 0x2c:
        return new ALOAD_2(address, reader);
      case 0x2d:
        return new ALOAD_3(address, reader);
      case 0x2e:
        return new IALOAD(address, reader);
      case 0x2f:
        return new LALOAD(address, reader);
      case 0x30:
        return new FALOAD(address, reader);
      case 0x31:
        return new DALOAD(address, reader);
      case 0x32:
        return new AALOAD(address, reader);
      case 0x33:
        return new BALOAD(address, reader);
      case 0x34:
        return new CALOAD(address, reader);
      case 0x35:
        return new SALOAD(address, reader);
      case 0x36:
        return new ISTORE(address, reader);
      case 0x37:
        return new LSTORE(address, reader);
      case 0x38:
        return new FSTORE(address, reader);
      case 0x39:
        return new DSTORE(address, reader);
      case 0x3a:
        return new ASTORE(address, reader);
      case 0x3b:
        return new ISTORE_0(address, reader);
      case 0x3c:
        return new ISTORE_1(address, reader);
      case 0x3d:
        return new ISTORE_2(address, reader);
      case 0x3e:
        return new ISTORE_3(address, reader);
      case 0x3f:
        return new LSTORE_0(address, reader);
      case 0x40:
        return new LSTORE_1(address, reader);
      case 0x41:
        return new LSTORE_2(address, reader);
      case 0x42:
        return new LSTORE_3(address, reader);
      case 0x43:
        return new FSTORE_0(address, reader);
      case 0x44:
        return new FSTORE_1(address, reader);
      case 0x45:
        return new FSTORE_2(address, reader);
      case 0x46:
        return new FSTORE_3(address, reader);
      case 0x47:
        return new DSTORE_0(address, reader);
      case 0x48:
        return new DSTORE_1(address, reader);
      case 0x49:
        return new DSTORE_2(address, reader);
      case 0x4a:
        return new DSTORE_3(address, reader);
      case 0x4b:
        return new ASTORE_0(address, reader);
      case 0x4c:
        return new ASTORE_1(address, reader);
      case 0x4d:
        return new ASTORE_2(address, reader);
      case 0x4e:
        return new ASTORE_3(address, reader);
      case 0x4f:
        return new IASTORE(address, reader);
      case 0x50:
        return new LASTORE(address, reader);
      case 0x51:
        return new FASTORE(address, reader);
      case 0x52:
        return new DASTORE(address, reader);
      case 0x53:
        return new AASTORE(address, reader);
      case 0x54:
        return new BASTORE(address, reader);
      case 0x55:
        return new CASTORE(address, reader);
      case 0x56:
        return new SASTORE(address, reader);
      case 0x57:
        return new POP(address, reader);
      case 0x58:
        return new POP2(address, reader);
      case 0x59:
        return new DUP(address, reader);
      case 0x5a:
        return new DUP_X1(address, reader);
      case 0x5b:
        return new DUP_X2(address, reader);
      case 0x5c:
        return new DUP2(address, reader);
      case 0x5d:
        return new DUP2_X1(address, reader);
      case 0x5e:
        return new DUP2_X2(address, reader);
      case 0x5f:
        return new SWAP(address, reader);
      case 0x60:
        return new IADD(address, reader);
      case 0x61:
        return new LADD(address, reader);
      case 0x62:
        return new FADD(address, reader);
      case 0x63:
        return new DADD(address, reader);
      case 0x64:
        return new ISUB(address, reader);
      case 0x65:
        return new LSUB(address, reader);
      case 0x66:
        return new FSUB(address, reader);
      case 0x67:
        return new DSUB(address, reader);
      case 0x68:
        return new IMUL(address, reader);
      case 0x69:
        return new LMUL(address, reader);
      case 0x6a:
        return new FMUL(address, reader);
      case 0x6b:
        return new DMUL(address, reader);
      case 0x6c:
        return new IDIV(address, reader);
      case 0x6d:
        return new LDIV(address, reader);
      case 0x6e:
        return new FDIV(address, reader);
      case 0x6f:
        return new DDIV(address, reader);
      case 0x70:
        return new IREM(address, reader);
      case 0x71:
        return new LREM(address, reader);
      case 0x72:
        return new FREM(address, reader);
      case 0x73:
        return new DREM(address, reader);
      case 0x74:
        return new INEG(address, reader);
      case 0x75:
        return new LNEG(address, reader);
      case 0x76:
        return new FNEG(address, reader);
      case 0x77:
        return new DNEG(address, reader);
      case 0x78:
        return new ISHL(address, reader);
      case 0x79:
        return new LSHL(address, reader);
      case 0x7a:
        return new ISHR(address, reader);
      case 0x7b:
        return new LSHR(address, reader);
      case 0x7c:
        return new IUSHR(address, reader);
      case 0x7d:
        return new LUSHR(address, reader);
      case 0x7e:
        return new IAND(address, reader);
      case 0x7f:
        return new LAND(address, reader);
      case 0x80:
        return new IOR(address, reader);
      case 0x81:
        return new LOR(address, reader);
      case 0x82:
        return new IXOR(address, reader);
      case 0x83:
        return new LXOR(address, reader);
      case 0x84:
        return new IINC(address, reader);
      case 0x85:
        return new I2L(address, reader);
      case 0x86:
        return new I2F(address, reader);
      case 0x87:
        return new I2D(address, reader);
      case 0x88:
        return new L2I(address, reader);
      case 0x89:
        return new L2F(address, reader);
      case 0x8a:
        return new L2D(address, reader);
      case 0x8b:
        return new F2I(address, reader);
      case 0x8c:
        return new F2L(address, reader);
      case 0x8d:
        return new F2D(address, reader);
      case 0x8e:
        return new D2I(address, reader);
      case 0x8f:
        return new D2L(address, reader);
      case 0x90:
        return new D2F(address, reader);
      case 0x91:
        return new I2B(address, reader);
      case 0x92:
        return new I2C(address, reader);
      case 0x93:
        return new I2S(address, reader);
      case 0x94:
        return new LCMP(address, reader);
      case 0x95:
        return new FCMPL(address, reader);
      case 0x96:
        return new FCMPG(address, reader);
      case 0x97:
        return new DCMPL(address, reader);
      case 0x98:
        return new DCMPG(address, reader);
      case 0x99:
        return new IFEQ(address, reader);
      case 0x9a:
        return new IFNE(address, reader);
      case 0x9b:
        return new IFLT(address, reader);
      case 0x9c:
        return new IFGE(address, reader);
      case 0x9d:
        return new IFGT(address, reader);
      case 0x9e:
        return new IFLE(address, reader);
      case 0x9f:
        return new IF_ICMPEQ(address, reader);
      case 0xa0:
        return new IF_ICMPNE(address, reader);
      case 0xa1:
        return new IF_ICMPLT(address, reader);
      case 0xa2:
        return new IF_ICMPGE(address, reader);
      case 0xa3:
        return new IF_ICMPGT(address, reader);
      case 0xa4:
        return new IF_ICMPLE(address, reader);
      case 0xa5:
        return new IF_ACMPEQ(address, reader);
      case 0xa6:
        return new IF_ACMPNE(address, reader);
      case 0xa7:
        return new GOTO(address, reader);
      case 0xa8:
        return new JSR(address, reader);
      case 0xa9:
        return new RET(address, reader);
      case 0xaa:
        return new TABLESWITCH(address, reader);
      case 0xab:
        return new LOOKUPSWITCH(address, reader);
      case 0xac:
        return new IRETURN(address, reader);
      case 0xad:
        return new LRETURN(address, reader);
      case 0xae:
        return new FRETURN(address, reader);
      case 0xaf:
        return new DRETURN(address, reader);
      case 0xb0:
        return new ARETURN(address, reader);
      case 0xb1:
        return new RETURN(address, reader);
      case 0xb2:
        return new GETSTATIC(address, reader, constantpool);
      case 0xb3:
        return new PUTSTATIC(address, reader, constantpool);
      case 0xb4:
        return new GETFIELD(address, reader, constantpool);
      case 0xb5:
        return new PUTFIELD(address, reader, constantpool);
      case 0xb6:
        return new INVOKEVIRTUAL(address, reader, constantpool);
      case 0xb7:
        return new INVOKESPECIAL(address, reader);
      case 0xb8:
        return new INVOKESTATIC(address, reader);
      case 0xb9:
        return new INVOKEINTERFACE(address, reader, constantpool);
      case 0xba:
        return new INVOKEDYNAMIC(address, reader, constantpool);
      case 0xbb:
        return new NEW(address, reader, constantpool);
      case 0xbc:
        return new NEWARRAY(address, reader);
      case 0xbd:
        return new ANEWARRAY(address, reader, constantpool);
      case 0xbe:
        return new ARRAYLENGTH(address, reader);
      case 0xbf:
        return new ATHROW(address, reader);
      case 0xc0:
        return new CHECKCAST(address, reader, constantpool);
      case 0xc1:
        return new INSTANCEOF(address, reader, constantpool);
      case 0xc2:
        return new MONITORENTER(address, reader);
      case 0xc3:
        return new MONITOREXIT(address, reader);
      case 0xc4:
        return createWideOpCode(address, reader);
      case 0xc5:
        return new MULTIANEWARRAY(address, reader, constantpool);
      case 0xc6:
        return new IFNULL(address, reader);
      case 0xc7:
        return new IFNONNULL(address, reader);
      case 0xc8:
        return new GOTO_W(address, reader);
      case 0xc9:
        return new JSR_W(address, reader);
      // Dürfen in .class-Dateien nicht vorkommen:
      // case 0xca:
      // return new BREAKPOINT(address, reader);
      // case 0xfe:
      // return new IMPDEP1(address, reader);
      // case 0xff:
      // return new IMPDEP2(address, reader);
      default:
        throw new RuntimeException("Unknown instruction code: 0x" + Integer.toHexString(opCode) + " @ " + address);
    }
  }

  private static JvmInstruction createWideOpCode(int address, DumpReader reader) {
    int subOpCode = reader.getCurrentByte() & 255;
    switch (subOpCode) {
      case 0x15:
        return new WIDE__ILOAD(address, reader);
      case 0x16:
        return new WIDE__LLOAD(address, reader);
      case 0x17:
        return new WIDE__FLOAD(address, reader);
      case 0x18:
        return new WIDE__DLOAD(address, reader);
      case 0x19:
        return new WIDE__ALOAD(address, reader);
      case 0x36:
        return new WIDE__ISTORE(address, reader);
      case 0x37:
        return new WIDE__LSTORE(address, reader);
      case 0x38:
        return new WIDE__FSTORE(address, reader);
      case 0x39:
        return new WIDE__DSTORE(address, reader);
      case 0x3a:
        return new WIDE__ASTORE(address, reader);
      case 0x84:
        return new WIDE__IINC(address, reader);
      case 0xa9:
        return new WIDE__RET(address, reader);
      default:
        throw new RuntimeException(
            "Unknown WIDE sub instruction code: 0x" + Integer.toHexString(subOpCode) + " @ " + (address + 1));
    }
  }
}
