package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public abstract class InvokeInstruction extends JvmInstruction {
  /**
   * Konstruktor.
   */
  public InvokeInstruction(int address, DumpReader reader) {
    super(address, reader);
  }
}
