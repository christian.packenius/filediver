package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class WIDE__IINC extends JvmInstruction implements UsingLocalVariable {
  /**
   * OP-Code, um den es hier geht.
   */
  public byte opcode;

  /**
   * Index auf eine lokale Variable.
   */
  public final int index;

  /**
   * Konstanter vorzeichenbehafteter 16Bit-Wert.
   */
  public final short _const;

  /**
   * Konstruktor.
   */
  public WIDE__IINC(int address, DumpReader reader) {
    super(address, reader);
    index = reader.readBigEndianU2("Local var index: ###DEC##").value;
    _const = reader.readBigEndianS2("Const value: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "wide iinc v" + index + "  " + _const;
  }

  /**
   * @see UsingLocalVariable#getLocalVariableIndex()
   */
  @Override
  public int getLocalVariableIndex() {
    return index;
  }
}
