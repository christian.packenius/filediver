package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IFGT extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IFGT(int address, DumpReader reader) {
    super("ifgt", address, reader);
    setEndAddress(reader);
  }
}
