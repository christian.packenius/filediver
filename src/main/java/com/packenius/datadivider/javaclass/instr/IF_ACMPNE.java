package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IF_ACMPNE extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IF_ACMPNE(int address, DumpReader reader) {
    super("if_acmpne", address, reader);
    setEndAddress(reader);
  }
}
