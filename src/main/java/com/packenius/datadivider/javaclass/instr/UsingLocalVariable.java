package com.packenius.datadivider.javaclass.instr;

/**
 * Verwendung einer lokalen Variable, deren Index hier abfragbar ist.
 *
 * @author Christian Packenius, 2016.
 */
public interface UsingLocalVariable {
  /**
   * Gibt den Index auf die lokal verwendete Variable zurück.
   *
   * @return Index der lokalen Variable.
   */
  public int getLocalVariableIndex();
}
