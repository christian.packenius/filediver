package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class BIPUSH extends JvmInstruction {
  /**
   * Byte-Wert.
   */
  public final byte _const;

  /**
   * Konstruktor.
   */
  public BIPUSH(int address, DumpReader reader) {
    super(address, reader);
    _const = reader.readS1("Const value: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "bipush " + _const;
  }
}
