package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IFLT extends IfInstruction {
  /**
   * Konstruktor.
   */
  public IFLT(int address, DumpReader reader) {
    super("iflt", address, reader);
    setEndAddress(reader);
  }
}
