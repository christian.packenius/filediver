package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.ConstantPoolEntry;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.Unsigned1ByteInteger;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class LDC extends JvmInstruction {
  /**
   * Index in den Konstantenpool.
   */
  private final short index;

  /**
   * Double- oder Long-Konstante.
   */
  public final ConstantPoolEntry _const;

  /**
   * Konstruktor.
   */
  public LDC(int address, DumpReader reader, ConstantPool cp) {
    super(address, reader);

    Unsigned1ByteInteger indexDumpBlock = reader.readU1("Variable index: ##DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    _const = cp.getEntry(index);

    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "ldc " + _const.toString();
  }
}
