package com.packenius.datadivider.javaclass.instr;

import com.packenius.datadivider.javaclass.ConstantPool;
import com.packenius.datadivider.javaclass.ConstantPoolEntryCrossPointer;
import com.packenius.datadivider.javaclass.cp.ConstantPoolEntry;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class INVOKEDYNAMIC extends InvokeInstruction {
  /**
   * Index in den Konstantenpool.
   */
  private final int index;

  /**
   * Methode oder Interface-Methode.
   */
  public final ConstantPoolEntry method;

  /**
   * Konstruktor.
   */
  public INVOKEDYNAMIC(int address, DumpReader reader, ConstantPool constantpool) {
    super(address, reader);

    BigEndianUnsigned2ByteInteger indexDumpBlock = reader.readBigEndianU2("Index: ###DEC##");
    indexDumpBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(indexDumpBlock.value));
    index = indexDumpBlock.value;

    method = constantpool.getEntry(index);

    // Nächsten zwei Bytes müssen 0 sein!
    if (reader.readBigEndianU2("Two zero bytes").value != 0) {
      throw new RuntimeException();
    }
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "invokedynamic " + method.toString() + "  // #" + index;
  }
}
