package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class LSTORE_2 extends JvmInstruction implements UsingLocalVariable {
  /**
   * Konstruktor.
   */
  public LSTORE_2(int address, DumpReader reader) {
    super(address, reader);
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "lstore_2";
  }

  /**
   * @see UsingLocalVariable#getLocalVariableIndex()
   */
  @Override
  public int getLocalVariableIndex() {
    return 2;
  }
}
