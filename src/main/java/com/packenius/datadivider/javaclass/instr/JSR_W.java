package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class JSR_W extends JvmInstruction {
  /**
   * Wird zum Program Counter hinzu gezählt (und damit verzweigt).
   */
  public final int branchDelta;

  /**
   * Konstruktor.
   */
  public JSR_W(int address, DumpReader reader) {
    super(address, reader);
    branchDelta = (int) reader.readBigEndianU4("Branch delta: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    int adr = address + branchDelta;
    String delta = branchDelta < 0 ? "" + branchDelta : "+" + branchDelta;
    return "jsr_w " + adr + "  // " + delta;
  }
}
