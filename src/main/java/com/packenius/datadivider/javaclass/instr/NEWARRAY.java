package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class NEWARRAY extends JvmInstruction {
  /**
   * Typ des zu erzeugenden Arrays - ein Wert zwischen 4 und 11.
   */
  public final int arrayType;

  /**
   * Konstruktor.
   */
  public NEWARRAY(int address, DumpReader reader) {
    super(address, reader);
    arrayType = reader.readU1("Array type: ##DEC##").value;
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "newarray " + arrayType;
  }
}
