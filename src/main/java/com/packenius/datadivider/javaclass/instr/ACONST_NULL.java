package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class ACONST_NULL extends JvmInstruction {
  /**
   * Konstruktor.
   */
  public ACONST_NULL(int address, DumpReader reader) {
    super(address, reader);
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "aconst_null";
  }
}
