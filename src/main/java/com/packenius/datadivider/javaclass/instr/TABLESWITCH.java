package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * @author Christian Packenius, 2016.
 */
public class TABLESWITCH extends JvmInstruction {
  /**
   * 0..3 padding bytes.
   */
  public final int padding;

  /**
   *
   */
  public final int defaultByte;

  /**
   * Niedrigster abgefragter Wert.
   */
  public final int lowByte;

  /**
   * Höchster abgefragter Wert.
   */
  public final int highByte;

  /**
   * Die diversen Offsets für die Werte von low bis high.
   */
  public final int[] jumpOffsets;

  /**
   * Konstruktor.
   */
  public TABLESWITCH(int address, DumpReader reader) {
    super(address, reader);
    address++;
    padding = 4 - (address & 3) & 3;
    reader.readBytes(padding, "Padding bytes");
    defaultByte = (int) reader.readBigEndianU4("Default: ##DEC##");
    lowByte = (int) reader.readBigEndianU4("Low: ##DEC##");
    highByte = (int) reader.readBigEndianU4("High: ##DEC##");
    int count = highByte - lowByte + 1;
    jumpOffsets = new int[count];
    for (int k = 0; k < count; k++) {
      jumpOffsets[k] = (int) reader.readBigEndianU4("Offset[" + (lowByte + k) + "]: ##DEC##");
    }
  }

  @Override
  public String toString() {
    String s = "tableswitch default:" + defaultByte;
    for (int i = lowByte; i <= highByte; i++) {
      s += " " + i + ">>" + jumpOffsets[i - lowByte];
    }
    return s;
  }
}
