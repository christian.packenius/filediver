package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class GOTO extends JvmInstruction {
  /**
   * Wird zum Program Counter hinzu gezählt (und damit verzweigt).
   */
  public final short branchDelta;

  /**
   * Konstruktor.
   */
  public GOTO(int address, DumpReader reader) {
    super(address, reader);
    branchDelta = reader.readBigEndianS2("Branch delta: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    int adr = address + branchDelta;
    if (branchDelta < 0) {
      return "goto " + adr + "  // " + branchDelta;
    }
    return "goto " + adr + "  // +" + branchDelta;
  }
}
