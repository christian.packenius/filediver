package com.packenius.datadivider.javaclass.instr;

import com.packenius.dumpapi.DumpReader;

/**
 * JVM-Instruktion.
 *
 * @author Christian Packenius, 2016
 */
public class IINC extends JvmInstruction implements UsingLocalVariable {
  /**
   * Variablenindex.
   */
  public final int index;

  /**
   * Konstanter Wert.
   */
  public final int _const;

  /**
   * Konstruktor.
   */
  public IINC(int address, DumpReader reader) {
    super(address, reader);
    index = reader.readU1("Variable index: ##DEC##").value;
    _const = reader.readS1("Const value: ##DEC##");
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "iinc v" + index + " " + _const;
  }

  /**
   * @see UsingLocalVariable#getLocalVariableIndex()
   */
  @Override
  public int getLocalVariableIndex() {
    return index;
  }
}
