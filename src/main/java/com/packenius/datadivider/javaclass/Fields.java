package com.packenius.datadivider.javaclass;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;

/**
 * Sämtliche Felder dieser Klasse.
 *
 * @author Christian Packenius, 2016.
 */
public class Fields extends DumpBlock {
  /**
   * Felder.
   */
  public FieldDescription[] fields;

  /**
   * Konstruktor.
   */
  public Fields(DumpReader reader) {
    super(reader);
    int fieldsCount = reader.readBigEndianU2("Fields count: ##DEC##").value;
    fields = new FieldDescription[fieldsCount];
    for (int i = 0; i < fieldsCount; i++) {
      fields[i] = new FieldDescription(reader);
    }
    setEndAddress(reader);
  }

  /**
   * @return Nummer des Feldes innerhalb der Klasse.
   */
  public long getFieldIndex(String fieldName) {
    for (int i = 0; i < fields.length; i++) {
      FieldDescription fd = fields[i];
      if (fd.name.equals(fieldName)) {
        return i;
      }
    }

    ClassHeader classHeader = reader.getUserObject(ClassHeader.class);

    throw new RuntimeException("Field <" + fieldName + "> of class <" + classHeader.thisClassName + "> not found!");
  }

  @Override
  public String toString() {
    return "Fields [" + fields.length + " entries]";
  }

  @Override
  public String getDescription() {
    String more = fields.length == 0 ? "\r\nHINT: This class does not contain any field." : "";
    return "The fields block contains both class attributes and object attributes." + more;
  }
}
