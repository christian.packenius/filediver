package com.packenius.datadivider.javaclass;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Liste der Interfaces dieser Klasse.
 *
 * @author Christian Packenius, 2016.
 */
public class Interfaces extends DumpBlock {
  /**
   * Array aller definierten Interfaces.
   */
  public final int[] interfaces;

  /**
   * Konstruktor.
   */
  public Interfaces(DumpReader reader) {
    super(reader);
    int interfacesCount = reader.readBigEndianU2("Interfaces count: ###DEC##").value;
    interfaces = new int[interfacesCount];
    for (int i = 0; i < interfacesCount; i++) {
      BigEndianUnsigned2ByteInteger interfaceIndexBlock = reader.readBigEndianU2("Interface index: ###DEC##");
      interfaceIndexBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(interfaceIndexBlock.value));
      interfaces[i] = interfaceIndexBlock.value;
    }
    setEndAddress(reader);
  }

  @Override
  public String toString() {
    return "Interfaces [" + interfaces.length + " entries]";
  }

  @Override
  public String getDescription() {
    return "List of all interfaces this class implements.";
  }
}
