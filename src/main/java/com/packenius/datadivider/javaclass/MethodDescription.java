package com.packenius.datadivider.javaclass;

import com.packenius.datadivider.javaclass.accflags.MethodAccessFlags;
import com.packenius.datadivider.javaclass.attr.CodeAttribute;
import com.packenius.datadivider.javaclass.cp.ConstantPoolEntry;
import com.packenius.datadivider.javaclass.cp.CpUtf8;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.DumpReader.BigEndianUnsigned2ByteInteger;

/**
 * Methodendefinition innerhalb einer class-Datei.
 *
 * @author Christian Packenius, 2016.
 */
public class MethodDescription extends DumpBlock {
  /**
   * Zugriffsflags der Methode.
   */
  public final MethodAccessFlags flags;

  /**
   * Index des Namens im Konstantenpool.
   */
  public final int nameIndex;

  /**
   * Name der Methode. Kann auch <init> oder <clinit> sein.
   */
  public final String methodName;

  /**
   * Index auf den Konstantenpool mit dem Typ (Descriptor) der Methode.
   */
  public final int descriptorIndex;

  /**
   * Der dazugehörige Descriptor.
   */
  public final String descriptor;

  /**
   * Attribute der Methode, falls vorhanden.
   */
  public final Attributes attributes;

  /**
   * Für toString().
   */
  private final String string;

  /**
   * Anzahl der Parameter dieser Methode, falls schon gezählt.
   */
  private int parmCount = -1;

  /**
   * Constructor.
   */
  public MethodDescription(DumpReader reader) {
    super(reader);

    ConstantPool cp = reader.getUserObject(ConstantPool.class);

    flags = new MethodAccessFlags(reader);

    BigEndianUnsigned2ByteInteger nameIndexBlock = reader.readBigEndianU2("Name index: ###DEC##");
    nameIndexBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(nameIndexBlock.value));
    nameIndex = nameIndexBlock.value;
    CpUtf8 utf8 = cp.getUtf8Entry(nameIndex);
    methodName = utf8.string;

    BigEndianUnsigned2ByteInteger descriptorIndexBlock = reader.readBigEndianU2("Descriptor index: ###DEC##");
    descriptorIndexBlock.setCrossPointer(new ConstantPoolEntryCrossPointer(descriptorIndexBlock.value));
    descriptorIndex = descriptorIndexBlock.value;
    descriptor = cp.getEntry(descriptorIndex).asUtf8().string;

    String rt = ConstantPoolEntry.singleTypeTranslation(descriptor.substring(descriptor.indexOf(')') + 1));
    String pl = ConstantPoolEntry.parmsListTranslation(descriptor);

    string = flags.getFlagsAsString() + " " + rt + " " + methodName + pl;

    attributes = new Attributes(reader);
    setEndAddress(reader);
  }

  /**
   * Ermittelt das Code-Attribut dieser Methode, falls vorhanden.
   *
   * @return Code-Attribut.
   */
  public CodeAttribute getCodeAttribute() {
    return (CodeAttribute) attributes.getAttributeByName("Code");
  }

  /**
   * Ermittelt den Namen dieser Methode.
   *
   * @return Name der Methode.
   */
  public String getName() {
    ConstantPool cp = reader.getUserObject(ConstantPool.class);
    return cp.getUtf8Entry(nameIndex).string;
  }

  /**
   * Gibt die Parameteranzahl der Methode zurück.
   *
   * @return Anzahl der Parameter dieser Methode.
   */
  public int getParmCount() {
    if (parmCount < 0) {
      parmCount = Statics.getParmCount(descriptor);
    }
    return parmCount;
  }

  /**
   * Prüft, ob diese Methode einen Rückgabewert zur Verfügung stellt.
   *
   * @return <i>true</i>, falls die Methode nicht <i>void</i> liefert.
   */
  public boolean hasReturnValue() {
    return !descriptor.endsWith(")V");
  }

  @Override
  public String toString() {
    return string.trim();
  }

  @Override
  public String getDescription() {
    return "Full method definition of\r\n" + string.trim();
  }
}
