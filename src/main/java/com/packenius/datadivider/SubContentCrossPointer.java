package com.packenius.datadivider;

import com.packenius.dumpapi.CrossPointer;
import com.packenius.dumpapi.MainDumpBlock;

/**
 * Special cross pointer that uses a sub byte array as a main byte array.
 *
 * @author Christian Packenius, 2019.
 */
public class SubContentCrossPointer implements CrossPointer {
  public SubContentCrossPointer(byte[] subContent) {
    DataDivider divider = new DataDivider();
    MainDumpBlock mdb = divider.divide(subContent);
  }
}
