package com.packenius.datadivider;

import com.packenius.datadivider.javaclass.JavaClassFileDescription;
import com.packenius.datadivider.jpeg.JpegDivider;
import com.packenius.datadivider.raw_content.RawContentDescription;
import com.packenius.dumpapi.DumpReader;
import com.packenius.dumpapi.MainDumpBlock;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Divides a given byte array in the best possible way.
 *
 * @author Christian Packenius, 2019.
 */
public class DataDivider {
  /**
   * All types of data descriptions that shall be tested.
   */
  private List<Class<? extends MainDumpBlock>> types = new ArrayList<>();

  /**
   * Standard constructor with default type(s).
   */
  public DataDivider() {
    types.add(JavaClassFileDescription.class);
    types.add(JpegDivider.class);
    types.add(RawContentDescription.class);
  }

  /**
   * Creates a data divider with a single type to check.
   */
  public DataDivider(Class<? extends MainDumpBlock> mdb) {
    types.add(mdb);
  }

  /**
   * Creates a data divider with the given array of types to check.
   */
  public DataDivider(Class<? extends MainDumpBlock>[] mdbs) {
    types.addAll(Arrays.asList(mdbs));
  }

  /**
   * Creates a data divider with the given list of types to check.
   */
  public DataDivider(List<Class<? extends MainDumpBlock>> mdbs) {
    types.addAll(mdbs);
  }

  /**
   * Adds another type to check.
   */
  public void addType(Class<? extends MainDumpBlock> mdb) {
    if (!types.contains(mdb)) {
      types.add(mdb);
    }
  }

  /**
   * Returns first type that matches the content.
   */
  public MainDumpBlock divide(byte[] content) {
    for (Class<? extends MainDumpBlock> clazz : types) {
      try {
        DumpReader dumpReader = new DumpReader(content);
        Constructor<? extends MainDumpBlock> constructor = clazz.getConstructor(DumpReader.class);
        constructor.newInstance(dumpReader);
        return dumpReader.getMainBlock();
      } catch (Exception e) {
        // Okay, not matching. Check next type.
        System.err.println("Not: " + clazz.getSimpleName() + ", reason: " + e.getMessage());
        // e.printStackTrace();
      }
    }

    // No matching type found.
    return null;
  }
}
