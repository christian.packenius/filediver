package com.packenius.fid.gui;

public class Arrow {
  public int x1;
  public int x2;
  public int x3;
  public int y1;
  public int y2;
  public boolean dottedArrow;

  public Arrow(int x1, int x2, int x3, int y1, int y2, boolean dottedArrow) {
    this.x1 = x1;
    this.x2 = x2;
    this.x3 = x3;
    this.y1 = y1;
    this.y2 = y2;
    this.dottedArrow = dottedArrow;
  }

  /**
   * Move every coordinate by this delta.
   */
  public void move(int dx, int dy) {
    x1 += dx;
    x2 += dx;
    x3 += dx;
    y1 += dy;
    y2 += dy;
  }
}
