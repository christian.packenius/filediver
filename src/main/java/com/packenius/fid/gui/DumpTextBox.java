package com.packenius.fid.gui;

import com.packenius.dumpapi.DumpBlock;
import com.packenius.fid.gui.label.AddressedLabel;
import com.packenius.fid.gui.label.DumpLabel;
import com.packenius.fid.gui.label.HeadlineLabel;
import com.packenius.fid.gui.label.LineLabel;
import com.packenius.fid.gui.label.PureTextLabel;
import com.packenius.hexr.HexR;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.util.List;

/**
 * A Panel that represents a full dump block containing only JLabel objects.
 *
 * @author Christan Packenius, 2019.
 */
public class DumpTextBox extends JPanelWithDesiredFont {
  private static final long serialVersionUID = 748329311429299832L;

  private static final Color BG_COLOR = new Color(0xfff0f0f0);

  private static final Color SPECIAL_BG_COLOR = new Color(0xff90a0a0);

  /**
   * Shown dump block.
   */
  public final DumpBlock dumpBlock;

  /**
   * Special boxes have a special background and a special arrow that points to
   * it. They are no real children but cross pointed from another box anywhere
   * within the file.
   */
  public final boolean specialBox;

  /**
   * The graphical addressed label that points with an arrow to this box.
   */
  public final AddressedLabel pointingAddressedLabel;

  /**
   * This was the lastly choosen AddressedLabel in this DumpTextBox.
   */
  private AddressedLabel lastChoosenAddressedLabel;

  /**
   * Use one of this fonts if possible. The last one is the best one.
   */
  private final static String[] desiredFonts = {"Consolas", "Lucida Console", "Noto Mono"};

  public DumpTextBox(DumpBlock dumpBlock) {
    this(dumpBlock, null);
  }

  public DumpTextBox(DumpBlock dumpBlock, AddressedLabel addressedLabel) {
    this(dumpBlock, addressedLabel, false);
  }

  public DumpTextBox(DumpBlock dumpBlock, AddressedLabel addressedLabel, boolean specialBox) {
    if (dumpBlock == null) {
      throw new RuntimeException("The " + DumpBlock.class.getSimpleName() + " object must not be null!");
    }

    // Store dump block to view here.
    this.dumpBlock = dumpBlock;

    // The graphical addressed label that points with an arrow to this box.
    this.pointingAddressedLabel = addressedLabel;

    // Is this a special box?
    this.specialBox = specialBox;

    // Background color.
    Color backgroundColor = specialBox ? SPECIAL_BG_COLOR : BG_COLOR;

    // Layout: Font, Border, ...
    createLayoutElements(backgroundColor);

    // Show every line of a box as a single JLabel object.
    createTextLabels(dumpBlock, backgroundColor);

    // We only need minimal size.
    setSize(getMinimumSize());
  }

  private void createLayoutElements(Color backgroundColor) {
    setBackground(backgroundColor);
    setLayout(new GridLayout(0, 1));
    String font = getDesiredFont(desiredFonts);
    if (font != null) {
      setFont(new Font(font, Font.PLAIN, 16));
    }
    Border outsideBorder = BorderFactory.createLineBorder(FidPanel.arrowColor, 2, true);
    Border insideBorder = BorderFactory.createEmptyBorder(3, 3, 3, 3);
    setBorder(BorderFactory.createCompoundBorder(outsideBorder, insideBorder));
  }

  private void createTextLabels(DumpBlock block, Color backgroundColor) {
    // Two lines for header.
    new HeadlineLabel(block.toString(), adressString(block.getStartAddress()), this, backgroundColor);
    int byteSize = block.getByteSize();
    new HeadlineLabel("[" + byteSize + " bytes]", ".." + adressString(block.getStartAddress() + byteSize - 1), this,
        backgroundColor);
    new LineLabel("-", this);

    // Are there sub blocks or shall we show hex bytes?
    List<DumpBlock> subBlocks = block.reader.getSubBlocks(block);

    // Create addresses JLabel objects.
    if (subBlocks != null) {
      for (DumpBlock sub : subBlocks) {
        new AddressedLabel(sub, this);
      }
    }

    // No sub blocks - show hex codes.
    else {
      int start = block.getStartAddress();
      int end = block.getEndAddress();
      int bytesPerLine = 8;
      HexR hexR = new HexR();
      hexR.setFirstMemoryAddress(start);
      hexR.setHexCodesPerLine(bytesPerLine);
      hexR.setResultType(String[].class);
      hexR.setPrintMemoryAddress(false);
      for (String hexLine : (String[]) hexR.dump(block.reader.getContent(), start, end)) {
        new AddressedLabel(start, hexLine, this);
        start += bytesPerLine;
      }
    }

    // Show description line(s) if it exists.
    // Falls es noch eine Beschreibung gibt, wird auch diese angehangen.
    String description = block.getDescription();
    if (description != null) {
      new LineLabel("-", this);
      String[] descLines = description.replace("\r\n", "\n").replace("\r", "\n").split("\n");
      for (String descLine : descLines) {
        while (descLine.length() > 50) {
          int sp = descLine.lastIndexOf(' ', 50);
          if (sp < 0) {
            new PureTextLabel(descLine.substring(0, 50), this);
            descLine = descLine.substring(50);
          } else {
            new PureTextLabel(descLine.substring(0, sp), this);
            descLine = descLine.substring(sp).trim();
          }
        }
        new PureTextLabel(descLine, this);
      }
    }

    // Sizes calculations...

    // Set max line count in every child component.
    int maxChars = 0;
    for (Component child : getComponents()) {
      maxChars = Math.max(maxChars, ((DumpLabel) child).getTextLength());
    }
    for (Component child : getComponents()) {
      ((DumpLabel) child).setTextCharMax(maxChars);
    }
  }

  public static String adressString(long adr) {
    String sAdr = Long.toString(adr, 16);
    return "00000000".substring(sAdr.length()) + sAdr;
  }

  /**
   * Remove all child boxes resursive, then remove itself.
   */
  public void destroy() {
    for (Component comp : getComponents()) {
      if (comp instanceof AddressedLabel) {
        List<DumpTextBox> children = ((AddressedLabel) comp).getChildren();
        if (children != null) {
          for (DumpTextBox child : children) {
            child.destroy();
          }
        }
      }
    }
    getParent().remove(this);
  }

  /**
   * Reposition all dump text boxes but the root dump box.
   */
  public void rePositionRekursive() {
    DumpTextBox pb = this;
    while (pb.pointingAddressedLabel != null) {
      pb = pb.pointingAddressedLabel.getParentDumpBox();
    }

    FidPanel fidPanel = getParentFidPanel();
    fidPanel.clearArrows();

    pb.rePosition(fidPanel, pb.getX(), pb.getY());

    // Redraw the full JFrame.
    SwingUtilities.getRoot(this).repaint();
  }

  private static final int arrowWidthPart = 26;

  /**
   * Reposition this and all child elements.
   *
   * @return Next y coordinate to set.
   */
  private int rePosition(FidPanel fidPanel, int x, int y) {
    setLocation(x, y);

    // Calculation of X position (for every child element):
    // - Number of child elements.
    // - Some additional static space for the arrow pointer.
    // - Right edge of this DumpTextBox.
    int directChildCount = getDirectChildCount(this);
    int childX = x + getWidth() + arrowWidthPart + 10 * directChildCount;

    // Calculation of Y position (of the child elements):
    // - Starting with Y position of this DumpTextBox.
    // - Add height of a child DumpTextBox an some addition pixels for spacing.
    int childY = y;
    int childY0 = childY + getHeight() + 10;

    // This value changes to true when the zig zag arrow first time goes down
    // stairs.
    boolean upOrDown = false;

    // Loop over all child labels (not every points to a sub DumpTextBox).
    int x3 = childX;
    int x1 = getWidth() + x;
    int x2 = x1;
    for (Component label : getComponents()) {
      if (label instanceof AddressedLabel) {
        List<DumpTextBox> children = ((AddressedLabel) label).getChildren();
        if (children != null) {
          if (upOrDown) {
            x2 -= 10;
          } else {
            x2 += 10;
          }
          for (DumpTextBox child : children) {
            int y1 = y + label.getY() + (label.getHeight() + 1) / 2;
            int y2 = childY + label.getHeight() * 3 / 2;
            if (y1 < y2 && !upOrDown) {
              upOrDown = true;
              x2 = x3 - arrowWidthPart - 10;
            }
            fidPanel.addArrow(x1, x2, x3, y1, y2, child.specialBox);
            childY = child.rePosition(fidPanel, childX, childY);
          }
        }
      }
    }

    // Calculate Y coordinate for the next (recursive) DumpTextBox.
    childY = Math.max(childY, childY0);

    return childY;
  }

  /**
   * Count number of real shown child boxes on the right side of the container.
   */
  private int getDirectChildCount(Container container) {
    int counter = 0;
    for (Component comp : container.getComponents()) {
      if (comp instanceof AddressedLabel) {
        List<DumpTextBox> children = ((AddressedLabel) comp).getChildren();
        if (children != null) {
          counter += children.size();
        }
      } else if (comp instanceof JPanel) {
        counter += getDirectChildCount((Container) comp);
      }
    }
    return counter;
  }

  public boolean setCurrentAddressedLabel(AddressedLabel label) {
    // Make the full addressed label visible.
    moveLabelToBeVisible(label);

    // Store as being the last addressed laben within this dump text box.
    setLastChoosenAddressedLabel(label);

    // Set the label as being the only one choosen within this FidPanel.
    return getParentFidPanel().setChoosenAddressedLabel(label);
  }

  private void moveLabelToBeVisible(AddressedLabel label) {
    Rectangle labelBounds = label.getBounds();
    Rectangle boxBounds = this.getBounds();
    Rectangle panelBounds = getParentFidPanel().getBounds();
    int dx = 0;
    int dy = 0;

    // Too much left or right?
    // We want to see the full width of the box of the label.
    if (boxBounds.x < 0) {
      dx = -boxBounds.x;
    } else if (boxBounds.x + boxBounds.width > panelBounds.width) {
      dx = panelBounds.width - (boxBounds.x + boxBounds.width);
    }

    // Too much up or down?
    if (boxBounds.y + labelBounds.y < 0) {
      dy = -boxBounds.y - labelBounds.y;
    } else if (boxBounds.y + labelBounds.y + labelBounds.height > panelBounds.height) {
      dy = panelBounds.height - (boxBounds.y + labelBounds.y + labelBounds.height);
    }

    if (dx != 0 || dy != 0) {
      getParentFidPanel().moveAllChildren(dx, dy);
    }
  }

  public boolean chooseFirstAddressedLabel() {
    Component[] components = getComponents();
    int size = components.length;
    for (int i = 0; i < size; i++) {
      if (components[i] instanceof AddressedLabel) {
        ((AddressedLabel) components[i]).choose();
        return true;
      }
    }
    return false;
  }

  public boolean chooseLastAddressedLabel() {
    Component[] components = getComponents();
    int size = components.length;
    for (int i = size - 1; i >= 0; i--) {
      if (components[i] instanceof AddressedLabel) {
        ((AddressedLabel) components[i]).choose();
        return true;
      }
    }
    return false;
  }

  private void setLastChoosenAddressedLabel(AddressedLabel lastChoosenAddressedLabel) {
    this.lastChoosenAddressedLabel = lastChoosenAddressedLabel;
  }

  public boolean chooseLastChoosenAddressedLabel() {
    if (lastChoosenAddressedLabel != null) {
      return lastChoosenAddressedLabel.choose();
    }
    return chooseFirstAddressedLabel();
  }

  public FidPanel getParentFidPanel() {
    return (FidPanel) getParent();
  }

  public void showHead() {
    if (getX() < 0 || getY() < 0) {
      int dx = getX() < 0 ? -getX() : 0;
      int dy = getY() < 0 ? -getY() : 0;
      getParentFidPanel().moveAllChildren(dx, dy);
    }
  }

  public void showFooter() {
    FidPanel fidPanel = getParentFidPanel();
    int dx = fidPanel.getWidth() - (getX() + getWidth());
    int dy = fidPanel.getHeight() - (getY() + getHeight());
    if (dx < 0 || dy < 0) {
      dx = dx < 0 ? dx : 0;
      dy = dy < 0 ? dy : 0;
      fidPanel.moveAllChildren(dx, dy);
    }
  }

  public void chooseNextAddressedLabel(AddressedLabel choosenAddressedLabel) {
    Component[] children = getComponents();
    int childrenCount = children.length;
    for (int i = childrenCount - 1; i >= 0; i--) {
      // Found current choosen label?
      if (children[i] == choosenAddressedLabel) {
        // Go to the next (lower) addressed label and choose it.
        while (true) {
          i++;
          if (i == childrenCount) {
            break;
          }

          // Another addressed label found. Choose it and finish.
          if (children[i] instanceof AddressedLabel) {
            ((AddressedLabel) children[i]).choose();
            return;
          }
        }

        // The choosen label was the last in its dump text box.
        // -> Make footer of dump text box visible.
        showFooter();
        gotoNextSiblingAddressedLabel();
        break;
      }
    }
  }

  /**
   * If this dump text box has a "near" sibling next dumb text box than choose the
   * first addressed label in it.
   */
  private void gotoNextSiblingAddressedLabel() {
    // Master dump text block? Than exit here.
    if (pointingAddressedLabel == null) {
      return;
    }

    // Maybe a "real sibling" (a special box or so)?
    List<DumpTextBox> parentChildren = pointingAddressedLabel.getChildren();
    for (int i = 0; i < parentChildren.size(); i++) {
      DumpTextBox child = parentChildren.get(i);
      if (child == this) {
        if (i + 1 < parentChildren.size()) {
          parentChildren.get(i + 1).chooseFirstAddressedLabel();
          return;
        }
      }
    }

    // Okay, just a normal sibling box?
    DumpTextBox parentDTB = pointingAddressedLabel.getParentDumpBox();
    boolean found = false;
    for (Component child : parentDTB.getComponents()) {
      if (child == pointingAddressedLabel) {
        found = true;
      } else if (found && child instanceof AddressedLabel) {
        AddressedLabel al = (AddressedLabel) child;
        if (al.getChildren() != null && !al.getChildren().isEmpty()) {
          al.getChildren().get(0).chooseFirstAddressedLabel();
          return;
        }
      }
    }
  }

  public void choosePreviousAddressedLabel(AddressedLabel choosenAddressedLabel) {
    Component[] children = getComponents();
    int childrenCount = children.length;
    for (int i = childrenCount - 1; i >= 0; i--) {
      // Found current choosen label?
      if (children[i] == choosenAddressedLabel) {
        // Go to the next (upper) addressed label and choose it.
        while (true) {
          i--;
          if (i < 0) {
            break;
          }

          // Another addressed label found. Choose it and finish.
          if (children[i] instanceof AddressedLabel) {
            ((AddressedLabel) children[i]).choose();
            return;
          }
        }

        // The choosen label was the first in its dump text box.
        // -> Make head of dump text box visible.
        showHead();
        gotoPreviousSiblingAddressedLabel();
        break;
      }
    }
  }

  /**
   * If this dump text box has a "near" sibling previous dumb text box than choose
   * the last addressed label in it.
   */
  private void gotoPreviousSiblingAddressedLabel() {
    // Master dump text block? Than exit here.
    if (pointingAddressedLabel == null) {
      return;
    }

    // Maybe a "real sibling" (a special box or so)?
    List<DumpTextBox> parentChildren = pointingAddressedLabel.getChildren();
    for (int i = parentChildren.size() - 1; i >= 0; i--) {
      DumpTextBox child = parentChildren.get(i);
      if (child == this) {
        if (i > 0) {
          parentChildren.get(i - 1).chooseLastAddressedLabel();
          return;
        }
      }
    }

    // Okay, just a normal sibling box?
    DumpTextBox parentDTB = pointingAddressedLabel.getParentDumpBox();
    boolean found = false;
    Component[] components = parentDTB.getComponents();
    for (int i = components.length - 1; i >= 0; i--) {
      Component child = components[i];
      if (child == pointingAddressedLabel) {
        found = true;
      } else if (found && child instanceof AddressedLabel) {
        AddressedLabel al = (AddressedLabel) child;
        List<DumpTextBox> alChildren = al.getChildren();
        if (alChildren != null && !alChildren.isEmpty()) {
          alChildren.get(alChildren.size() - 1).chooseLastAddressedLabel();
          return;
        }
      }
    }
  }

}
