package com.packenius.fid.gui.label;

import com.packenius.fid.gui.DumpTextBox;

import javax.swing.JLabel;
import java.awt.Color;

/**
 * Text line for a graphical dump block that is just a line and do not lead to
 * another dump block.
 *
 * @author Christian Packenius, 2019.
 */
public class PureTextLabel extends JLabel implements DumpLabel {
  private static final long serialVersionUID = -8577994297559759734L;

  private static final Color textColor = new Color(0, 64, 0);

  public PureTextLabel(String headline, DumpTextBox parent) {
    super(headline);
    setFont(parent.getFont());
    setForeground(textColor);
    parent.add(this);
  }

  @Override
  public int getTextLength() {
    return getText().length();
  }

  @Override
  public void setTextCharMax(int maxChars) {
    // May be ignored.
  }
}
