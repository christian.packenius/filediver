package com.packenius.fid.gui.label;

import com.packenius.fid.gui.DumpTextBox;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;

/**
 * Headline with two text strings: One left and one right on the same line.
 *
 * @author Christian Packenius, 2019.
 */
public class HeadlineLabel extends JPanel implements DumpLabel {
  private static final long serialVersionUID = 6479034758271440441L;
  private static final Color textColor = new Color(128, 0, 0);
  private JLabel labelLeft, labelRight;

  public HeadlineLabel(String leftText, String rightText, DumpTextBox parent, Color backgroundColor) {
    super(new BorderLayout());
    setFont(parent.getFont());
    setBackground(backgroundColor);
    labelLeft = createLabel(leftText + " ");
    labelRight = createLabel(rightText);
    add(labelLeft, BorderLayout.WEST);
    add(labelRight, BorderLayout.EAST);
    parent.add(this);
  }

  private JLabel createLabel(String text) {
    JLabel label = new JLabel(text);
    label.setFont(getFont());
    label.setForeground(textColor);
    return label;
  }

  @Override
  public int getTextLength() {
    return labelLeft.getText().length() + labelRight.getText().length();
  }

  @Override
  public void setTextCharMax(int maxChars) {
    // Kann ignoriert werden.
  }
}
