package com.packenius.fid.gui.label;

import com.packenius.fid.gui.DumpTextBox;

import javax.swing.JLabel;
import java.awt.Color;
import java.util.Arrays;

/**
 * Single line label.
 *
 * @author Christian Packenius, 2019.
 */
public class LineLabel extends JLabel implements DumpLabel {
  private static final long serialVersionUID = 6479034758271440441L;
  private static final Color textColor = new Color(0, 0, 196);

  public LineLabel(String headline, DumpTextBox parent) {
    super(headline);
    setFont(parent.getFont());
    setForeground(textColor);
    parent.add(this);
  }

  @Override
  public void setTextCharMax(int len) {
    String string = repeatChar(len, '-');
    setText(string);
    setSize(getMinimumSize());
  }

  public static String repeatChar(int len, char ch) {
    char[] ca = new char[len];
    Arrays.fill(ca, ch);
    String string = new String(ca);
    return string;
  }

  @Override
  public int getTextLength() {
    return getText().length();
  }
}
