package com.packenius.fid.examples;

import com.packenius.datadivider.DataDivider;
import com.packenius.datadivider.javaclass.Statics;
import com.packenius.dumpapi.MainDumpBlock;
import com.packenius.fid.gui.FileDiverFrame;

import java.io.IOException;
import java.io.InputStream;

/**
 * Example for how to start FiD with a Kotlin class.
 *
 * @author Christian Packenius, 2019.
 */
public class FileDiverHelloKotlin {
  public static void main(String[] args) throws IOException {
    try (InputStream resourceAsStream = FileDiverHelloKotlin.class.getResourceAsStream("HelloWorldKt.class")) {
      byte[] content = Statics.getInputStreamBytes(resourceAsStream);
      MainDumpBlock mainDumpBlock = new DataDivider().divide(content);
      new FileDiverFrame(mainDumpBlock);
    }
  }
}
