package com.packenius.fid.examples.dynamiccompilation;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardJavaFileManager;
import java.io.IOException;
import java.security.SecureClassLoader;

class ClassFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {
  private InMemoryJavaSourceCode imClazz;

  private SecureClassLoader classLoader;

  public ClassFileManager(StandardJavaFileManager standardManager, InMemoryJavaSourceCode imClazz) {
    super(standardManager);
    this.imClazz = imClazz;
    this.classLoader = new SecureClassLoader() {
      @Override
      protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] b = imClazz.getBytes();
        return super.defineClass(name, imClazz.getBytes(), 0, b.length);
      }
    };
  }

  @Override
  public ClassLoader getClassLoader(Location location) {
    return this.classLoader;
  }

  @Override
  public JavaFileObject getJavaFileForOutput(Location location, String className, Kind kind, FileObject sibling)
      throws IOException {
    return imClazz;
  }
}
