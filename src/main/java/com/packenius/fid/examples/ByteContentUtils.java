package com.packenius.fid.examples;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Christian Packenius, 2019.
 */
class ByteContentUtils {
  /**
   * Reads full input stream into a byte array.
   */
  static byte[] getInputStreamBytes(InputStream in) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    int by;
    while ((by = in.read()) >= 0) {
      baos.write(by);
    }
    return baos.toByteArray();
  }

  /**
   * Get the raw bytes of a class from class path. This is the content of a .class
   * file.
   */
  static byte[] getClassBytes(Class<?> clazz) {
    // We cannot use getSimpleName() blind, there is only returned "$" when clazz is
    // an inner class. :-/
    String simpleName = clazz.getName();
    int k = simpleName.lastIndexOf('.');
    if (k > 0) {
      simpleName = simpleName.substring(k + 1);
    }
    try {
      return getInputStreamBytes(clazz.getResourceAsStream(simpleName + ".class"));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
