package com.packenius.hexr;

/**
 * Static methods.
 *
 * @author Christian Packenius, 2013.
 */
class Statics implements Constants {
  private static String spaces = " ";

  /**
   * Returns the given number of spaces as a single string.
   *
   * @param spaceCount Number of spaces to return.
   * @return String with number of spaces given in parameter.
   */
  static String getSpaces(int spaceCount) {
    if (spaceCount < 0) {
      throw new IllegalArgumentException("Number of spaces must not be negative!");
    }
    if (spaceCount > spaces.length()) {
      synchronized (spaces) {
        String localSpaces = spaces;
        while (spaceCount > localSpaces.length()) {
          localSpaces += localSpaces;
        }
        spaces = localSpaces;
      }
    }
    return spaces.substring(0, spaceCount);
  }
}
