package com.packenius.hexr;

/**
 * Static values and objects.
 *
 * @author Christian Packenius, 2013.
 */
interface Constants {
  /**
   * Line separator of this system.
   */
  static final String EOL = System.getProperty("line.separator");

  /**
   * Hexadecimal characters as string.
   */
  static final String STRING_HEX_CHARS = "0123456789ABCDEF";

  /**
   * Hexadecimal characters as character array.
   */
  static final char[] HEX_CHARS = STRING_HEX_CHARS.toCharArray();
}
