package com.packenius.hexr;

import java.util.ArrayList;
import java.util.List;

/**
 * The <i>HexDump</i> class in Apache's Commons IO is fast, concise and easy to
 * use but not flexible enough for all of my needs. <i>HexR</i> is little
 * better.
 *
 * @author Christian Packenius, 2013.
 */
public class HexR extends com.packenius.hexr.Statics implements Constants {
  /**
   * Byte array to dump.
   */
  private byte[] ba = null;

  /**
   * First byte of the array to dump (inclusive) and last byte of the array to
   * dump (exclusive).
   */
  private int start, end;

  /**
   * Result type if wanted - may be null.
   */
  private Class<? extends Object> resultType = String.class;

  /**
   * Result (if wanted) to return.
   */
  private Object result = null;

  /**
   * StringBuilder object with result.
   */
  private StringBuilder sb;

  /**
   * List of Lines with result.
   */
  private List<String> lines;

  /**
   * 16 characters to use as hex codes.
   */
  private char[] hexCodes = HEX_CHARS;

  /**
   * Number of bytes to dump in a line.
   */
  private int hexCodesPerLine = 16;

  /**
   * Delimiter string to print between two hex codes.
   */
  private String hexCodeDelimiter = " ";

  /**
   * String to print instead of a hex code in the last line (when missing enough
   * bytes to fill this line).
   */
  private String freeHexCodes = "   ";

  /**
   * Shall memory address be printed at the beginning of the line?
   */
  private boolean printMemoryAddress = true;

  /**
   * Shall ASCII text be printed at the end of the line?
   */
  private boolean printAsciiText = true;

  /**
   * Shall hex codes be printed upper cased?
   */
  private boolean printUpperCasedHexCodes = true;

  /**
   * Memory address of the first byte to dump.
   */
  private int firstMemoryAdress = 0;

  /**
   * Codes to use instead of hex codes.
   */
  private String[] alternativeHexCodes = null;

  /**
   * Code to use if one of the alternative codes in the upper array is
   * <i>null</i>.
   */
  private String unknownCode = null;

  /**
   * The following objects are only for single method usage. They are defined as
   * class variables for performance reasons.
   */

  private StringBuilder sbAscii = new StringBuilder();

  private StringBuilder sbHexCodes = new StringBuilder();

  /**
   * Dumps string bytes.
   *
   * @param string String to dump the bytes from.
   * @return Result object (if wanted). Default type is <i>String</i>.
   */
  public Object dump(String string) {
    return dump(string.getBytes());
  }

  /**
   * Dumps a complete byte array.
   *
   * @param byteArray Byte array. Must not be <i>null</i> (this would lead into a
   *                  <i>NullPointerException</i>).
   * @return Result object (if wanted). Default type is <i>String</i>.
   */
  public Object dump(byte[] byteArray) {
    return dump(byteArray, 0, byteArray.length);
  }

  /**
   * Dumps a complete byte array.
   *
   * @param byteArray Byte array. Must not be <i>null</i> (this would lead into a
   *                  <i>NullPointerException</i>).
   * @param start     First byte to dump (inclusive).
   * @param end       Last byte to dump (exclusive).
   * @return Result object (if wanted). Default type is <i>String</i>.
   */
  public Object dump(byte[] byteArray, int start, int end) {
    ba = byteArray;
    this.start = start;
    this.end = end;
    checkParms();
    createDump();
    return result;
  }

  private void checkParms() {
    if (ba == null) {
      throw new IllegalArgumentException("'content' is null!");
    }
    if (start < 0) {
      throw new IllegalArgumentException("'start' must positive or zero!");
    }
    if (end > ba.length) {
      throw new IllegalArgumentException(
          "'end' is too large! end := " + end + ", content-length := " + ba.length);
    }
    if (start > end) {
      throw new IllegalArgumentException("'start' must lower than or equal to 'end'!");
    }
  }

  /**
   * Central start point for dump creation.
   */
  private void createDump() {
    initResultObject();

    for (int i = start; i < end; i += hexCodesPerLine) {
      String address = printMemoryAddress ? getAddress(firstMemoryAdress + i - start) + " " : "";
      int virtualEnd = i + hexCodesPerLine;
      int realEnd = Math.min(virtualEnd, end);
      int freeBytes = virtualEnd - realEnd;
      String hexCodes = getHexCodes(i, realEnd, freeBytes);
      String ascText = printAsciiText ? " " + getAsciiText(i, realEnd, freeBytes) : "";
      String line = address + hexCodes + ascText;
      addResultLine(line);
    }

    // If no bytes shall be dumped, we create an empty line with exact spaces
    // number.
    if (start == end) {
      String address = printMemoryAddress ? getAddress(firstMemoryAdress) + " " : "";
      int virtualEnd = start + hexCodesPerLine;
      int realEnd = Math.min(virtualEnd, end);
      int freeBytes = virtualEnd - realEnd;
      String hexCodes = getHexCodes(start, realEnd, freeBytes);
      String ascText = printAsciiText ? getAsciiText(start, realEnd, freeBytes) : "";
      String line = address + hexCodes + ascText;
      addResultLine(line);
    }

    setResult();
  }

  /**
   * Adds the next dump line.
   */
  private void addResultLine(String line) {
    if (resultType == String.class) {
      sb.append(line + "\r\n");
    } else if (resultType == String[].class) {
      lines.add(line);
    }
  }

  private String getAddress(int adr) {
    String s = Long.toHexString(adr);
    return "00000000".substring(s.length()) + s;
  }

  /**
   * Returns a string with the hex codes of the wanted bytes.
   */
  private String getHexCodes(int start, int end, int free) {
    sbHexCodes.setLength(0);
    if (alternativeHexCodes == null) {
      for (int i = start; i < end; i++) {
        if (i != start) {
          sbHexCodes.append(hexCodeDelimiter);
        }
        byte by = ba[i];
        sbHexCodes.append(getHexCode(by));
      }
      for (int i = 0; i < free; i++) {
        sbHexCodes.append(freeHexCodes);
      }
    } else {
      for (int i = start; i < end; i++) {
        if (i != start) {
          sbHexCodes.append(hexCodeDelimiter);
        }
        String code = alternativeHexCodes[ba[i] & 255];
        sbHexCodes.append(code == null ? unknownCode : code);
      }
      for (int i = 0; i < free; i++) {
        sbHexCodes.append(freeHexCodes);
      }
    }
    return sbHexCodes.toString();
  }

  /**
   * Returns the hex code as a two byte string.
   */
  private String getHexCode(byte by) {
    return "" + hexCodes[by >> 4 & 15] + hexCodes[by & 15];
  }

  /**
   * Returns a string with the ASCII text of the bytes.
   */
  private String getAsciiText(int start, int end, int free) {
    sbAscii.setLength(0);
    for (int i = start; i < end; i++) {
      byte by = ba[i];
      if (by >= (byte) 32 && by < (byte) 127) {
        sbAscii.append((char) by);
      } else {
        sbAscii.append('.');
      }
    }
    for (int i = 0; i < free; i++) {
      sbAscii.append(' ');
    }
    return sbAscii.toString();
  }

  private void initResultObject() {
    if (resultType == String.class) {
      sb = new StringBuilder();
    } else if (resultType == String[].class) {
      lines = new ArrayList<String>();
    }
  }

  private void setResult() {
    if (resultType == null) {
      result = null;
    } else if (resultType == String.class) {
      result = sb.toString();
    } else if (resultType == String[].class) {
      result = lines.toArray(new String[lines.size()]);
    } else {
      throw new RuntimeException("Illegal result type: " + resultType.toString());
    }
  }

  /**
   * Shall memory address be printed at the beginning of each line?
   *
   * @param printMemoryAddress <i>true</i> to print memory addresses (default).
   */
  public void setPrintMemoryAddress(boolean printMemoryAddress) {
    this.printMemoryAddress = printMemoryAddress;
  }

  /**
   * Returns <i>true</i> if memory addresses shall be printed at the beginning of
   * each line.
   *
   * @return <i>true</i> if memory addresses shall be printed at the beginning of
   * each line.
   */
  public boolean getPrintMemoryAddress() {
    return printMemoryAddress;
  }

  /**
   * Shall ASCII text be printed at the end of each line?
   *
   * @param printAsciiText <i>true</i> to print ASCII text (default).
   */
  public void setPrintAsciiText(boolean printAsciiText) {
    this.printAsciiText = printAsciiText;
  }

  /**
   * Returns <i>true</i> if ASCII text shall be printed at the end of each line.
   *
   * @return <i>true</i> if ASCII text shall be printed at the end of each line.
   */
  public boolean getPrintAsciiText() {
    return printAsciiText;
  }

  /**
   * Shall hex codes be printed uppercased?
   *
   * @param printUpperCasedHexCodes <i>true</i> to user upper cased hex codes
   *                                (default).
   */
  public void setUpperCasedHexCodes(boolean printUpperCasedHexCodes) {
    this.printUpperCasedHexCodes = printUpperCasedHexCodes;
    if (printUpperCasedHexCodes) {
      hexCodes = HEX_CHARS;
    } else {
      hexCodes = STRING_HEX_CHARS.toLowerCase().toCharArray();
    }
  }

  /**
   * Returns <i>true</i> if hex characters are printed upper cased.
   *
   * @return <i>true</i> if hex characters are printed upper cased.
   */
  public boolean getUpperCasedHexCodes() {
    return printUpperCasedHexCodes;
  }

  /**
   * Set the number of hex codes per line. Default is 16.
   *
   * @param hexCodesPerLine Number of hex codes per line.
   */
  public void setHexCodesPerLine(int hexCodesPerLine) {
    if (hexCodesPerLine < 1) {
      throw new IllegalArgumentException("Number of hex codes per line must be greater than zero!");
    }
    this.hexCodesPerLine = hexCodesPerLine;
  }

  /**
   * Returns the number of hex codes per line.
   *
   * @return Number of hex codes per line. Default ist 16.
   */
  public int getHexCodesPerLine() {
    return hexCodesPerLine;
  }

  /**
   * Set the string to print between two hex codes. Default is a single space.
   *
   * @param hexCodeDelimiter String to print between two hex codes.
   */
  public void setHexCodesDelimiter(String hexCodeDelimiter) {
    if (hexCodeDelimiter == null) {
      hexCodeDelimiter = "";
    }
    this.hexCodeDelimiter = hexCodeDelimiter;
    freeHexCodes = com.packenius.hexr.Statics.getSpaces(2 + hexCodeDelimiter.length());
  }

  /**
   * Set the memory address that represents the address of the first byte to dump.
   *
   * @param firstMemoryAdress Default is zero.
   */
  public void setFirstMemoryAddress(int firstMemoryAdress) {
    this.firstMemoryAdress = firstMemoryAdress;
  }

  /**
   * Returns the first memory address to use for memory address printing.
   *
   * @return First memory address to print.
   */
  public int getFirstMemoryAddress() {
    return firstMemoryAdress;
  }

  /**
   * Set codes to use instead of hex codes.
   *
   * @param codes       Codes to use.
   * @param unknownCode String to use if an entry of the upper array is
   *                    <i>null</i>.
   */
  public void setAlternativeHexCodes(String[] codes, String unknownCode) {
    if (codes == null) {
      throw new IllegalArgumentException("Alternative codes must not be null!");
    }
    if (unknownCode == null) {
      throw new IllegalArgumentException("Alternative code for unknown codes must not be null!");
    }
    alternativeHexCodes = new String[256];
    int codesLength = Math.min(codes.length, 256);
    int minimalCodeLength = 0;
    for (int i = 0; i < codesLength; i++) {
      if (codes[i] != null) {
        minimalCodeLength = Math.max(minimalCodeLength, codes[i].length());
      }
    }
    for (int i = 0; i < codesLength; i++) {
      if (codes[i] != null) {
        alternativeHexCodes[i] = codes[i] + getSpaces(minimalCodeLength - codes[i].length());
      }
    }
    this.unknownCode = unknownCode + getSpaces(minimalCodeLength - unknownCode.length());
  }

  /**
   * Set the type of desired result object. Valid types are String or String[].
   */
  public void setResultType(Class<? extends Object> resultType) {
    this.resultType = resultType;
  }
}
