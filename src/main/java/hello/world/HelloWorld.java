package hello.world;

public class HelloWorld {
  public static void main(String[] args) {
    HelloWorld hw = new HelloWorld();
    hw.printMe("Gaby", "Klaus");
  }

  private void printMe(String... namen) {
    for (String name : namen)
      singlePrintMe(name);
  }

  private void singlePrintMe(String name) {
    System.out.println("Hallo " + name + "!");
  }
}
