package hello.world;

public class ShortHelloWorld {
  public static void main(String[] args) {
    new ShortHelloWorld();
  }

  private ShortHelloWorld() {
    System.out.println("Hello World!");
  }
}
